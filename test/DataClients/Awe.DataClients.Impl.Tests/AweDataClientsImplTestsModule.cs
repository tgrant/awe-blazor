using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.TestBase;
using Awe.DataClients.Contracts;
using Awe.DataClients.Impl.Tests.DependencyInjection;

namespace Awe.DataClients.Impl.Tests
{
    [DependsOn(
        typeof(AbpTestBaseModule),
        typeof(AweDataClientsContractsModule)
    )]
    public class AweDataClientsImplTestsModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(AweDataClientsContractsModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}