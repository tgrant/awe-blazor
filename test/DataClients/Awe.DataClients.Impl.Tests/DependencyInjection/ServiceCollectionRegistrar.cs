﻿using Abp.Dependency;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;

namespace Awe.DataClients.Impl.Tests.DependencyInjection
{
    public static class ServiceCollectionRegistrar
    {
        public static void Register(IIocManager iocManager)
        {
            var services = new ServiceCollection();

            var serviceProvider = WindsorRegistrationHelper.CreateServiceProvider(iocManager.IocContainer, services);

            //-- Register services example --
            //iocManager.IocContainer.Register(
            //    Component
            //        .For<DbContextOptions<AweSettingsDbContext>>()
            //        .Instance(builder.Options)
            //        .LifestyleSingleton()
            //);
        }
    }
}
