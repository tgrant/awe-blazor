﻿using Newtonsoft.Json;
using Shouldly;

namespace Awe.DataClients.Impl.Tests.Settings;

public class FileDataClientTestBaseImpl_LoadAllPages : FileSettingsClientTestBase
{
    const string _filePath = @"Settings\Data\viewer.json";

    [Fact]
    public async Task LoadJson_ShouldReturnAllProps()
    {
        // Arrange
        var path = Path.IsPathRooted(_filePath)
            ? _filePath
            : Path.GetRelativePath(Directory.GetCurrentDirectory(), _filePath);

        // Act
        var fileData = await File.ReadAllTextAsync(path);
        var pages = JsonConvert.DeserializeObject<Page[]>(fileData);

        // Assert
        pages.ShouldNotBeNull();
        pages.Length.ShouldBe(1);

        var page = pages[0];
        page.Id.ShouldBe(1);
        page.Alias.ShouldBe("PageNameWithoutSpace");
        page.PageGroup.ShouldBe("SettingsOrChopOrOther");
        page.PageType.ShouldBe("Book");
        page.Caption.ShouldBe("Заголовок с возможными пробелами");

        var pageProps = page.PageProps;
        pageProps.ShouldNotBeNull();
        pageProps.Count.ShouldBe(2);

        pageProps[0].ShouldNotBeNull();
        pageProps[0].Id.ShouldBe(1101);
        pageProps[0].PropType.ShouldBe("None");
        pageProps[0].Value.ShouldBe("100500");

        pageProps[1].ShouldNotBeNull();
        pageProps[1].Id.ShouldBe(1102);
        pageProps[1].PropType.ShouldBe("Caption");
        pageProps[1].Value.ShouldBe("CaptionValue");

        page.MasterViewer.Id.ShouldBe(12);
        page.MasterViewer.ViewerType.ShouldBe("List");
        page.MasterViewer.Alias.ShouldBe("ViewerAlias");
        page.MasterViewer.Caption.ShouldBe("Заголовок Вьювера");
        page.MasterViewer.ViewerLevel.ShouldBe(1);
        page.MasterViewer.ViewerController.ShouldBe("~ViewerBookController");
        page.MasterViewer.ViewerPanel.ShouldBe("~ViewerBookPanel");

        page.MasterViewer.Columns.Count.ShouldBe(4);

        page.MasterViewer.Columns[0].Id.ShouldBe(1201);
        page.MasterViewer.Columns[0].Caption.ShouldBe("Id");
        page.MasterViewer.Columns[0].ColumnOrder.ShouldBe(0);
        page.MasterViewer.Columns[0].FormatType.ShouldBeNull();
        page.MasterViewer.Columns[0].Height.ShouldBeNull();
        page.MasterViewer.Columns[0].Width.ShouldBe(50);
        page.MasterViewer.Columns[0].ReadOnly.ShouldBeTrue();
        page.MasterViewer.Columns[0].ShowSum.ShouldBeFalse();
        page.MasterViewer.Columns[0].SortOrder.ShouldBe(0);
        page.MasterViewer.Columns[0].Visible.ShouldBeTrue();

        page.MasterViewer.Columns[1].Id.ShouldBe(1202);
        page.MasterViewer.Columns[1].Caption.ShouldBe("Наименование");
        page.MasterViewer.Columns[1].ColumnOrder.ShouldBe(1);
        page.MasterViewer.Columns[1].FormatType.ShouldBeNull();
        page.MasterViewer.Columns[1].Height.ShouldBeNull();
        page.MasterViewer.Columns[1].Width.ShouldBe(100);
        page.MasterViewer.Columns[1].ReadOnly.ShouldBeTrue();
        page.MasterViewer.Columns[1].ShowSum.ShouldBeFalse();
        page.MasterViewer.Columns[1].SortOrder.ShouldBe(0);
        page.MasterViewer.Columns[1].Visible.ShouldBeTrue();

        page.MasterViewer.Columns[2].Id.ShouldBe(1203);
        page.MasterViewer.Columns[2].Caption.ShouldBe("Заголовок");
        page.MasterViewer.Columns[2].ColumnOrder.ShouldBe(2);
        page.MasterViewer.Columns[2].FormatType.ShouldBeNull();
        page.MasterViewer.Columns[2].Height.ShouldBe(25);
        page.MasterViewer.Columns[2].Width.ShouldBe(100);
        page.MasterViewer.Columns[2].ReadOnly.ShouldBeTrue();
        page.MasterViewer.Columns[2].ShowSum.ShouldBeTrue();
        page.MasterViewer.Columns[2].SortOrder.ShouldBe(0);
        page.MasterViewer.Columns[2].Visible.ShouldBeTrue();

        page.MasterViewer.Columns[3].Id.ShouldBe(1204);
        page.MasterViewer.Columns[3].Caption.ShouldBe("ReadOnly");
        page.MasterViewer.Columns[3].ColumnOrder.ShouldBe(3);
        page.MasterViewer.Columns[3].FormatType.ShouldBe(1);
        page.MasterViewer.Columns[3].Height.ShouldBeNull();
        page.MasterViewer.Columns[3].Width.ShouldBe(10);
        page.MasterViewer.Columns[3].ReadOnly.ShouldBeTrue();
        page.MasterViewer.Columns[3].ShowSum.ShouldBeFalse();
        page.MasterViewer.Columns[3].SortOrder.ShouldBe(0);
        page.MasterViewer.Columns[3].Visible.ShouldBeTrue();

        page.MasterViewer.DataSource.ShouldNotBeNull();
        page.MasterViewer.DataSource.Source.ShouldBe("AweApi");
        page.MasterViewer.DataSource.TableName.ShouldBe("ViewerTable");
        page.MasterViewer.DataSource.ViewerAlias.ShouldBe("ViewerAlias");
        page.MasterViewer.DataSource.ServiceName.ShouldBe("ApiViewerService");

        page.MasterViewer.DataSource.LoadCrud.VerbName.ShouldBe("GetAll");

        page.MasterViewer.DataSource.LoadCrud.InParams[0].ParamType.ShouldBe("int");
        page.MasterViewer.DataSource.LoadCrud.InParams[0].DtoName.ShouldBeNull();
        page.MasterViewer.DataSource.LoadCrud.InParams[0].ParamName.ShouldBe("viewerId");

        page.MasterViewer.DataSource.LoadCrud.InParams[1].ParamType.ShouldBeNull();
        page.MasterViewer.DataSource.LoadCrud.InParams[1].DtoName.ShouldBe("Dto");
        page.MasterViewer.DataSource.LoadCrud.InParams[1].ParamName.ShouldBe("ViewerDto");

        page.MasterViewer.DataSource.LoadCrud.OutParam.ParamType.ShouldBe("Dto");
        page.MasterViewer.DataSource.LoadCrud.OutParam.DtoName.ShouldBe("ViewerListInfo");
        page.MasterViewer.DataSource.LoadCrud.OutParam.ParamName.ShouldBeNull();

        page.MasterViewer.ViewerProps.Count.ShouldBe(0);

        page.MasterViewer.EditLink.ShouldNotBeNull();

        page.DetailViewers.Count.ShouldBe(0);
    }
}