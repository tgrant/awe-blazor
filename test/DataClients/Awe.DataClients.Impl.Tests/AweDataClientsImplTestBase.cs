﻿using Abp.Dependency;
using Awe.DataClients.Contracts;
using Abp.Reflection.Extensions;
using Abp.TestBase;

namespace Awe.DataClients.Impl.Tests
{
    public class AweDataClientsImplTestBase : AbpTestBaseModule, IDisposable
    {
        protected IIocManager LocalIocManager { get; }

        public AweDataClientsImplTestBase()
        {
            //LocalIocManager = new IocManager();

            //LocalIocManager.RegisterAssemblyByConvention(typeof(AweDataClientsContractsModule).GetAssembly());

            ////LocalIocManager.IocContainer.Register(
            ////    Component.For<IProgramConstDomainService>()
            ////        .Instance(_programConstDomainService)
            ////);
            ////LocalIocManager.RegisterIfNot<IFuncEvalDomainService, FuncEvalDomainService>();
        }

        public void Dispose()
        {
            LocalIocManager?.Dispose();
        }
    }
}
