using System;
using System.Windows.Forms;
using Awe.Core.Common.Contracts;
using Awe.Core.Common.Contracts.Data;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.View.LevelPanels;
using Awe.Core.Widget.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.Widget;

namespace Awe.Platform.Win.View.Page;

public partial class PageView : Form, IPageView
{
    public PageView()
    {
        InitializeComponent();
    }

    public IWPage Widget { get; set; }
    public string ViewerAlias { get; set; }
    public PageType PageType { get; set; }

    public event Action<ILevelPanelView> ViewRendered;

    public string Caption
    {
        get => Text;
        set => Text = value;
    }

    public virtual void EventsInit()
    {
    }

    public PageViewInfo PageViewInfo { get; set; }
    public FilterCollection Filters { get; set; }
}