﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Awe.Core.Action.Contracts;
using Awe.Core.Common.Contracts;
using Awe.Platform.Wasm.Domain;

namespace Awe.Platform.Wasm.View;

[DependsOn(typeof(ActionContractsModule))]
[DependsOn(typeof(AweCoreCommonContractsModule))]
[DependsOn(typeof(PlatformWasmDomainModule))]
public class PlatformWasmViewModule : AbpModule
{
    public override void Initialize()
    {
        IocManager.RegisterAssemblyByConvention(GetType().GetAssembly());
    }
}