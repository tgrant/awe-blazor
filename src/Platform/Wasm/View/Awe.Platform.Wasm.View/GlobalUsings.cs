// Global using directives

global using System.Net.Http;
global using System.Net.Http.Json;

global using Microsoft.AspNetCore.Components;
global using Microsoft.AspNetCore.Components.Web;

global using Microsoft.AspNetCore.Components.Forms;
global using Microsoft.AspNetCore.Components.Routing;
global using Microsoft.AspNetCore.Components.Web.Virtualization;
global using Microsoft.AspNetCore.Components.WebAssembly.Http;
global using Microsoft.JSInterop;

global using Awe.Core.Widget.Contracts;

global using Awe.Platform.Wasm.View;
global using Awe.Platform.Wasm.View.Pages;
global using Awe.Platform.Wasm.View.Shared;

global using DevExpress.Blazor;