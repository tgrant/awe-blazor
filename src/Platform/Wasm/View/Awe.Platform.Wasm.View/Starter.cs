﻿using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Abp.Dependency;
using Awe.Core.Common.Contracts.Configuration;
using Awe.Core.Common.Contracts.ViewEventsData;
using Awe.Platform.Wasm.View.Events;
using Castle.MicroKernel.Registration;
using DevExpress.Blazor.Configuration;
using Flurl;
using Microsoft.Extensions.Configuration;

namespace Awe.Platform.Wasm.View
{
    public class Starter : IStarter, ISingletonDependency
    {
        private AppSettings _appSettings;

        public async Task StartAsync(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            var appSettings = builder.Configuration.Get<AppSettings>();

            appSettings.BaseAddress = builder.HostEnvironment.BaseAddress;
            appSettings.Environment = builder.HostEnvironment.Environment;

            _appSettings = appSettings;

            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            var services = builder.Services;
            AddInjectionService(services, appSettings);
            AddDevExpress(services, appSettings);
            AddHttpClients(services, appSettings);

            var app = builder.Build();
            await app.RunAsync();
        }

        private static void AddDevExpress(IServiceCollection services, AppSettings appSettings)
        {
            services.AddDevExpressBlazor(options => { options.SizeMode = SizeMode.Small; });
            services.Configure<GlobalOptions>(options => { options.BootstrapVersion = BootstrapVersion.v5; });
        }

        private void AddInjectionService(IServiceCollection services, AppSettings appSettings)
        {
            IocManager.Instance.IocContainer.Register(Component
                .For<AppSettings>()
                .Instance(appSettings));

            services.AddTransient<IViewEventService, ViewEventService>();
        }

        private void AddHttpClients(IServiceCollection services, AppSettings appSettings)
        {
            var apiUrl = appSettings.App.Api.BaseApiUrl;
            var apiPrefix = appSettings.App.Api.ApiPrefix;

            try
            {
                ConfigureHttpClient<HttpClient>(services, Url.Combine(apiUrl.AweApi, apiPrefix.AweApi, "/"), nameof(apiUrl.AweApi));
                ConfigureHttpClient<HttpClient>(services, Url.Combine(apiUrl.AweApiSettings, apiPrefix.AweApiSettings, "/"), nameof(apiUrl.AweApiSettings));
                ConfigureHttpClient<HttpClient>(services, Url.Combine(apiUrl.AweApiAuth, apiPrefix.AweApiAuth, "/"), nameof(apiUrl.AweApiAuth));

                ConfigureHttpClient<HttpClient>(services, Url.Combine(appSettings.BaseAddress, "sample_data", "/"), "viewer");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            #region Skip

            //var baseAddress = new Uri(builder.HostEnvironment.BaseAddress);
            //services.AddScoped(sp => new HttpClient { BaseAddress = baseAddress });

            //IocManager.Instance.IocContainer.Register(Component
            //    .For<HttpClient>()
            //    .UsingFactoryMethod(() => new HttpClient
            //    {
            //        BaseAddress = baseAddress
            //    }));

            // // Configure the HTTP request pipeline.
            // if (!app.Environment.IsDevelopment())
            // {
            //     app.UseExceptionHandler("/Error");
            //     // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //     app.UseHsts();
            // }

            #endregion
        }

        private void ConfigureHttpClient<T1>(IServiceCollection services, string url, string httpClientName)
            where T1 : HttpClient
        {
            services
                .AddHttpClient(httpClientName, 
                    client =>
                    {
                        client.BaseAddress = new Uri(url);
                        client.Timeout = TimeSpan.FromSeconds(_appSettings.App.RequestTimeout);
                    })
                ;

            var serviceProvider = services.BuildServiceProvider();
            var iocContainer = IocManager.Instance.IocContainer;

            iocContainer.Register(Component
                .For<HttpClient>()
                .ImplementedBy<HttpClient>()
                .Named(httpClientName)
                .UsingFactoryMethod(() =>
                {
                    var httpClientFactory = serviceProvider.GetRequiredService<IHttpClientFactory>();
                    var httpClient = httpClientFactory.CreateClient(httpClientName);

                    return httpClient;
                }));
        }
    }
}
