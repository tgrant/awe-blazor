﻿using Abp.Domain.Services;
using Abp.Events.Bus;
using Awe.Core.Common.Contracts.ViewEvent;
using Awe.Core.Common.Contracts.ViewEventsData;
using Newtonsoft.Json;

namespace Awe.Platform.Wasm.View.Events
{
    public class ViewEventService : DomainService, IViewEventService
    {
        private readonly EventBus _eventBus;

        public ViewEventService()
        {
            _eventBus = EventBus.Default;
        }

        public void TriggerEvent<T>(T eventData)
            where T : ViewEventData
        {
            _eventBus.Trigger(eventData);

            Logger.Debug(JsonConvert.SerializeObject(eventData));
        }

        public async Task TriggerEventAsync<T>(T eventData) where T : ViewEventData
        {
            await _eventBus.TriggerAsync(eventData);

            Logger.Debug(JsonConvert.SerializeObject(eventData));
        }
    }
}
