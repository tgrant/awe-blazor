using Awe.Core.Common.Contracts;
using Awe.Core.Common.Contracts.Data;
using Awe.Core.Widget.Contracts.View.Page;

namespace Awe.Platform.Wasm.View.Shared.Widget.Page;

public partial class PageCard : PageBase, IPageCardView
{
    [Parameter] public override string ViewerAlias { get; set; }
    [Parameter] public int RowId { get; set; }

    public override PageType PageType { get; set; } = PageType.Card;

    private ParameterView Params { get; set; }

    public override async Task SetParametersAsync(ParameterView parameters)
    {
        this.Params = parameters;
                
        await base.SetParametersAsync(parameters);
    }

    protected override Task OnInitializedAsync()
    {
        Filters = new FilterCollection(new Dictionary<string, int>
        {
            { "Id", RowId }
        });

        return base.OnInitializedAsync();
    }
}