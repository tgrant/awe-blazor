﻿using Abp.Timing;
using Awe.Core.Common.Contracts;
using Awe.Core.Common.Contracts.Data;
using Awe.Core.Common.Contracts.ViewEventsData;
using Awe.Core.Common.Contracts.ViewEventsData.Page;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Platform.Wasm.View.Shared.Widget.Page;

public abstract class PageBase : ComponentBase, IPageView
{
    [Inject] private IViewEventService ViewEventService { get; set; }

    public abstract PageType PageType { get; set; }
    public abstract string ViewerAlias { get; set; }
    
    public PageViewInfo? PageViewInfo { get; set; }
    public FilterCollection Filters { get; set; }

    public virtual string Caption { get; set; }
    public bool Enabled { get; set; }

    protected override async Task OnInitializedAsync()
    {
        if (PageViewInfo == null)
        {
            var eventData = new PageBuildViewEventData(Clock.Now,
                this, 
                this,
                null,
                PageType, 
                ViewerAlias, 
                new FilterCollection(Filters));

            await ViewEventService.TriggerEventAsync(eventData);
        }

        await base.OnInitializedAsync();
    }

    protected override Task OnAfterRenderAsync(bool firstRender)
    {
        if (firstRender)
        {
            
        }

        return base.OnAfterRenderAsync(firstRender);
    }

    public virtual void EventsInit()
    {
    }

    public virtual void Dispose()
    {
        var eventData = new PageDestroyedViewEventData(
            Clock.Now,
            this,
            this,
            null,
            PageType,
            ViewerAlias);

        ViewEventService.TriggerEvent(eventData);
    }

    protected virtual Task ViewEventCallbackAsync(ViewEventData viewEventData)
    {
        var eventData = new PageEventData(
            Clock.Now,
            viewEventData.EventSource,
            this,
            viewEventData,
            PageType,
            ViewerAlias);

        return ViewEventService.TriggerEventAsync(eventData);
    }
}