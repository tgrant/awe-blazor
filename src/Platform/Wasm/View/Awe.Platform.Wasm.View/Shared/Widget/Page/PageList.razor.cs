using Awe.Core.Common.Contracts;
using Awe.Core.Common.Contracts.ViewEventsData;
using Awe.Core.Widget.Contracts.View.Page;

namespace Awe.Platform.Wasm.View.Shared.Widget.Page;

public partial class PageList : PageBase, IPageListView
{
    [Parameter] public override string ViewerAlias { get; set; }

    public override PageType PageType { get; set; } = PageType.Book;

    private ParameterView Params { get; set; }

    public override async Task SetParametersAsync(ParameterView parameters)
    {
        this.Params = parameters;
                
        await base.SetParametersAsync(parameters);
    }

    public override void Dispose()
    {
        base.Dispose();
    }
}