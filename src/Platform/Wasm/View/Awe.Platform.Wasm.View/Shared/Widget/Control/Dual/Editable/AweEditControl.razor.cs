﻿using System.Reflection;
using Abp.Extensions;
using Awe.Contracts.Settings.Enums;
using Awe.Core.Common.Contracts.Widget;

namespace Awe.Platform.Wasm.View.Shared.Widget.Control.Dual.Editable;

public partial class AweEditControl : DualControl
{
    [Parameter] public string? Caption { get; set; }
    [Parameter] public string? Field { get; set; }
    [Parameter] public AweControlType ControlType { get; set; }
    [CascadingParameter] public ISingleDataStore? DataStore { get; set; }

    [Parameter]
    public string? Value
    {
        get => GetDataPropInfo()?.GetValue(Data).ToString();
        set => GetDataPropInfo()?.SetValue(Data, value, null);
    }

    private dynamic? Data => DataStore?.Data;

    private string? TextValue
    {
        get => Value;
        set => Value = value;
    }

    private decimal? NumValue
    {
        get => Convert.ToDecimal(Value);
        set => Value = value.ToString();
    }

    private DateTime? DateValue
    {
        get => Convert.ToDateTime(Value);
        set => Value = value.ToString();
    }

    private PropertyInfo? GetDataPropInfo()
    {
        if (Field.IsNullOrEmpty())
        {
            return null;
        }

        var obj = DataStore?.Data;
        var type = obj?.GetType();

        return type?.GetProperty(Field);
    }
}