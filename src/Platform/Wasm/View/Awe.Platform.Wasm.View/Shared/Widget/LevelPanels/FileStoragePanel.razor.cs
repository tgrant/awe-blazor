﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.View.LevelPanels;

namespace Awe.Platform.Wasm.View.Shared.Widget.LevelPanels;

public partial class FileStoragePanel : AwePanel, IFileStorageView
{
    [Parameter] public FileStorageViewInfo? FileStorageViewInfo { get; set; }

    public string Caption { get; set; }
    public bool Enabled { get; set; }
    public void EventsInit()
    {
        throw new NotImplementedException();
    }
}