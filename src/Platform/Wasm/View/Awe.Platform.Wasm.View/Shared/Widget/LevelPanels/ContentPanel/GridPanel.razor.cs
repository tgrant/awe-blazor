﻿using System.Diagnostics;
using Abp.Timing;
using Awe.Core.Common.Contracts.ViewEventsData;
using Awe.Core.Common.Contracts.ViewEventsData.ContentPanel;
using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;

namespace Awe.Platform.Wasm.View.Shared.Widget.LevelPanels.ContentPanel;

public partial class GridPanel : ContentPanelBase, IGridPanelView
{
    [Inject] private IViewEventService ViewEventService { get; set; }

    [Parameter] public GridPanelViewInfo? GridPanelViewInfo { get; set; }

    [Parameter]
    public IListDataStore? DataStore
    {
        get; 
        set;
    }

    public string Alert { get; set; } = "";

    protected override Task OnInitializedAsync()
    {
        return base.OnInitializedAsync();
    }

    //private void OnRowDoubleClick(GridRowClickEventArgs e)
    //{
    //    Alert = $"The row {e.VisibleIndex} has been double clicked. The row value is '{e.Grid.GetRowValue(e.VisibleIndex, "Id")}'. ";

    //    //Widget?.Controller.RowDoubleClickHandle(this, e);

    //    Debug.Assert(GridPanelViewInfo != null, nameof(GridPanelViewInfo) + " != null");

    //    var gridEventData = new GridEventData(
    //        Clock.Now, 
    //        this, 
    //        (int?)e.Grid.GetRowValue(e.VisibleIndex, "Id"), 
    //        GridPanelViewInfo.LinkedPageInfo);

    //    ViewEventService.TriggerEventAsync(gridEventData);

    //    OnRowDoubleClickCallback.InvokeAsync(Alert);
    //}

    void Grid_UnboundColumnData(GridUnboundColumnDataEventArgs e)
    {
        if (e.FieldName == "Total")
        {
            var price = (decimal)e.GetRowValue("UnitPrice");
            var quantity = (short)e.GetRowValue("Quantity");
            e.Value = price * quantity;
        }
    }

    //void Grid_CustomizeElement(GridCustomizeElementEventArgs e)
    //{
    //    switch (e.ElementType)
    //    {
    //        case GridElementType.DataRow when e.VisibleIndex % 2 == 1:
    //            e.CssClass = "alt-item";
    //            break;
    //        case GridElementType.HeaderCell:
    //            e.Style = "background-color: rgba(0, 0, 0, 0.1)";
    //            e.CssClass = "header-bold";
    //            break;
    //    }
    //}
}