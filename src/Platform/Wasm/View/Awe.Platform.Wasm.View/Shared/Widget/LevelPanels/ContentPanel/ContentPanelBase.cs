﻿using Awe.Core.Common.Contracts.ViewEventsData;
using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;

namespace Awe.Platform.Wasm.View.Shared.Widget.LevelPanels.ContentPanel
{
    public abstract class ContentPanelBase : AwePanel, IContentPanelView
    {
        [Parameter] public ContentPanelViewInfo? ContentPanelViewInfo { get; set; }

        [Parameter] public EventCallback<string> OnRowDoubleClickCallback { get; set; }
        [Parameter] public EventCallback<ViewEventData> OnViewEventCallback { get; set; }

        protected virtual Task RowDoubleClickCallbackAsync(string arg)
        {
            return OnRowDoubleClickCallback.InvokeAsync(arg);
        }

        protected virtual Task ViewEventCallbackAsync(ViewEventData viewEventData)
        {
            return OnViewEventCallback.InvokeAsync(viewEventData);
        }
    }
}
