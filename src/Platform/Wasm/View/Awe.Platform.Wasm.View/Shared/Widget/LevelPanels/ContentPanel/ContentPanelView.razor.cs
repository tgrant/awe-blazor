﻿using Awe.Core.Common.Contracts.Data;
using Awe.Core.Common.Contracts.ViewEvent;
using Awe.Core.Common.Contracts.ViewEventsData;
using Awe.Core.Common.Contracts.ViewEventsData.ContentPanel;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;

namespace Awe.Platform.Wasm.View.Shared.Widget.LevelPanels.ContentPanel;

public partial class ContentPanelView : ContentPanelBase, IContentPanelView
{
}