﻿using Awe.Core.Common.Contracts.ViewEventsData;
using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.View.LevelPanels;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Platform.Wasm.View.Shared.Widget.LevelPanels;

public partial class LevelPanelView : AwePanel, ILevelPanelView
{
    [Parameter] public LevelPanelViewInfo? LevelPanelViewInfo { get; set; }
    [Parameter] public EventCallback<ViewEventData> OnViewEventCallback { get; set; }

    public string Caption { get; set; }
    public bool Enabled { get; set; }

    public virtual void EventsInit()
    {
    }

    private Task RowDoubleClickCallbackAsync(string arg)
    {
        return Task.FromResult(arg);
    }

    private Task ViewEventCallbackAsync(ViewEventData arg)
    {
        return OnViewEventCallback.InvokeAsync(arg);
    }
}