﻿using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;
using DevExpress.Data.Helpers;

namespace Awe.Platform.Wasm.View.Shared.Widget.LevelPanels.ContentPanel;

public partial class SimplePanel : ContentPanelBase, ISimplePanelView
{
    private string _componentLibNamespace = "Awe.Platform.Wasm.View.Shared.Custom.";

    [Parameter] public SimplePanelViewInfo? SimplePanelViewInfo { get; set; }
    
    [Parameter]
    public virtual ISingleDataStore? DataStore
    {
        get;
        set;
    }

    private Type? _simplePanelType;
    private Type? SimplePanelType
    {
        get
        {
            if (_simplePanelType == null 
                && SimplePanelViewInfo != null 
                && !string.IsNullOrEmpty(SimplePanelViewInfo.ViewPanelClass))
            {
                _simplePanelType = Type.GetType($"{_componentLibNamespace}{SimplePanelViewInfo.ViewPanelClass}");
            }

            return _simplePanelType;
        }
    }

    private DynamicComponent? dc;

    private void Refresh()
    {
        (dc?.Instance as IRefreshable)?.Refresh();
    }

    protected override void OnInitialized()
    {
        base.OnInitialized();

        //var xx = Widget?.DataManager?.DataStore;
    }

    public string Alert { get; set; } = "";

    private Dictionary<string, object> Parameters => new() { { nameof(DataStore), DataStore } };
}