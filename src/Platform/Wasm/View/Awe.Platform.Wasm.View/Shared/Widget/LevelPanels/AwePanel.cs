﻿using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.View;

namespace Awe.Platform.Wasm.View.Shared.Widget.LevelPanels;

public abstract class AwePanel : ComponentBase, IDisposable, IView
{
    public virtual void Dispose()
    {
    }

    public string Caption { get; set; }
    public bool Enabled { get; set; }
    public void EventsInit()
    {
        throw new NotImplementedException();
    }
}