﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.View.LevelPanels;

namespace Awe.Platform.Wasm.View.Shared.Widget.LevelPanels;

public partial class LevelMenuPanel : AwePanel, ILevelPanelMenuView
{
    [Parameter] public LevelMenuViewInfo? LevelMenuViewInfo { get; set; }
}