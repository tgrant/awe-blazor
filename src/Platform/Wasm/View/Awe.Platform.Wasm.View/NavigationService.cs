﻿using Abp.Dependency;
using Abp.Domain.Services;
using Abp.Events.Bus.Handlers;
using Awe.Core.Action.Contracts.EventData;

namespace Awe.Platform.Wasm.View
{
    public class NavigationService : DomainService, IAsyncEventHandler<NavigateEventData>
    {
        private NavigationManager NavManager { get; } = IocManager.Instance.Resolve<NavigationManager>();

        public Task HandleEventAsync(NavigateEventData eventData)
        {
            if (NavManager == null)
            {
                throw new ApplicationException("NavManager");
            }

            var rowIdSuffix = eventData.RowId.HasValue
                ? eventData.RowId.Value.ToString()
                : string.Empty;

            var uri = $"/app/viewer/{eventData.PageType}/{eventData.Alias}/{rowIdSuffix}";

            NavManager.NavigateTo(uri);

            return Task.CompletedTask;
        }
    }
}
