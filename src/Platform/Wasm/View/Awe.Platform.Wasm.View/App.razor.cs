using Abp.Dependency;
using Abp.Events.Bus.Handlers;
using Awe.Core.Action.Contracts.EventData;
using Castle.MicroKernel.Registration;

namespace Awe.Platform.Wasm.View;

public partial class App : IAsyncEventHandler<NavigateEventData>
{
    protected override void OnInitialized()
    {
        base.OnInitialized();

        IocManager.Instance.IocContainer.Register(Component
            .For<NavigationManager>()
            .Instance(NavManager));
    }

    public Task HandleEventAsync(NavigateEventData eventData)
    {
        if (NavManager == null)
        {
            throw new ApplicationException("NavManager");
        }

        var rowIdSuffix = eventData.RowId.HasValue
            ? eventData.RowId.Value.ToString()
            : string.Empty;

        var uri = $"/app/viewer/{eventData.PageType}/{rowIdSuffix}";

        NavManager.NavigateTo(uri);

        return Task.CompletedTask;
    }
}