using Abp.Dependency;
using Abp.Domain.Services;
using Awe.Core.Widget.Contracts.Domain;

namespace Awe.Platform.Wasm.Domain.App;

public class ApplicationWasmDomainService : DomainService, IWApplicationDomainService, ISingletonDependency
{
    public void Start(string[] args)
    {
        throw new NotImplementedException();
    }

    public async Task StartAsync(string[] args)
    {
    }

    public void Restart()
    {
        throw new NotImplementedException();
    }

    public string ProductInfo { get; }
}