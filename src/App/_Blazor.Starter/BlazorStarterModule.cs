using Abp.Modules;
using Awe.Core.Data.Impl;
using Awe.Core.Widget.Domain;
using Awe.DataClients.Api.Impl;
using Awe.DataClients.File.Impl;
using Awe.Platform.Wasm.Domain;
using Awe.Platform.Wasm.View;

namespace _Blazor.Starter;

[DependsOn(typeof(AweCoreDataImplModule))]
[DependsOn(typeof(AweCoreWidgetImplModule))]

/* Platform Wasm realization */
[DependsOn(typeof(PlatformWasmDomainModule))]
[DependsOn(typeof(PlatformWasmViewModule))]
/* Platform Wasm realization */

/* Develop DataClients realization */
[DependsOn(typeof(AweDataClientsApiImplModule))]
[DependsOn(typeof(AweDataClientsFileImplModule))]
/* Develop DataClients realization */

public class BlazorStarterModule : AbpModule
{
}