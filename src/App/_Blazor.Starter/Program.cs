using _Blazor.Starter;
using Abp;
using Awe.Platform.Wasm.View;

using var bootstrapper = AbpBootstrapper.Create<BlazorStarterModule>();

//bootstrapper.IocManager.IocContainer.AddFacility<LoggingFacility>(f =>
//    f.UseAbpLog4Net().WithConfig("log4net.config"));

bootstrapper.Initialize();

bootstrapper.IocManager
    .Resolve<IStarter>()
    .StartAsync(args);