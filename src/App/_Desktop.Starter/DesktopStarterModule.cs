using Abp.Modules;

namespace Awe.Desktop.Starter;

/* Platform Win realization */
[DependsOn(typeof(PlatformWinDomainModule))]
[DependsOn(typeof(PlatformWinViewModule))]
/* Platform Win realization */

public class DesktopStarterModule : AbpModule
{
}