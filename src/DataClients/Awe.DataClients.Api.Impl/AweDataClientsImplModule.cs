﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Awe.Core.Data.Contracts;
using Awe.DataClients.Api.Impl.AutoMapper;

namespace Awe.DataClients.Api.Impl;

[DependsOn(typeof(AbpAutoMapperModule))]
[DependsOn(typeof(AweCoreDataContractsModule))]
[DependsOn(typeof(AweDataClientsApiContractsModule))]
public class AweDataClientsApiImplModule : AbpModule
{
    public override void Initialize()
    {
        IocManager.RegisterAssemblyByConvention(GetType().GetAssembly());

        Configuration.Modules.AbpAutoMapper().Configurators.Add(
            // Scan the assembly for classes which inherit from AutoMapper.Profile
            cfg =>
            {
                var enumerableOfProfiles = new[] { new SettingsMapProfile() };
                cfg.AddProfiles(enumerableOfProfiles);
            });
    }
}