﻿using System;
using System.Net;
using System.Web;

namespace Awe.DataClients.Api.Impl.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при работе с <see cref="T:TGrant.Core.DAL.AweClient" />.
    /// </summary>
    [Serializable]
    public class ApiClientException : HttpException
    {
        /// <summary>
        /// Инициализирует экземпляр <see cref="T:TGrant.Core.DAL.AweClient.ApiClientException" />.
        /// </summary>
        public ApiClientException(string errorMessage)
            : base(HttpStatusCode.InternalServerError, errorMessage)
        {
        }

        /// <summary>
        /// Инициализирует экземпляр <see cref="T:TGrant.Core.DAL.AweClient.ApiClientException" />.
        /// </summary>
        public ApiClientException(string errorMessage, Exception innerException)
            : base(HttpStatusCode.InternalServerError, errorMessage, innerException)
        {
        }

        /// <summary>
        /// Инициализирует экземпляр <see cref="T:TGrant.Core.DAL.AweClient.ApiClientException" />.
        /// </summary>
        public ApiClientException(string errorMessage, int statusCode)
            : base((HttpStatusCode)statusCode, errorMessage)
        {
        }
    }
}