﻿using Abp;
using Abp.Dependency;
using Awe.Contracts.Settings.Enums.Action;
using Awe.Contracts.Settings.Enums.Viewer;
using Awe.Contracts.Settings.Menu;
using Awe.Contracts.Settings.ProgramConstants;
using Awe.Contracts.Settings.ViewerBuild;
using Awe.Contracts.Settings.ViewerLinks;
using Awe.Core.Common.Contracts.Configuration;
using Awe.DataClients.Api.Contracts.Api;

namespace Awe.DataClients.Api.Impl.ApiClients
{
    public class SettingsClient : ApiClientBase, ISettingsClient, IShouldInitialize
    {
        private readonly AppSettings _appSettings;
        public SettingsClient(ITokenAuthService tokenAuthService, AppSettings appSettings) 
            : base(tokenAuthService)
        {
            _appSettings = appSettings;
        }

        public void Initialize()
        {
            _httpClient = IocManager.Instance.IocContainer.Resolve<HttpClient>(nameof(_appSettings.App.Api.BaseApiUrl.AweApiSettings));
        }


        public async Task<ViewerBuildInfo> GetViewerBuildSettingsAsync(int viewerId, ViewerType viewerType = ViewerType.None)
        {
            return await GetWithParamsAsync<ViewerBuildInfo>("ViewerBuild", 
                "GetViewerBuild",
                new ApiParams
                {
                    {"id", viewerId},
                    {"viewerType", viewerType}
                });
        }

        public async Task<CrudInfo> GetCrudAsync(string alias)
        {
            return await GetWithParamsAsync<CrudInfo>("ViewerData",
                "GetCrud",
                new ApiParams
                {
                    {"viewerAlias", alias},
                });
        }

        public async Task<string> GetFullSpNameAsync(int viewerId)
        {
            return await GetWithParamsAsync<string>("ViewerData",
                "GetFullSpName",
                new ApiParams
                {
                    {"viewerId", viewerId},
                });
        }

        public async Task<ViewerInfo> GetViewerInfoAsync(int viewerId)
        {
            return await GetWithParamsAsync<ViewerInfo>("Viewer",
                "GetViewerInfo",
                new ApiParams
                {
                    {"viewerId", viewerId},
                });
        }

        public async Task<ViewerInfo> GetViewerInfoAsync(string alias, ViewerType viewerType = ViewerType.None)
        {
            return await GetWithParamsAsync<ViewerInfo>("Viewer",
                "GetViewerInfoByAlias",
                new ApiParams
                {
                    {"alias", alias},
                    {"viewerType", viewerType},
                });
        }

        public async Task<ViewerFullInfo> GetViewerFullInfoAsync(int viewerId)
        {
            return await GetWithParamsAsync<ViewerFullInfo>("ViewerBuild",
                "GetViewerFullInfo",
                new ApiParams
                {
                    {"viewerId", viewerId}
                });
        }

        public async Task<bool> IsFilterFormExistsAsync(int viViewerId)
        {
            return await GetWithParamsAsync<bool>("ViewerBuild",
                "IsFilterFormExists",
                new ApiParams
                {
                    {"viewerId", viViewerId},
                });
        }

        public async Task<ViewerLinkInfo> GetViewerLinkAsync(int viewerId, ActionType actionType, string curDbColumnName = null)
        {
            return await GetWithParamsAsync<ViewerLinkInfo>("ViewerLink",
                "GetViewerLink",
                new ApiParams
                {
                    { "viewerId", viewerId },
                    { "actionType", actionType },
                    { "curDbColumnName", curDbColumnName },
                });
        }

        public async Task<ViewerLinkInfo[]> GetCtxMenuViewerLinksAsync(int viewerId)
        {
            return await GetWithParamsAsync<ViewerLinkInfo[]>("ViewerLink",
                "GetCtxMenuViewerLinks",
                new ApiParams
                {
                    { "viewerId", viewerId }
                });
        }

        public async Task<ProgramConstInfo> GetProgramConstantsAsync()
        {
            var result = await GetWithParamsAsync<ProgramConstInfo>("ProgramConst",
                "GetProgramConsts");

            return result;
        }

        public async Task<string> GetCustomProgramConst(string constName)
        {
            return await GetWithParamsAsync<string>("ProgramConst",
                "GetCustomProgramConst",
                new ApiParams
                {
                    { "key", constName }
                });
        }

        public async Task<MenuItemInfo[]> LoadMainMenuAsync(int userId)
        {
            return await GetWithParamsAsync<MenuItemInfo[]>("Menu",
                "GetMainMenu",
                new ApiParams
                {
                    { "userId", userId }
                });
        }

        public async Task<MenuItemInfo[]> LoadSideMenuAsync(int userId)
        {
            return await GetWithParamsAsync<MenuItemInfo[]>("Menu",
                "GetSideMenu",
                new ApiParams
                {
                    { "userId", userId }
                });
        }
    }
}
