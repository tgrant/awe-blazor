﻿using System.Net;
using Abp.Domain.Services;
using Awe.Core.Data.Contracts.DataServices;
using Awe.DataClients.Api.Contracts.Api;
using Awe.DataClients.Api.Contracts.Contracts;
using Awe.DataClients.Api.Impl.Exceptions;
using Castle.Core.Internal;
using Castle.Core.Logging;
using Flurl;
using Newtonsoft.Json;

namespace Awe.DataClients.Api.Impl.ApiClients
{
    /// <summary>Класс клиента WebAPI</summary>
    public abstract class ApiClientBase : DomainService, IApiClient
    {
        private readonly ITokenAuthService _tokenAuthService;
        protected HttpClient _httpClient;

        /// <summary>
        /// Инициализирует экземпляр <see cref="T:Awe.DataClients.Api.Impl.ApiClients.ApiClientBase" />.
        /// </summary>
        /// <param name="tokenAuthService"></param>
        /// <param name="restClient"></param>
        /// <param name="configManager"></param>
        public ApiClientBase(ITokenAuthService tokenAuthService)
        {
            _tokenAuthService = tokenAuthService;

            Logger = NullLogger.Instance;
        }

        /// <inheritdoc />
        public async Task<Stream> GetStreamAsync(
          string apiActionName,
          IEnumerable<string> cookie,
          string token,
          CancellationToken cancellation = default(CancellationToken))
        {
            throw new NotImplementedException(nameof(GetStreamAsync));

            //TODO подключить стримы в .net core 6
            #region Skip

            //Stream stream;
            //try
            //{
            //    using (var response = await restClient.SendAsync(CreateGetRequest(apiActionName, cookie, token), cancellation))
            //    {
            //        if (!response.IsSuccessStatusCode)
            //            throw CreateException(await response.Content.ReadAsStringAsync(), (int)response.StatusCode);
            //        stream = await response.Content.ReadAsStreamAsync();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw this.WrapException(apiActionName, ex);
            //}
            //return stream;

            #endregion
        }

        /// <inheritdoc />
        public async Task<T> GetAsync<T>(
          string apiActionName,
          IDictionary<string, string> cookie = null,
          string? token = null,
          CancellationToken cancellation = default)
        {
            T? result;
            try
            {
                var requestHeaders = _httpClient.DefaultRequestHeaders;

                if (!requestHeaders.Contains("Authorization"))
                {
                    token = !token.IsNullOrEmpty()
                        ? token
                        : await GetTokenBasedAuthAsync();

                    requestHeaders.Add("Authorization", $"Bearer {token}");
                }

                var response = await _httpClient.GetAsync(apiActionName, cancellation);

                return await ProcessResponseAsync<T>(apiActionName, response);
            }
            catch (ApiClientException ex)
            {
                //TODO добавить структурное логирование
                Logger.Error($"При вызове {nameof(GetAsync)} произошла ошибка. [apiActionName]: {apiActionName}", ex);
                throw;
            }
            catch (HttpRequestException ex) // Non success
            {
                Console.WriteLine("An error occurred.");
                throw WrapException(apiActionName, ex);
            }
            catch (NotSupportedException ex) // When content type is not valid
            {
                Console.WriteLine("The content type is not supported.");
                throw WrapException(apiActionName, ex);
            }
            catch (JsonException ex) // Invalid JSON
            {
                Console.WriteLine("Invalid JSON.");
                throw WrapException(apiActionName, ex);
            }
            catch (Exception ex)
            {
                //TODO добавить структурное логирование
                Logger.Error($"При вызове {nameof(GetAsync)} произошла ошибка. [apiActionName]: {apiActionName}", ex);
                throw WrapException(apiActionName, ex);
            }

            return result;
        }

        private async Task<T> ProcessResponseAsync<T>(string apiActionName, HttpResponseMessage response)
        {
            var readAsStringAsync = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                if (!readAsStringAsync.IsNullOrEmpty())
                {
                    var content = JsonConvert.DeserializeObject<AbpResult<T>>(readAsStringAsync);
                    Logger.Error($"Ошибка при вызове api\r\n" +
                                 $"[ErrorMessage]: {content?.Error?.Message}\r\n" +
                                 $"[ErrorMessageDetails]: {content?.Error?.Message}\r\n" +
                                 $"[StatusCode]: {response.StatusCode}" +
                                 $"[ReasonPhrase]: {response.ReasonPhrase}" + 
                                 $"[RequestMessage]: {response.RequestMessage}");

#if DEBUG
                    throw WrapException(apiActionName, new Exception(content?.Error?.Message, new Exception(content?.Error?.Details)));

#else
                    throw WrapException(apiActionName, new Exception(content?.Error?.Message));
#endif
                }
                else
                {
                    Logger.Error($"Ошибка при вызове api\r\n" +
                                 $"[StatusCode]: {response.StatusCode}" +
                                 $"[ReasonPhrase]: {response.ReasonPhrase}" +
                                 $"[RequestMessage]: {response.RequestMessage}");


                    throw new ApiClientException("Ошибка при вызове api!\r\n" +
                                                     "Убедитесь, что API запущен!\r\n" +
                                                     "Или обратитесь к Администратору.\r\n\r\n" +
                                                     $"[StatusCode]: {response.StatusCode}");
                }
            }

            var data = JsonConvert.DeserializeObject<AbpResult<T>>(readAsStringAsync);

            if (data == null)
            {
                //TODO обогатить сообщение
                Logger.Error("Данные от сервера пусты");
                throw new ApiClientException("Данные от сервера пусты");
            }

            var dataResult = data.Result;
            return dataResult;
        }

        //        private T ProcessResponse<T>(string apiActionName, RestResponse response)
        //        {
        //            if (response.StatusCode != HttpStatusCode.OK)
        //            {
        //                if (!response.Content.IsNullOrEmpty())
        //                {
        //                    var content = JsonConvert.DeserializeObject<AbpResult<T>>(response.Content);
        //                    Logger.Error($"Ошибка при вызове api\r\n" +
        //                                 $"[ErrorMessage]: {content.Error?.Message}\r\n" +
        //                                 $"[ErrorMessageDetails]: {content.Error?.Message}\r\n" +
        //                                 $"[StatusCode]: {response.StatusCode}" +
        //                                 $"[ResponseUri]: {response.ResponseUri}"
        //                        , response.ErrorException);

        //#if DEBUG
        //                    throw WrapException(apiActionName, new Exception(content?.Error?.Message,new Exception(content?.Error?.Details)));

        //#else
        //                    throw WrapException(apiActionName, new Exception(content?.Error?.Message));
        //#endif
        //                }
        //                else
        //                {
        //                    Logger.Error($"Ошибка при вызове api\r\n" +
        //                                 $"[ErrorMessage]: {response.ErrorMessage}\r\n" +
        //                                 $"[ErrorMessageDetails]: {response.ErrorException?.InnerException}\r\n" +
        //                                 $"[StatusCode]: {response.StatusCode}\r\n" +
        //                                 $"[ResponseUri]: {response.ResponseUri}\r\n" + 
        //                                 $"[Parameters]: {JsonConvert.SerializeObject(response.Request.Parameters)}"
        //                        , response.ErrorException);

        //                    throw new ApiClientException("Ошибка при вызове api!\r\n" +
        //                                                     "Убедитесь, что API запущен!\r\n" +
        //                                                     "Или обратитесь к Администратору.\r\n\r\n" +
        //                                                     $"[StatusCode]: {response.StatusCode}");
        //                }
        //            }

        //            var data = JsonConvert.DeserializeObject<AbpResult<T>>(response.Content);

        //            if (data == null)
        //            {
        //                //TODO обогатить сообщение
        //                Logger.Error("Данные от сервера пусты");
        //                throw new ApiClientException("Данные от сервера пусты");
        //            }

        //            var dataResult = data.Result;
        //            return dataResult;
        //        }

        /// <inheritdoc />
        public async Task<string> GetStringAsync(
            string apiActionName,
            IDictionary<string, string> cookie = null,
            string token = null,
            CancellationToken cancellation = default)
        {
            throw new NotImplementedException();

            //try
            //{
            //    var request = CreateRequest("", apiActionName, Method.Get, cookie, token);
            //    var response = await _httpClient.ExecuteGetAsync(request, cancellation);
            //    if (response.ResponseStatus != ResponseStatus.Completed)
            //    {
            //        throw WrapException(apiActionName, response.StatusCode);
            //    }

            //    return response.Content;
            //}
            //catch (Exception ex)
            //{
            //    throw WrapException(apiActionName, ex);
            //}
        }

        /// <inheritdoc />
        public async Task<byte[]> PostRawAsync(
          string apiActionName,
          object body,
          IDictionary<string, string> cookie = null,
          string token = null,
          CancellationToken cancellation = default)
        {
            throw new NotImplementedException();

            //try
            //{
            //    var request = CreateRequest("", apiActionName, Method.Post, cookie, token, body);
            //    var response = await _httpClient.ExecutePostAsync(request, cancellation);
            //    if (response.ResponseStatus != ResponseStatus.Completed)
            //    {
            //        throw WrapException(apiActionName, response.StatusCode);
            //    }

            //    return response.RawBytes;
            //}
            //catch (Exception ex)
            //{
            //    throw WrapException(apiActionName, ex);
            //}
        }

        /// <inheritdoc />
        public async Task<T> PostAsync<T>(string apiActionName,
            object body,
            string token = null,
            IDictionary<string, string> cookie = null,
            CancellationToken cancellation = default)
        {
            throw new NotImplementedException();

            //var request = CreateRequest("", apiActionName, Method.Post, cookie, token, body);
            //try
            //{
            //    var response = await _httpClient.ExecutePostAsync<T>(request, cancellation);
            //    return ProcessResponse<T>(apiActionName, response);
            //}
            //catch (Exception ex)
            //{
            //    throw this.WrapException(apiActionName, ex);
            //}
        }

        /// <inheritdoc />
        public async Task<T> PutAsync<T>(
          string apiActionName,
          object body,
          IDictionary<string, string> cookie = null,
          string token = null,
          CancellationToken cancellation = default)
        {
            throw new NotImplementedException();

            //var request = CreateRequest("", apiActionName, Method.Put, cookie, token, body);
            //try
            //{
            //    var response = await _httpClient.ExecuteAsync<T>(request, cancellation);
            //    return ProcessResponse<T>(apiActionName, response);
            //}
            //catch (Exception ex)
            //{
            //    throw this.WrapException(apiActionName, ex);
            //}
        }

        /// <inheritdoc />
        public async Task PostAsync(
          string apiActionName,
          object body,
          IDictionary<string, string> cookie = null,
          string token = null,
          CancellationToken cancellation = default)
        {
            throw new NotImplementedException();

            //try
            //{
            //    var request = CreateRequest("", apiActionName, Method.Post, cookie, token, body);
            //    var response = await _httpClient.ExecutePostAsync(request, cancellation);

            //    if (response.ResponseStatus != ResponseStatus.Completed)
            //    {
            //        throw WrapException(apiActionName, response.StatusCode);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw WrapException(apiActionName, ex);
            //}
        }

        /// <inheritdoc />
        public async Task<T> DeleteAsync<T>(string apiActionName,
            IDictionary<string, string> cookie = null,
            string token = null,
            CancellationToken cancellation = default)
        {
            throw new NotImplementedException();

            //var request = CreateRequest("", apiActionName, Method.Delete, cookie, token);
            //try
            //{
            //    var response = await _httpClient.ExecuteAsync<T>(request, cancellation);
            //    return ProcessResponse<T>(apiActionName, response);
            //}
            //catch (Exception ex)
            //{
            //    throw this.WrapException(apiActionName, ex);
            //}
        }

        #region Protected methodes

        protected virtual ApiClientException WrapException(
            string apiActionName,
            Exception exception)
        {
            return new ApiClientException(
                $"Произошло исключение при обращении к api {_httpClient.BaseAddress}{apiActionName} в сервисе {GetType().FullName}", 
                exception);
        }

        protected virtual ApiClientException WrapException(string apiActionName, HttpStatusCode responseStatusCode)
        {
            var  apiNotAvailableMessage = responseStatusCode == 0 ? "API недоступен, обратитесь к Администратору." : "";
            return new ApiClientException(
                $"Произошло исключение при обращении к api {_httpClient.BaseAddress}{apiActionName} в сервисе {GetType().FullName}" +
                $"\r\n[StatusCode]: {responseStatusCode}" +
                $"\r\n{apiNotAvailableMessage}" );
        }

        #endregion

        protected async Task<string> GetTokenBasedAuthAsync()
        {
            return await _tokenAuthService.GetTokenBasedAuthAsync();
        }

        protected async Task<T> GetWithParamsAsync<T>(string serviceName, string actionName,
            IApiParams queryParams = null)
        {
            var url = serviceName.AppendPathSegment(actionName);

            if (queryParams != null) url.SetQueryParams(queryParams);


            return await GetAsync<T>(url);
        }
    }
}
