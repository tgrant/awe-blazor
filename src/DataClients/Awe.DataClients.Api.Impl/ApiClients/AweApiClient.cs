﻿using Abp;
using Abp.Dependency;
using Awe.Core.Common.Contracts.Configuration;
using Awe.Core.Data.Contracts.DataServices;
using Awe.Core.Data.Contracts.Settings.Enums;
using Awe.DataClients.Api.Contracts.Api;
using Flurl;
using Newtonsoft.Json.Linq;

namespace Awe.DataClients.Api.Impl.ApiClients
{
    public class AweApiClient : ApiClientBase, IAweApiClient, IShouldInitialize
    {
        private readonly AppSettings _appSettings;

        public AweApiClient(ITokenAuthService tokenAuthService, AppSettings appSettings)
            : base(tokenAuthService)
        {
            _appSettings = appSettings;
        }

        public void Initialize()
        {
            _httpClient = IocManager.Instance.IocContainer.Resolve<HttpClient>(nameof(_appSettings.App.Api.BaseApiUrl.AweApi));
        }

        public async Task<IEnumerable<T>?> GetViewerDataAsync<T>(ApiSource apiSource, string viewerName, string verb, IApiParams queryParams)
        {
            return await GetWithParamsAsync<List<T>>(viewerName, verb, queryParams);
        }

        public async Task<T?> GetViewerDataSingleAsync<T>(ApiSource apiSource, string viewerName, string verb, IApiParams queryParams)
        {
            return await GetWithParamsAsync<T>(viewerName, verb, queryParams);
        }

        public async Task<JObject> InsertRowDataAsync(ApiSource apiSource, string viewerName, string verb, IApiParams apiParams)
        {
            var url = apiSource.ToString()
                .AppendPathSegment(viewerName)
                .AppendPathSegment(verb);

            var token = await GetTokenBasedAuthAsync();

            return await PostAsync<JObject>(url, apiParams.Dto, token, null);
        }

        public async Task<JObject> UpdateRowDataAsync(ApiSource apiSource, string viewerName, string verb,
            IApiParams apiParams)
        {
            var url = apiSource.ToString()
                .AppendPathSegment(viewerName)
                .AppendPathSegment(verb);

            var token = await GetTokenBasedAuthAsync();
            IDictionary<string, string> cookies = null;
            //    new Dictionary<string, string>
            //{
            //    //{_antiForgeryManager.Configuration.TokenCookieName, _antiForgeryManager.GenerateToken()}
            //    //{_antiForgeryManager.Configuration.TokenCookieName, GetAntiForgeryToken()}
            //};

            return await PutAsync<JObject>(url, apiParams.Dto, cookies, token);
        }

        public async Task<JObject> DeleteRowDataAsync(ApiSource apiSource, string viewerName, string actionName,
            IApiParams queryParams)
        {
            var url = apiSource.ToString()
                .AppendPathSegment(viewerName)
                .AppendPathSegment(actionName);

            url.SetQueryParams(queryParams);

            var token = await GetTokenBasedAuthAsync();
            IDictionary<string, string> cookies = null;

            return await DeleteAsync<JObject>(url, null,token);
        }

        public async Task<JObject> ExecDataAsync(ApiSource apiSource, string viewerName, string verb,
            IApiParams apiParams)
        {
            var url = apiSource.ToString()
                .AppendPathSegment(viewerName)
                .AppendPathSegment(verb);

            url.SetQueryParams(apiParams);

            var token = await GetTokenBasedAuthAsync();

            return await PostAsync<JObject>(url, apiParams.Dto, token);
        }
    }
}
