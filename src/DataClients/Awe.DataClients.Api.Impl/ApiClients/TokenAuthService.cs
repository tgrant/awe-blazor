﻿using Abp;
using Abp.Domain.Services;
using Abp.Extensions;
using Awe.Core.Data.Contracts.Settings.Enums;
using Awe.DataClients.Api.Contracts.Api;
using Awe.DataClients.Api.Contracts.Contracts;
using System.Diagnostics;
using System.Net.Http.Json;
using System.Text.Json;
using Abp.Dependency;
using Awe.Core.Common.Contracts.Configuration;

namespace Awe.DataClients.Api.Impl.ApiClients
{
    public class TokenAuthService : DomainService, ITokenAuthService, IShouldInitialize
    {
        private string _accessToken;
        private AuthenticateDto _authenticateDto;

        private readonly HttpClient _httpClient;
        private readonly AppSettings _appSettings;

        public TokenAuthService(AppSettings appSettings)
        {
            _appSettings = appSettings;
            _httpClient = IocManager.Instance.IocContainer.Resolve<HttpClient>(nameof(_appSettings.App.Api.BaseApiUrl.AweApiAuth));
        }

        public void Initialize()
        {
            var apiOptions = _appSettings.App.Api;
            _authenticateDto = new AuthenticateDto
            {
                UserNameOrEmailAddress = apiOptions.ApiUser.Default.UserName,
                Password = apiOptions.ApiUser.Default.UserPass,
                RememberClient = true
            };
        }

        public async Task<string> GetTokenBasedAuthAsync()
        {
            if (!_accessToken.IsNullOrEmpty()) return _accessToken;

            try
            {
                var response = await _httpClient.PostAsJsonAsync("TokenAuth/Authenticate", _authenticateDto);

                if (response.IsSuccessStatusCode)
                {
                    Debug.WriteLine(@"\tTodoItem successfully saved.");

                    var resultDto = await response.Content.ReadFromJsonAsync<AbpResult<AuthenticateResultModel>>();

                    Debug.Assert(resultDto != null, nameof(resultDto) + " != null");

                    _accessToken = resultDto.Result.AccessToken;

                    return _accessToken;
                }
            }
            catch (HttpRequestException) // Non success
            {
                Console.WriteLine("An error occurred.");
            }
            catch (NotSupportedException) // When content type is not valid
            {
                Console.WriteLine("The content type is not supported.");
            }
            catch (JsonException) // Invalid JSON
            {
                Console.WriteLine("Invalid JSON.");
            }

            return _accessToken;
        }
    }
}
