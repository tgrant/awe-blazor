﻿using Awe.Core.Data.Contracts.DataServices;

namespace Awe.DataClients.Api.Impl.ApiClients
{
    public class ApiParams : Dictionary<string, object>, IApiParams
    {
        public int? KeyVal { get; set; }
        public object Dto { get; set; }
    }
}