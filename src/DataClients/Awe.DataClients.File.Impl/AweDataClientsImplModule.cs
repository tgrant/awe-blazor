﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Awe.DataClients.Contracts;
using Awe.DataClients.File.Impl.AutoMapper;

namespace Awe.DataClients.File.Impl;

[DependsOn(typeof(AbpAutoMapperModule))]
[DependsOn(typeof(AweDataClientsContractsModule))]
public class AweDataClientsFileImplModule : AbpModule
{
    public override void Initialize()
    {
        IocManager.RegisterAssemblyByConvention(GetType().GetAssembly());

        Configuration.Modules.AbpAutoMapper().Configurators.Add(
            // Scan the assembly for classes which inherit from AutoMapper.Profile
            cfg =>
            {
                var enumerableOfProfiles = new[] { new SettingsMapProfile() };
                cfg.AddProfiles(enumerableOfProfiles);
            });
    }
}