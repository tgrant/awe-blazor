﻿using System.Collections.ObjectModel;
using System.Net.Http.Json;
using Abp;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Services;
using Awe.Contracts.Settings.Enums.Action;
using Awe.Contracts.Settings.Enums.Viewer;
using Awe.Contracts.Settings.Menu;
using Awe.Contracts.Settings.ProgramConstants;
using Awe.Contracts.Settings.ViewerBuild;
using Awe.Contracts.Settings.ViewerLinks;
using Awe.Core.Common.Contracts;
using Awe.Core.Common.Contracts.Configuration;
using Awe.DataClients.Contracts.Dto.Info;
using Awe.DataClients.Contracts.Settings;

namespace Awe.DataClients.File.Impl.Settings;

public class FileSettingsClient : DomainService, ISettingsClient, IShouldInitialize
{
    private readonly AppSettings _appSettings;
    private const string FileViewerPath = "viewer.json";

    private IReadOnlyCollection<ViewerFullInfo> _viewers;
    private HttpClient _httpClient;

    public FileSettingsClient(AppSettings appSettings)
    {
        _appSettings = appSettings;
    }

    public void Initialize()
    {
        _httpClient = IocManager.Instance.IocContainer.Resolve<HttpClient>("viewer");
    }

    public Task<CrudInfo> GetCrudAsync(string alias)
    {
        throw new NotImplementedException();
    }

    public Task<string> GetFullSpNameAsync(int viewerId)
    {
        throw new NotImplementedException();
    }

    public Task<ViewerInfo> GetViewerInfoAsync(int viewerId)
    {
        throw new NotImplementedException();
    }

    public Task<ViewerFullInfo> GetViewerFullInfoAsync(int viewerId)
    {
        throw new NotImplementedException();
    }

    public Task<bool> IsFilterFormExistsAsync(int viViewerId)
    {
        throw new NotImplementedException();
    }

    public Task<ViewerLinkInfo> GetViewerLinkAsync(int viewerId, ActionType actionType, string curDbColumnName = null)
    {
        throw new NotImplementedException();
    }

    public Task<ViewerLinkInfo[]> GetCtxMenuViewerLinksAsync(int viewerId)
    {
        throw new NotImplementedException();
    }

    public Task<ProgramConstInfo> GetProgramConstantsAsync()
    {
        throw new NotImplementedException();
    }

    public Task<string> GetCustomProgramConst(string constName)
    {
        throw new NotImplementedException();
    }

    public Task<MenuItemInfo[]> LoadMainMenuAsync(int userId)
    {
        throw new NotImplementedException();
    }

    public Task<MenuItemInfo[]> LoadSideMenuAsync(int userId)
    {
        throw new NotImplementedException();
    }

    public Task<IEnumerable<PageInfo>> GetAllPages()
    {
        throw new NotImplementedException();
    }


    // --------------------------------------------------------------------

    public async Task<ViewerInfo> GetViewerInfoAsync(string alias, PageType pageType)
    {
        var listTypes = new List<ViewerType> { ViewerType.Book, ViewerType.DoubleBook, ViewerType.TreeBook };
        var cardTypes = new List<ViewerType> { ViewerType.Card, ViewerType.SimpleCard, ViewerType.DoubleCard };

        var viewers = (await GetViewers())
                .Where(v => alias.Equals(v.Alias, StringComparison.OrdinalIgnoreCase))
                .WhereIf(pageType == PageType.Book, v => listTypes.Contains(v.ViewerType))
                .WhereIf(pageType == PageType.Card, v => cardTypes.Contains(v.ViewerType))
                .ToArray()
            ;

        if (viewers?.Length != 1)
        {
            throw new ArgumentOutOfRangeException($"viewers count = {viewers?.Length}");
        }

        return ObjectMapper.Map<ViewerInfo>(viewers.Single());
    }

    public async Task<IEnumerable<ViewerFullInfo>> GetViewerBuildSettingsAsync(int viewerId, PageType pageType)
    {
        var viewers = await GetViewers();

        return viewers
            .Where(x => x.Id == viewerId || x.MainViewerId == viewerId);
    }

    public async Task<IEnumerable<Page>> LoadAllPages()
    {
        List<Page> pages;
        try
        {
            pages = await _httpClient.GetFromJsonAsync<List<Page>>("viewer.json") 
                    ?? throw new InvalidOperationException();
        }
        catch (Exception e)
        {
            throw;
        }

        return pages;
    }

    private async Task<IReadOnlyCollection<ViewerFullInfo>?> GetViewers()
    {
        if (_viewers != null && _viewers.Count != 0) return _viewers;

        List<ViewerFullInfo> viewerFullInfos;
        try
        {
            viewerFullInfos = await _httpClient.GetFromJsonAsync<List<ViewerFullInfo>>(FileViewerPath) 
                              ?? throw new InvalidOperationException();
        }
        catch (Exception e)
        {
            throw;
        }

        _viewers = new ReadOnlyCollection<ViewerFullInfo>(viewerFullInfos);

        return _viewers;
    }
}