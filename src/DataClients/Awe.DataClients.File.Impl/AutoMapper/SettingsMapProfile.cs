﻿using Awe.Contracts.Settings.ViewerBuild;
using AutoMapper;
using Awe.DataClients.Contracts.Dto.Info;

namespace Awe.DataClients.File.Impl.AutoMapper
{
    public class SettingsMapProfile : Profile
    {
        public SettingsMapProfile()
        {
            CreateMap<Page, PageInfo>();
                
            CreateMap<Page.Viewer, PageInfo.ViewerInfo>();

            CreateMap<Page.Viewer.Link, PageInfo.ViewerInfo.LinkInfo>();
            CreateMap<Page.Viewer.Link.LinkAction, PageInfo.ViewerInfo.LinkInfo.LinkActionInfo>();
            CreateMap<Page.Viewer.Link.LinkRule, PageInfo.ViewerInfo.LinkInfo.LinkRuleInfo>();
            CreateMap<Page.Viewer.Link.LinkRule.Store, PageInfo.ViewerInfo.LinkInfo.LinkRuleInfo.StoreInfo>();

            CreateMap<Page.Viewer.Column, PageInfo.ViewerInfo.ColumnInfo>();

            CreateMap<Page.Viewer.DbSource, PageInfo.ViewerInfo.DbSourceInfo>();
            CreateMap<Page.Viewer.DbSource.Crud, PageInfo.ViewerInfo.DbSourceInfo.CrudInfo>();
            CreateMap<Page.Viewer.DbSource.SaveCrud, PageInfo.ViewerInfo.DbSourceInfo.SaveCrudInfo>();
            CreateMap<Page.Viewer.DbSource.Crud.CrudParam, PageInfo.ViewerInfo.DbSourceInfo.CrudInfo.CrudParamInfo>();

            CreateMap<PageInfo.ViewerInfo, ViewerListInfo>();
            CreateMap<PageInfo.ViewerInfo, ViewerDCardInfo>();

            CreateMap<ViewerFullInfo, ViewerInfo>();
            CreateMap<ViewerFullInfo, ViewerListInfo>();
            CreateMap<ViewerFullInfo, ViewerDCardInfo>();
            CreateMap<PageInfo.ViewerInfo.ColumnInfo, CColumnInfo>();
        }
    }
}
