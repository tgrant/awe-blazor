﻿using System;

namespace Awe.Inspector.Contracts.ExamDemandDto
{
    //DTO для получения ExamDemand
    public class InspectorExamDemandInfo
    {
        public int Id { get; set; }

        //Номер заявки
        public string Number { get; set; }

        //Номер графика, к которому привязана заявка
        public string ScheduleNumber { get; set; }

        //Наименование ЧОП
        public string ChopName { get; set; }

        //Предпочтительное НОУ
        public string PreferredNouName { get; set; }

        //Предпочтительная дата экзамена
        public DateTime? PreferredExamDate { get; set; }

        //Примечание
        public string Description { get; set; }

        //Количество тэйкеров
        public int TakersCount { get; set; }

        //Статус отправки на рассмотрение
        public string Status { get; set; }

        //Дата отправки заявки из ЧОПа
        public DateTime ChopSentDate { get; set; }

        //Флаг возможности создания графика
        public bool CanCreateSchedule { get; set; }
    }
}