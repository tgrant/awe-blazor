﻿using System;

namespace Awe.Inspector.Contracts.ExamDemandTakerDto
{
    //DTO для обновления статуса ExamDemandTaker
    public class InspectorUpdateExamDemandTakerDto 
    {
        public int Id { get; set; }

        //Cтатус подтверждения тэйкера
        public string Status { get; set; }
    }
}