﻿namespace Awe.Inspector.Contracts.ExamScheduleDto
{
    public class ExamScheduleDetailDto
    {
        //ID графика
        public int ScheduleId { get; set; }

        //ID заявки
        public int DemandId { get; set; }
    }
}