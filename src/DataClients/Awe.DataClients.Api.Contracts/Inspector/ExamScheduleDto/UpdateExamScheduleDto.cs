﻿using System;

namespace Awe.Inspector.Contracts.ExamScheduleDto
{
    public class UpdateExamScheduleDto
    {
        public int Id { get; set; }

        //Наименование НОУ
        public string NouName { get; set; }

        //Дата экзамена
        public DateTime ExamDate { get; set; }

        //Примечание
        public string Description { get; set; }
    }
}