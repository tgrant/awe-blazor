﻿using System;

namespace Awe.Inspector.Contracts.ExamScheduleDto
{
    public class ExamScheduleInfo
    {
        public int Id { get; set; }

        //Номер графика
        public string Number { get; set; }

        //Дата создания графика
        public DateTime CreatedDate { get; set; }

        //Статус графика
        public string Status { get; set; }

        //Наименование НОУ
        public string NouName { get; set; }

        //Дата экзамена
        public DateTime ExamDate { get; set; }

        //Примечание
        public string Description { get; set; }

        //Статус отправки графика в НОУ
        public bool IsSubmitted { get; set; }

        //Дата проведения
        public DateTime? SubmitDate { get; set; }

        //Название ЧОПа
        public string ChopName { get; set; }

        //Количество заявок в графике
        public int DemandsCount { get; set; }

        //Количество тэйкеров
        public int TakersCount { get; set; }
    }
}