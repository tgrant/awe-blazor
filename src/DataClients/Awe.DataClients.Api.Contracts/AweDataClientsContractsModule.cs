﻿using Abp.Modules;
using Awe.Core.Data.Contracts;

[DependsOn(typeof(AweCoreDataContractsModule))]
public class AweDataClientsApiContractsModule : AbpModule
{
}