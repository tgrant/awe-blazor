﻿using System.Collections.Generic;

namespace Awe.Contracts.Report
{
    public class ReportRenderDto
    {
        public ReportRenderDto(int viewerId, string templateFileName, string baseDirShortName, int keyVal, 
            Dictionary<string, object> apiParams, 
            int? keyVal4Storage = null, bool rewriteDoc = false, string alias = "", bool ooPrintOnly = false)
        {
            ViewerId = viewerId;
            TemplateFileName = templateFileName;
            BaseDirShortName = baseDirShortName;
            KeyVal = keyVal;
            ApiParams = apiParams;
            KeyVal4Storage = keyVal4Storage;
            RewriteDoc = rewriteDoc;
            Alias = alias;
            OOPrintOnly = ooPrintOnly;
            //OpenDoc = openDoc;
        }

        public int ViewerId { get; set; }
        public string TemplateFileName { get; set; }
        public string BaseDirShortName { get; set; }
        public int KeyVal { get; set; }
        public Dictionary<string, object> ApiParams { get; set; }
        public int? KeyVal4Storage { get; set; }
        public bool RewriteDoc { get; set; }
        public string Alias { get; set; }
        public bool OOPrintOnly { get; set; }
    }
}