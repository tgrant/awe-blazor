﻿namespace Awe.Contracts.Report
{
    public class ReportOutput
    {
        public string EndPath { get; set; }
        public byte[] Content { get; set; }
    }
}