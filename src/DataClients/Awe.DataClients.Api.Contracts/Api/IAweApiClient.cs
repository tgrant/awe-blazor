﻿using System.Threading.Tasks;
using Awe.Core.Data.Contracts.DataServices;
using Awe.Core.Data.Contracts.Settings.Enums;
using Awe.DataClients.Contracts.DataClients;
using Newtonsoft.Json.Linq;

namespace Awe.DataClients.Api.Contracts.Api
{
    public interface IAweApiClient : IApiClient, IDataClient
    {
        Task<JObject> InsertRowDataAsync(ApiSource apiSource, string viewerName, string verb, IApiParams apiParams);
        Task<JObject> UpdateRowDataAsync(ApiSource apiSource, string viewerName, string verb, IApiParams apiParams);
        Task<JObject> DeleteRowDataAsync(ApiSource apiSource, string viewerName, string verb, IApiParams apiParams);

        Task<JObject> ExecDataAsync(ApiSource apiSource, string viewerName, string verb, IApiParams apiParams);

    }
}