﻿using System.Threading.Tasks;
using Awe.Contracts.Settings.Enums;

namespace Awe.DataClients.Api.Contracts.Api
{
    public interface IAuditClient
    {
        Task SaveErrorAsync(int errorCode, int userId, int viewerId, int? rowId, int? masterRowId, CrudMode read, string errorMessage);
        Task SaveHistoryAsync(int userId, int viewerId, int? rowId, int? masterRowId, CrudMode crudMode, string data);
    }
}