﻿using System.Collections.Generic;
using Awe.Core.Data.Contracts.DataServices;

namespace Awe.DataClients.Api.Contracts.Api
{
    public class ApiParams : Dictionary<string, object>, IApiParams
    {
        public int? KeyVal { get; set; }
        public object Dto { get; set; }
    }
}