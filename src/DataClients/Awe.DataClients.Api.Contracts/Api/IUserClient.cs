﻿using System.Threading.Tasks;
using Awe.Application.Settings.Users;

namespace Awe.DataClients.Api.Contracts.Api
{
    public interface IUserClient
    {
        Task<UserInfo> GetRegisteredUserAsync(string login, string password, string pswHash);
        Task SetApplStoreAsync(UserInfo user);
    }
}
