﻿using System.Threading.Tasks;
using Awe.Core.Data.Contracts.Settings.Enums;

namespace Awe.DataClients.Api.Contracts.Api;

public interface ITokenAuthService
{
    Task<string> GetTokenBasedAuthAsync();
}