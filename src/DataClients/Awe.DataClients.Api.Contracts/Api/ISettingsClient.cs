﻿using System.Threading.Tasks;
using Awe.Contracts.Settings.Enums.Action;
using Awe.Contracts.Settings.Enums.Viewer;
using Awe.Contracts.Settings.Menu;
using Awe.Contracts.Settings.ProgramConstants;
using Awe.Contracts.Settings.ViewerBuild;
using Awe.Contracts.Settings.ViewerLinks;

namespace Awe.DataClients.Api.Contracts.Api
{
    public interface ISettingsClient
    {
        Task<ViewerBuildInfo> GetViewerBuildSettingsAsync(int viewerId, ViewerType viewerType = ViewerType.None);
        Task<CrudInfo> GetCrudAsync(string alias);

        Task<string> GetFullSpNameAsync(int viewerId);

        Task<ViewerInfo> GetViewerInfoAsync(int viewerId);
        Task<ViewerInfo> GetViewerInfoAsync(string alias, ViewerType viewerType = ViewerType.None);
        Task<ViewerFullInfo> GetViewerFullInfoAsync(int viewerId);

        Task<bool> IsFilterFormExistsAsync(int viViewerId);

        Task<ViewerLinkInfo> GetViewerLinkAsync(int viewerId, ActionType actionType, string curDbColumnName = null);
        Task<ViewerLinkInfo[]> GetCtxMenuViewerLinksAsync(int viewerId);
        Task<ProgramConstInfo> GetProgramConstantsAsync();
        Task<string> GetCustomProgramConst(string constName);
        Task<MenuItemInfo[]> LoadMainMenuAsync(int userId);
        Task<MenuItemInfo[]> LoadSideMenuAsync(int userId);
    }
}