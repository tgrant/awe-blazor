﻿using System.Threading.Tasks;
using Awe.Contracts.Report;

namespace Awe.DataClients.Api.Contracts.Api
{
    public interface IReportClient
    {
        Task<string> Render2FileAsync(ReportRenderDto reportRenderDto);
        Task<ReportOutput> Render2MemoryAsync(ReportRenderDto reportRenderDto);
    }
}