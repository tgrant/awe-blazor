﻿using System.Threading.Tasks;
using Awe.Contracts.Settings.ViewerData;
using Awe.Core.Data.Contracts.DataServices;
using Newtonsoft.Json.Linq;

namespace Awe.DataClients.Api.Contracts.Api;

public interface IAdoApiClient : IApiClient
{
    Task<ViewerDataDto> GetAdoDataAsync(int viewerId, IApiParams sqlParams = null,
        int? filterKeyVal = null,
        bool writeSchema = false,
        bool normalizeNames = false);
    Task<JObject> ExecAdoStoredProcAsync(int viewerId, IApiParams sqlParams);
    Task<ViewerDataDto> SaveAdoDataAsync(int viewerId, ViewerDataDto changedViewerData);
}