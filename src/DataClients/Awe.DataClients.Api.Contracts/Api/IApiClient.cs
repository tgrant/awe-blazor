﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Awe.DataClients.Api.Contracts.Api
{
    /// <summary>Интерфейс API клиента.</summary>
    public interface IApiClient
    {
        /// <summary>
        /// Выполняет GET запрос и возвращает результат в виде потока.
        /// </summary>
        /// <param name="apiActionName">Адрес, на который необходимо выполнить запрос.</param>
        /// <param name="cancellation">Токен отмены действия.</param>
        /// <param name="cookie">cookie</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        Task<Stream> GetStreamAsync(
            string apiActionName,
            IEnumerable<string> cookie,
            string token,
            CancellationToken cancellation = default(CancellationToken));

        /// <summary>Выполняет GET запрос к методу.</summary>
        /// <param name="apiActionName">Адрес, на который необходимо выполнить запрос.</param>
        /// <param name="cancellation">Токен отмены действия.</param>
        /// <param name="cookie">cookie</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        Task<T> GetAsync<T>(
            string apiActionName,
            IDictionary<string, string> cookie = null,
            string token = null,
            CancellationToken cancellation = default);

        /// <summary>
        /// Выполняет GET запрос и возвращает результат в виде строки.
        /// </summary>
        /// <param name="apiActionName">Адрес, на который необходимо выполнить запрос.</param>
        /// <param name="cancellation">Токен отмены действия.</param>
        /// <param name="cookie">cookie</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        Task<string> GetStringAsync(
            string apiActionName,
            IDictionary<string, string> cookie = null,
            string token = null,
            CancellationToken cancellation = default);

        /// <summary>Выполняет POST запрос.</summary>
        /// <param name="apiActionName">Адрес, на который необходимо выполнить запрос.</param>
        /// <param name="body">Тело запроса.</param>
        /// <param name="cookie">cookie</param>
        /// <param name="token">Токен.</param>
        /// <param name="cancellation">Токен отмены действия.</param>
        Task<byte[]> PostRawAsync(
            string apiActionName,
            object body,
            IDictionary<string, string> cookie = null,
            string token = null,
            CancellationToken cancellation = default);

        /// <summary>Выполняет POST запрос к методу.</summary>
        /// <typeparam name="T">Тип ответа.</typeparam>
        /// <param name="apiActionName">Адрес, на который необходимо выполнить запрос.</param>
        /// <param name="body">Тело запроса.</param>
        /// <param name="token">Токен.</param>
        /// <param name="cookie">cookie</param>
        /// <param name="cancellation">Токен отмены действия.</param>
        /// <returns>Результат.</returns>
        Task<T> PostAsync<T>(string apiActionName,
            object body,
            string token = null,
            IDictionary<string, string> cookie = null,
            CancellationToken cancellation = default);

        /// <summary>Выполняет POST запрос к методу.</summary>
        /// <param name="apiActionName">Адрес, на который необходимо выполнить запрос.</param>
        /// <param name="body">Тело запроса.</param>
        /// <param name="cancellation">Токен отмены действия.</param>
        /// <param name="cookie">cookie</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        Task PostAsync(
            string apiActionName,
            object body,
            IDictionary<string, string> cookie = null,
            string token = null,
            CancellationToken cancellation = default);

        /// <summary>Выполняет PUT запрос к методу.</summary>
        /// <param name="apiActionName">Адрес, на который необходимо выполнить запрос.</param>
        /// <param name="body">Тело запроса.</param>
        /// <param name="cancellation">Токен отмены действия.</param>
        /// <param name="cookie">cookie</param>
        /// <param name="token">Токен.</param>
        /// <returns>Результат.</returns>
        Task<T> PutAsync<T>(
            string apiActionName,
            object body,
            IDictionary<string, string> cookie = null,
            string token = null,
            CancellationToken cancellation = default);

        /// <summary>Выполняет DELETE запрос к методу.</summary>
        /// <param name="apiActionName">Адрес, на который необходимо выполнить запрос.</param>
        /// <param name="cookie">cookie</param>
        /// <param name="token">Токен.</param>
        /// <param name="cancellation">Токен отмены действия.</param>
        /// <returns>Результат.</returns>
        Task<T> DeleteAsync<T>(string apiActionName,
            IDictionary<string, string> cookie = null,
            string token = null,
            CancellationToken cancellation = default);
    }
}