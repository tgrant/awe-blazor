﻿using System;

namespace Awe.Taker.Contracts.Documents
{
    /// <summary>
    /// Документ тейкера
    /// </summary>
    public class TakerDocumentInfo
    {
        /// <summary>
        /// Идентификатор документа
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор тейкера
        /// </summary>
        public int TakerId { get; set; }

        /// <summary>
        /// Идентификатор типа документа
        /// </summary>
        public int TakerDocumentTypeId { get; set; }

        /// <summary>
        /// Тип документа в виде <see cref="TakerDocumentTypeEnum"/>
        /// </summary>
        public string TakerDocumentType { get; set; }

        /// <summary>
        /// Серия документа
        /// </summary>
        public string DocumentSerial { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string DocumentNumber { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTime? DocumentIssuedDate { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime? DocumentExpiredDate { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        public string DocumentDescription { get; set; }
    }
}
