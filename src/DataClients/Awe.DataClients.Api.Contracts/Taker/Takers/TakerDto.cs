﻿using System;

namespace Awe.Taker.Contracts.Takers
{
    /// <summary>
    /// Информация о тейкере
    /// </summary>
    public class TakerDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор организации
        /// </summary>
        public int ContragentId { get; set; }

        /// <summary>
        /// Название организации
        /// </summary>
        public string ContragentName { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Полное имя
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Серия паспорта
        /// </summary>
        public string PasportSerial { get; set; }

        /// <summary>
        /// Номер паспорта
        /// </summary>
        public string PasportNumber { get; set; }

        /// <summary>
        /// Дата выдачи паспорта
        /// </summary>
        public DateTime? PasportIssuedDate { get; set; }

        /// <summary>
        /// Код
        /// </summary>
        public string PassportAuthorityCode { get; set; }

        /// <summary>
        /// Инн
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// Снилс
        /// </summary>
        public string Snils { get; set; }

        /// <summary>
        /// Место рождения
        /// </summary>
        public string BirthPlace { get; set; }

        /// <summary>
        /// Пасспорт выдан
        /// </summary>
        public string PasportIssuedBy { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public string Сountry { get; set; } = "РФ";

        /// <summary>
        /// Регион
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Район
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Улица
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Дом
        /// </summary>
        public string House { get; set; }

        /// <summary>
        /// Корпус
        /// </summary>
        public string Corpus { get; set; }

        /// <summary>
        /// Квартира
        /// </summary>
        public string Apartment { get; set; }

        /// <summary>        
        /// Номер удостоверения
        /// </summary>
        public string CardNumber { get; set; }

        /// Номер удостоверения
        /// </summary>
        public string CertificationNumber { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTime? CertificationIssuedDate { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public DateTime? CertificationExpiredDate { get; set; }

        /// <summary>
        /// Идентификатор должности
        /// </summary>
        public int? PostId { get; set; }

        /// <summary>
        /// Название должность
        /// </summary>
        public string Post { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        public DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        public string TakerDescription { get; set; }

        /// <summary>
        /// Индекс Кпи(за год)
        /// </summary>
        public string IndexKpi { get; set; }

        /// <summary>
        /// Дата устройства
        /// </summary>
        public DateTime? BeginWorkDate { get; set; }

        /// <summary>
        /// Дата увольнения
        /// </summary>
        public DateTime? EndWorkDate { get; set; }

        /// <summary>
        /// Зарплата
        /// </summary>
        public int? Salary { get; set; }

        /// <summary>
        /// Добавить в ведомость
        /// </summary>
        public bool AddInPayroll { get; set; }

        /// <summary>
        /// Идентификатор разряда
        /// </summary>
        public int? GuardcatId { get; set; }

        /// <summary>
        /// Название разряд
        /// </summary>
        public string GuardcatName { get; set; }

        public DateTime? MinDocumentExpiredDate { get; set; }
    }
}
