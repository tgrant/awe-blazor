﻿using System;
using Awe.Core.Data.Contracts.DataServices;
using Newtonsoft.Json.Linq;

namespace Awe.DataClients.Api.Contracts.Contracts
{
    public class ApiResult : IApiResult
    {
        public ApiResult(JObject resultValue)
        {
            Value  = resultValue;
        }

        public ApiResult(IApiDataStore dataStore)
        {
            DataStore = dataStore;
        }

        public IApiDataStore DataStore { get; set; }
        public object Value { get; set; }
        public Exception Error { get; set; }
    }
}
