﻿namespace Awe.DataClients.Api.Contracts.Contracts
{
    public class AbpResultInnerException
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
    }
}