﻿namespace Awe.DataClients.Api.Contracts.Contracts
{
    /// <summary>
    /// TODO
    /// </summary>
    public class AbpResult<T>
    {
        public T Result { get; set; }
        public string TargetUrl { get; set; }
        public bool Success { get; set; }
        public AbpResultInnerException Error { get; set; }
        public bool UnAuthorizedRequest { get; set; }
        public bool __abp { get; set; }
    }
}