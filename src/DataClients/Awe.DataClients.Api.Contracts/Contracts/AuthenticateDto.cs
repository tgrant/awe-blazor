﻿namespace Awe.DataClients.Api.Contracts.Contracts
{
    public class AuthenticateDto
    {
        public string UserNameOrEmailAddress { get; set; }
        public string Password { get; set; }
        public bool RememberClient { get; set; }
    }
}