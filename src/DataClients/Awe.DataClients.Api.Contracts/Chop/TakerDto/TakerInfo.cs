﻿using System;

namespace Awe.Chop.Contracts.TakerDto
{
    public class TakerInfo
    {
        public int Id { get; set; }

        //Id контрагента
        public int ContragentId { get; set; }

        //Имя
        public string FirstName { get; set; }

        //Фамилия
        public string LastName { get; set; }

        //Отчество
        public string MiddleName { get; set; }

        //Полное имя
        public string FullName { get; set; }

        //Номер удостоверения
        public string CardNumber { get; set; }

        //Дата приема на работу
        public DateTime? BeginWorkDate { get; set; }

        //Дата увольнения
        public DateTime? EndWorkDate { get; set; }

        //Зарплата
        public int? Salary { get; set; }
    }
}
