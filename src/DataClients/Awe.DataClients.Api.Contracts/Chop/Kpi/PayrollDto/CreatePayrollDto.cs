﻿using System;

namespace Awe.Chop.Contracts.Kpi.PayrollDto
{
    //DTO для добавления ведомости (карточка)
    public class CreatePayrollDto
    {
        //ФИО менеджера, который последним редактировал карточку
        public string ManagerName { get; set; }

        //Наименование периода, за который производится расчет(например, Апрель 2021)
        public DateTime Date { get; set; }

        //Примечание
        public string Description { get; set; }
    }
}
