﻿using System;

namespace Awe.Chop.Contracts.Kpi.TakerIncidentDto
{
    //DTO для добавления события по тэйкеру (карточка)
    public class CreateTakerIncidentDto
    {
        //Id события
        public int IncidentId { get; set; }

        //Id охранника, по которому произошло событие
        public int TakerId { get; set; }

        //Имя пользователя
        public string TakerFullName { get; set; }

        //Имя пользователя, который внес изменения
        public string ManagerName { get; set; }

        //Имя пользователя, который внес изменения
        public DateTime Date { get; set; }

        //Примечание
        public string Description { get; set; }
    }
}
