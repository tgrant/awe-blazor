﻿using System;

namespace Awe.Chop.Contracts.Kpi.IncidentDto
{
    //DTO для отображения всех доступных событий (каталог, ИВ)
    public class IncidentInfo
    {
        public int Id { get; set; }

        //Дата записи
        public DateTime RecDate { get; set; }

        //Наименование события (замечание от руководителя, выход на замену, ...)
        public string Name { get; set; }

        //Тип события (профессионализм, лояльность, ...)
        public string CategoryName { get; set; }

        //Коэффициент для рассчета зарплаты
        public decimal Ratio { get; set; }

        //Примечание
        public string Description { get; set; }
    }
}
