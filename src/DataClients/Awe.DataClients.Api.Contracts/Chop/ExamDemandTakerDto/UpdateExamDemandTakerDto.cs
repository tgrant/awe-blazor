﻿using System;

namespace Awe.Chop.Contracts.ExamDemandTakerDto
{
    //DTO для изменения ExamDemandTaker
    public class UpdateExamDemandTakerDto
    {
        public int Id { get; set; }

        //ID Exam Demand
        public int ExamDemandId { get; set; }

        //Имя
        public string FirstName { get; set; }

        //Фамилия
        public string LastName { get; set; }

        //Отчество
        public string MiddleName { get; set; }

        //Пол (Male/Female)
        public string Gender { get; set; }

        //Дата рождения
        public DateTime BirthDate { get; set; }

        //№ СНИЛС
        public string Snils { get; set; }

        //Должность
        public string PostName { get; set; }

        //Разряд
        public string Rank { get; set; }

        //Периодичность
        public string PeriodicType { get; set; }

        //Примечание
        public string Description { get; set; }
    }
}