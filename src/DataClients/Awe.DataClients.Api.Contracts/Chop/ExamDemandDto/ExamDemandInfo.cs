﻿using System;

namespace Awe.Chop.Contracts.ExamDemandDto
{
    //DTO для получения ExamDemand
    public class ExamDemandInfo : CreateExamDemandDto
    {
        public int Id { get; set; }

        //Дата создания
        public DateTime CreatedDate { get; set; }

        //Номер заявки
        public string Number { get; set; }

        //Дата отправки на рассмотрение
        public DateTime? SubmitDate { get; set; }

        //Статус отправки на рассмотрение
        public bool IsSubmitted { get; set; }

        //Текст статуса отправки на рассмотрение
        public string StatusText { get; set; }

        //Имя контрагента
        public string ContragentName { get; set; }

        //Город контрагента
        public string ContragentCityName { get; set; }
        
        //Количество тэйкеров
        public int TakersCount { get; set; }
    }
}