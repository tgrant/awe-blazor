﻿using System;

namespace Awe.Chop.Contracts.ExamDemandDto
{
    //DTO для создания ExamDemand
    public class CreateExamDemandDto
    {
        //ID Контрагента
        public int ContragentId { get; set; }

        //Предпочтительное НОУ
        public string PreferredNouName { get; set; }

        //Предпочтительная дата экзамена
        public DateTime? PreferredExamDate { get; set; }

        //Примечание
        public string Description { get; set; }
    }
}