﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Awe.Core.Data.Contracts.DataServices;
using Awe.Core.Data.Contracts.Settings.Enums;

namespace Awe.DataClients.Contracts.DataClients
{
    public interface IDataClient
    {
        Task<IEnumerable<T>?> GetViewerDataAsync<T>(ApiSource apiSource, string viewerName, string verb, IApiParams queryParams);

        Task<T?> GetViewerDataSingleAsync<T>(ApiSource apiSource, string viewerName, string verb, IApiParams queryParams);
    }
}
