namespace Awe.DataClients.Contracts.Settings;

public interface ISettingsFromConfigClient
{
    string CurUserLogin { get; set; }
    int WorkPlaceId { get; set; }
    void Save();
}