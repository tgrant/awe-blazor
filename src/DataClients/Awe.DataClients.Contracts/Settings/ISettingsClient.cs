﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Awe.Contracts.Settings.Enums.Action;
using Awe.Contracts.Settings.Menu;
using Awe.Contracts.Settings.ProgramConstants;
using Awe.Contracts.Settings.ViewerBuild;
using Awe.Contracts.Settings.ViewerLinks;
using Awe.Core.Common.Contracts;
using Awe.DataClients.Contracts.Dto.Info;

namespace Awe.DataClients.Contracts.Settings
{
    public interface ISettingsClient
    {
        Task<IEnumerable<ViewerFullInfo>> GetViewerBuildSettingsAsync(int viewerId, PageType pageType);
        Task<ViewerInfo> GetViewerInfoAsync(string alias, PageType pageType);


        Task<CrudInfo> GetCrudAsync(string alias);

        Task<string> GetFullSpNameAsync(int viewerId);

        Task<ViewerInfo> GetViewerInfoAsync(int viewerId);
        Task<ViewerFullInfo> GetViewerFullInfoAsync(int viewerId);

        Task<bool> IsFilterFormExistsAsync(int viViewerId);

        Task<ViewerLinkInfo> GetViewerLinkAsync(int viewerId, ActionType actionType, string curDbColumnName = null);
        Task<ViewerLinkInfo[]> GetCtxMenuViewerLinksAsync(int viewerId);
        Task<ProgramConstInfo> GetProgramConstantsAsync();
        Task<string> GetCustomProgramConst(string constName);
        Task<MenuItemInfo[]> LoadMainMenuAsync(int userId);
        Task<MenuItemInfo[]> LoadSideMenuAsync(int userId);
        Task<IEnumerable<PageInfo>> GetAllPages();
        Task<IEnumerable<Page>> LoadAllPages();
    }
}