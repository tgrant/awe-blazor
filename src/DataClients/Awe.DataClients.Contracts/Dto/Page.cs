﻿using System.Collections.Generic;

public class Page
{
    public int Id { get; set; }
    public string PageGroup { get; set; }
    public string PageType { get; set; }
    public string Alias { get; set; }
    public string Caption { get; set; }
    public List<PageProp> PageProps { get; set; }
    public Viewer MasterViewer { get; set; }
    public List<Viewer> DetailViewers { get; set; }

    public class PageProp
    {
        public int Id { get; set; }
        public string PropType { get; set; }
        public string Value { get; set; }
    }

    public class Viewer
    {
        public int Id { get; set; }
        public string ViewerType { get; set; }
        public string Alias { get; set; }
        public string Caption { get; set; }
        public int ViewerLevel { get; set; }
        public string ViewerController { get; set; }
        public string ViewerPanel { get; set; }
        public List<Column> Columns { get; set; }
        public DbSource DataSource { get; set; }
        public List<object> ViewerProps { get; set; }
        public Link? EditLink { get; set; }

        public class Column
        {
            public int Id { get; set; }
            public string Caption { get; set; }
            public int ColumnOrder { get; set; }
            public object FormatType { get; set; }
            public int? Height { get; set; }
            public int Width { get; set; }
            public bool ReadOnly { get; set; }
            public bool ShowSum { get; set; }
            public int SortOrder { get; set; }
            public bool Visible { get; set; }
            public int ViewerId { get; set; }
            public string Field { get; set; }
            public string ControlType { get; set; }
            public Link EditLink { get; set; }
            public string Description { get; set; }
        }

        public class DbSource
        {
            public int Id { get; set; }
            public string Source { get; set; }
            public string TableName { get; set; }
            public string ViewerAlias { get; set; }
            public string ServiceName { get; set; }
            public Crud LoadCrud { get; set; }
            public SaveCrud SaveCruds { get; set; }
            public List<Viewer> DetailViewers { get; set; }

            public class Crud
            {
                public int Id { get; set; }
                public string VerbName { get; set; }
                public List<CrudParam> InParams { get; set; }
                public CrudParam OutParam { get; set; }

                public class CrudParam
                {
                    public int Id { get; set; }
                    public string ParamType { get; set; }
                    public string DtoName { get; set; }
                    public string ParamName { get; set; }
                }
            }

            public class SaveCrud
            {
                public Crud Insert { get; set; }
                public Crud Update { get; set; }
                public Crud Delete { get; set; }
            }
        }

        public class Link
        {
            public int Id { get; set; }
            public string EditMode { get; set; }
            public string SelectMode { get; set; }
            public List<LinkAction> ActionsBefore { get; set; }
            public List<LinkAction> ActionsAfter { get; set; }
            public List<LinkRule> LinkRules { get; set; }
            public Page LinkedPage { get; set; }

            public class LinkAction
            {
                public int Id { get; set; }
                public string ActionType { get; set; }
                public string ViewerLevel { get; set; }
                public int Order { get; set; }
            }

            public class LinkRule
            {
                public int Id { get; set; }
                public string RuleType { get; set; }
                public Store Destination { get; set; }
                public Store Source { get; set; }
                public string Description { get; set; }

                public class Store
                {
                    public string StoreType { get; set; }
                    public string Field { get; set; }
                    public string Value { get; set; }
                }
            }
        }
    }
}