﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Тип свойства Страницы
/// </summary>
public enum PageType
{
    /// <summary>Не указан</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    None = 0,

    /// <summary>Каталог</summary>
    Book = 1,

    /// <summary>Карточка</summary>
    Card = 2,

    /// <summary>Интерфейс выбора</summary>
    SelectBook = 3
}