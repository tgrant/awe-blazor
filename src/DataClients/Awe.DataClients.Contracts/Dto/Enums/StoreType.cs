﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Источник данных в переходе.
/// </summary>
public enum StoreType
{
    /// <summary>Из текущей строки источника</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    CurrentRow = 0,

    /// <summary>Из текущей строки формы Источника-Выбора (ИВ)</summary>
    /// <remarks>Устарело. Тоже, что и CurrentRow</remarks>
    viCurRow = 1,

    /// <summary>Из шапки текущей формы</summary>
    SelfMasterRow = 2,

    /// <summary>Из хранилища формы</summary>
    ViewerStore = 3,

    /// <summary>Из хранилища приложения</summary>
    ApplicationStore = 4,

    /// <summary>Из текущей строки текущей формы</summary>
    SelfRow = 5,

    /// <summary>Из шапки формы Источника-Выбора (ИВ)</summary>
    viMasterRow = 6,

    /// <summary>Из sql-параметров </summary>
    SqlParam = 7,

    /// <summary>Из хранилища компьютера</summary>
    MachineConfig = 8,

    /// <summary>Из хранилища пользователя</summary>
    UserStore = 9
}