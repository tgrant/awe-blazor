﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Тип правила для Перехода.
/// Как рассчитывать/взять значение.
/// </summary>
public enum LinkRuleType
{
    /// <summary>Копирование значения простое</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    Copy = 0,

    /// <summary>Подстановка явно указанного значения</summary>
    /// <remarks>
    /// Если задано значение Value,
    /// то этот Тип устанавливается автоматически
    /// </remarks>
    Value = 1,

    /// <summary>Текущая дата</summary>
    CurDate = 2,

    /// <summary>Пустая строка</summary>
    EmptyString = 3,

    /// <summary>ИД персоны</summary>
    PersonId = 5,

    /// <summary>Рассчитать сумму</summary>
    CalcSum = 6,

    /// <summary>ИД пользователя</summary>
    UserId = 7,

    /// <summary>Взять значение из текущей строки</summary>
    CurColumn = 8,

    /// <summary>Рассчитать указанную формулу</summary>
    CustomFunc = 10,

    /// <summary>Подставить фильтр указанный в строку фильтрации грида</summary>
    LiteFilter = 21
}