﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Действия до/после срабатывания перехода.
/// </summary>
public enum LinkActionType
{
    /// <summary>Не делать ничего</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    None = 0,

    /// <summary>Обновить Вьювер</summary>
    Refresh = 1,

    /// <summary>Сохранить Вьюевер</summary>
    Save = 2,

    /// <summary>Выполнить другой переход</summary>
    Link = 4,
}