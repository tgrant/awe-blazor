﻿using System;

namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Уровень вьювера.
/// </summary>
[Flags]
public enum ViewerLevel
{
    /// <summary>Не определен</summary>
    None = 0,

    /// <summary>Шапка</summary>
    Master = 1,

    /// <summary>Подвал</summary>
    Detail = 2,//10,

    /// <summary>Подвал, 2я вкладка</summary>
    Detail2 = 4,

    /// <summary>Подвал, 3я вкладка</summary>
    Detail3 = 8,

    /// <summary>Остальные после шапки и подвалов</summary>
    Other = 1024,

    /// <summary>Все до 3й вкладки в подвале</summary>
    All = Master | Detail | Detail2 | Detail3
}