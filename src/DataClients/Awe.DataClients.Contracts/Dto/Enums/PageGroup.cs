﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Группа страницы
/// </summary>
public enum PageGroup
{
    /// <summary>Не указан</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    None = 0,

    /// <summary>Служебные страницы</summary>
    Settings = 2,

    /// <summary>Все</summary>
    Analitika = 3,

    /// <summary>Экзамены</summary>
    Exam = 11,

    /// <summary>МКБ</summary>
    MKB = 21,

    /// <summary>ЧОП</summary>
    Chop = 31,

    /// <summary>Инспектор</summary>
    Inspector = 41,

    /// <summary>Инспектор</summary>
    BlackLabel = 51,

    /// <summary>Оценка</summary>
    Ocenka = 61,
}