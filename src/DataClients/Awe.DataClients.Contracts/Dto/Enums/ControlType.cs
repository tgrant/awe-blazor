﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Тип контрола / столбца грида.
/// </summary>
public enum ControlType
{
    /// <summary>Не указан</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    None = 0,

    /// <summary>Стандартное текстовое поле</summary>
    TextBox = 1,

    /// <summary>Стандартная кнопка</summary>
    Button = 2,

    /// <summary>Нередактируемое текстовое поле (серый фон).</summary>
    /// <remarks>Не выбирается. Не редактируется</remarks>
    StaticTextBox = 3,

    /// <summary>Нередактируемое выбираемое текстовое поле (желтый фон).</summary>
    /// <remarks>Выбирается. Не редактируется</remarks>
    SelectTextBox = 4,

    /// <summary>Стандартное числовое поле</summary>
    NumericTextBox = 5,

    /// <summary>Стандартное поле с Датой</summary>
    DateTextBox = 6,

    /// <summary>Стандартное поле с Галочкой</summary>
    CheckBox = 7,

    /// <summary>Стандартный Комбобокс</summary>
    ComboBox = 8,

    /// <summary>Кнопка для печати</summary>
    CPrintButton = 10
}