﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Тип свойства Страницы
/// </summary>
public enum PagePropType
{
    /// <summary>Не указан</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    None = 0,

    /// <summary>Заголовок страницы</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    Caption = 1
}