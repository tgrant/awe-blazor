﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Тип перехода.
/// </summary>
public enum SelectMode
{
    /// <summary>Не указан</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    None = 0,

    /// <summary>Выбор одной записи без запроса количества</summary>
    Simple = 2,

    /// <summary>Выбор нескольких записей с запросом количества типа Int</summary>
    IntMulti = 3,

    /// <summary>Выбор нескольких записей с запросом количества типа Decimal</summary>
    DecMulti = 4,

    /// <summary>Выбор нескольких записей без запроса количества</summary>
    SimpleMulti = 9,
}