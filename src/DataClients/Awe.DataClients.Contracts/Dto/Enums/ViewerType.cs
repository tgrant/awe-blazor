﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Тип вьювера.
/// </summary>
public enum ViewerType
{
    //*****************************
    /// <summary>Не указан</summary>
    None = 0,

    //*****************************
    /// <summary>Каталог без выбора</summary>
    List = 11,

    /// <summary>Форма с деревом</summary>
    TreeBook = 14,

    //*****************************
    /// <summary>Простая карточка односложная</summary>
    SimpleCard = 21,

    /// <summary>Шапка карточки многосложной</summary>
    EditableList = 22,

    //*****************************
    /// <summary>Отчет ОО/ЛО односложный</summary>
    OOReport = 34,

    /// <summary>Шапка отчета ОО/ЛО многосложного</summary>
    OODoubleReport = 35,

    /// <summary>Подвал отчета ОО/ЛО многосложного</summary>
    OODoubleReportDetail = 36,

    //*****************************
    /// <summary>Хранимая процедура</summary>
    StoredProc = 41,
}