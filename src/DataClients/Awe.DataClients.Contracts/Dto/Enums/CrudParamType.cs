﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Действия до/после срабатывания перехода.
/// </summary>
public enum CrudParamType
{
    /// <summary>Не указан</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    None = 0,

    // <summary>DTO</summary>
    Dto = 1,

    // <summary>Параметр типа bool</summary>
    Bool = 11,

    // <summary>Параметр типа string</summary>
    String = 12,

    // <summary>Параметр типа int</summary>
    Int = 31,

    // <summary>Параметр типа decimal</summary>
    Decimal = 32
}