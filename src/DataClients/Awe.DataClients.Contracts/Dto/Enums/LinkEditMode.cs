﻿namespace Awe.DataClients.Contracts.Dto.Enums;

/// <summary>
/// Тип редактирования при переходе.
/// </summary>
public enum LinkEditMode
{
    /// <summary>Не указан</summary>
    /// <remarks>Значение по-умолчанию</remarks>
    None = 0,

    /// <summary>Добавление новой записи</summary>
    Add = 1,

    /// <summary>Редактирование новой записи</summary>
    Edit = 2,

    /// <summary>Редактирование текущего поля вызовом ИВ</summary>
    FieldEdit = 3,
}