﻿using System.Collections.Generic;
using Awe.DataClients.Contracts.Dto.Enums;

namespace Awe.DataClients.Contracts.Dto.Info
{
    public class PageInfo
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public PageGroup PageGroup { get; set; }
        public PageType PageType { get; set; }
        public string Caption { get; set; }
        public List<PagePropInfo> PageProps { get; set; }
        public ViewerInfo MasterViewer { get; set; }
        public List<ViewerInfo> DetailViewers { get; set; }

        public class PagePropInfo
        {
            public PagePropType PagePropType { get; set; }
            public dynamic Value { get; set; }
        }

        public class ViewerInfo
        {
            public int Id { get; set; }
            public ViewerType ViewerType { get; set; }
            public string Alias { get; set; }
            public string Caption { get; set; }
            public int ViewerLevel { get; set; }
            public string ViewerController { get; set; }
            public string ViewerPanel { get; set; }
            public List<ColumnInfo> Columns { get; set; }
            public DbSourceInfo DataSource { get; set; }
            public List<object> ViewerProps { get; set; }
            public LinkInfo? EditLink { get; set; }

            public class ColumnInfo
            {
                public int Id { get; set; }
                public string Caption { get; set; }
                public int ColumnOrder { get; set; }
                public ControlFormatType FormatType { get; set; }
                public int Height { get; set; }
                public int Width { get; set; }
                public bool ReadOnly { get; set; }
                public bool ShowSum { get; set; }
                public int SortOrder { get; set; }
                public bool Visible { get; set; }
                public string Field { get; set; }
                public ControlType ControlType { get; set; }
                public LinkInfo EditLink { get; set; }
                public string Description { get; set; }
            }

            public class DbSourceInfo
            {
                public string Source { get; set; }
                public string TableName { get; set; }
                public string ViewerAlias { get; set; }
                public string ServiceName { get; set; }
                public CrudInfo LoadCrud { get; set; }
                public SaveCrudInfo SaveCruds { get; set; }
                public List<ViewerInfo> DetailViewers { get; set; }

                public class CrudInfo
                {
                    public string VerbName { get; set; }
                    public List<CrudParamInfo> InParams { get; set; }
                    public CrudParamInfo OutParam { get; set; }

                    public class CrudParamInfo
                    {
                        public CrudParamType CrudParamType { get; set; }
                        public string DtoName { get; set; }
                        public string ParamName { get; set; }
                    }
                }

                public class SaveCrudInfo
                {
                    public CrudInfo Insert { get; set; }
                    public CrudInfo Update { get; set; }
                    public CrudInfo Delete { get; set; }
                }
            }

            public class LinkInfo
            {
                public LinkEditMode LinkEditMode { get; set; }
                public SelectMode SelectMode { get; set; }
                public List<LinkActionInfo> ActionsBefore { get; set; }
                public List<LinkActionInfo> ActionsAfter { get; set; }
                public List<LinkRuleInfo> LinkRules { get; set; }
                public PageInfo LinkedPage { get; set; }

                public class LinkActionInfo
                {
                    public LinkActionType ActionType { get; set; }
                    public ViewerLevel ViewerLevel { get; set; }
                    public int Order { get; set; }

                    public List<LinkInfo> Links { get; set; }
                }

                public class LinkRuleInfo
                {
                    public LinkRuleType RuleType { get; set; }
                    public StoreInfo Destination { get; set; }
                    public StoreInfo Source { get; set; }
                    public string Description { get; set; }

                    public class StoreInfo
                    {
                        public StoreType StoreType { get; set; }
                        public string Field { get; set; }
                        public bool FieldDropDown { get; set; }
                        public string Value { get; set; }
                    }
                }
            }
        }
    }
}
