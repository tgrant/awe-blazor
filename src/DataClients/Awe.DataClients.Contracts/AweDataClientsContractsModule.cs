﻿using Abp.Modules;
using Awe.Core.Data.Contracts;

namespace Awe.DataClients.Contracts;

[DependsOn(typeof(AweCoreDataContractsModule))]
public class AweDataClientsContractsModule : AbpModule
{
}