namespace Awe.Core.Common.Contracts.ViewEventsData;

public interface IViewEventService
{
    Task TriggerEventAsync<T>(T eventData)
        where T : ViewEventData;

    void TriggerEvent<T>(T eventData)
        where T : ViewEventData;
}