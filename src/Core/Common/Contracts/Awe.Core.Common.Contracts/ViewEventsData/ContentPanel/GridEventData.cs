﻿namespace Awe.Core.Common.Contracts.ViewEventsData.ContentPanel
{
    public class GridEventData : ViewEventData
    {
        public GridEventData(DateTime eventTime, object eventSource, int? rowId, LinkedPageInfo linkedPageInfo) 
            : base(eventTime, eventSource)
        {
            RowId = rowId;
            LinkedPageInfo = linkedPageInfo;
        }

        public int? RowId { get; set; }
        public LinkedPageInfo LinkedPageInfo { get; set; }
    }
}
