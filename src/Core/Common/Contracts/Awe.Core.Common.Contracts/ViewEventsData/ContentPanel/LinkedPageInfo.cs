﻿namespace Awe.Core.Common.Contracts.ViewEventsData.ContentPanel;

public class LinkedPageInfo
{
    public PageType PageType { get; set; }
    public string Alias { get; set; }
}