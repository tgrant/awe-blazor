﻿using Awe.Core.Common.Contracts.Data;

namespace Awe.Core.Common.Contracts.ViewEventsData.ContentPanel;

public class GridLoadEventData : ViewEventData
{
    public GridLoadEventData(DateTime eventTime, object eventSource, int? viewerId, FilterCollection filters) 
        : base(eventTime, eventSource)
    {
        ViewerId = viewerId;
        Filters = filters;
    }

    public int? ViewerId { get; }
    public FilterCollection Filters { get; }
}