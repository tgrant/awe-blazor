namespace Awe.Core.Common.Contracts.ViewEvent;

public interface IWidgetEventService
{
    void TriggerEvent<T>(T eventData)
        where T : WidgetEventData;

    Task TriggerEventAsync<T>(T eventData)
        where T : WidgetEventData;
}