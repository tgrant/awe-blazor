using Abp.Events.Bus;

namespace Awe.Core.Common.Contracts.ViewEventsData;

public class ViewEventData : IEventData
{
    public ViewEventData(DateTime eventTime, object eventSource)
    {
        EventTime = eventTime;
        EventSource = eventSource;
    }

    public DateTime EventTime { get; set; }
    public object EventSource { get; set; }

    public IView View => (IView)EventSource;
}