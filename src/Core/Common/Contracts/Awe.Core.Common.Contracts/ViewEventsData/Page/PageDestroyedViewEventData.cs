namespace Awe.Core.Common.Contracts.ViewEventsData.Page;

public class PageDestroyedViewEventData : PageEventData
{
    public PageDestroyedViewEventData(DateTime eventTime, object eventSource, IPageView pageView, ViewEventData viewEventData, PageType pageType, string viewerAlias) 
        : base(eventTime, eventSource, pageView, viewEventData, pageType, viewerAlias)
    {
        PageType = pageType;
        ViewerAlias = viewerAlias;
    }

    public PageType PageType { get; }
    public string ViewerAlias { get; }
}