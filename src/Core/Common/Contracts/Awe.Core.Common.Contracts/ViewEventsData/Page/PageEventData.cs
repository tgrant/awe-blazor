using Abp.Events.Bus;

namespace Awe.Core.Common.Contracts.ViewEventsData.Page;

public class PageEventData : ViewEventData, IEventData
{
    public PageEventData(DateTime eventTime, object eventSource, IPageView pageView, ViewEventData viewEventData, PageType pageType, string viewerAlias) 
        : base(eventTime, eventSource)
    {
        PageView = pageView;
        ViewEventData = viewEventData;
        PageType = pageType;
        ViewerAlias = viewerAlias;
    }

    public IPageView PageView { get; set; }
    public ViewEventData ViewEventData { get; }
    public PageType PageType { get; }
    public string ViewerAlias { get; }
}