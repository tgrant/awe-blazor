using Awe.Core.Common.Contracts.Data;

namespace Awe.Core.Common.Contracts.ViewEventsData.Page;

public class PageBuildViewEventData : PageEventData
{
    public PageBuildViewEventData(DateTime eventTime, object eventSource, IPageView pageView, ViewEventData viewEventData, PageType pageType, string viewerAlias, FilterCollection filters) 
        : base(eventTime, eventSource, pageView, viewEventData, pageType, viewerAlias)
    {
        PageType = pageType;
        ViewerAlias = viewerAlias;
        Filters = filters;
    }

    public PageType PageType { get; }
    public string ViewerAlias { get; }
    public FilterCollection Filters { get; }
}