﻿namespace Awe.Core.Common.Contracts.Data
{
    public enum DataSource
    {
        None,
        AweApi,
        AweApiSettings,
        AweApiAuth,
        AweApiAudit,

        ApiBlackLabel,
        ApiDistant,

        Ado,

        LogDB,
        MiniCRM,
        //Settings,
        Study,
        Weapon,

        AweSettingsFile
    }
}
