﻿using System.Collections.ObjectModel;

namespace Awe.Core.Common.Contracts.Data
{
    public class FilterCollection : ReadOnlyDictionary<string, int>
    {
        static FilterCollection()
        {
            Empty = new FilterCollection(new Dictionary<string, int>());
        }

        public FilterCollection(IDictionary<string, int> dictionary) : base(dictionary ?? new Dictionary<string, int>())
        {
        }

        public static FilterCollection Empty { get; }
    }
}
