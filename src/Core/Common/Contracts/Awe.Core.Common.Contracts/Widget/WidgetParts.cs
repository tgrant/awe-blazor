﻿namespace Awe.Core.Common.Contracts.Widget;

public enum WidgetParts
{
    NotDefined,
    Model,
    View,
    Controller
}