namespace Awe.Core.Common.Contracts.Widget;

public enum AweControlType
{
    None,
    Auto,
    TextBox,
    Button,
    StaticTextBox,
    ComboTextBox,
    NumericTextBox,
    IntTextBox,
    DateTextBox,
    CheckBox,
    ComboBox,
    TextBoxInfo,
    MemoTextBox
}