using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Common.Contracts.Widget;

public interface IPageView : IView, IDisposable
{
    PageViewInfo? PageViewInfo { get; set; }
}