namespace Awe.Core.Common.Contracts.Widget;

public interface IView : IWidgetPart<IView>
{
    string Caption { get; set; }
    bool Enabled { get; set; }

    void EventsInit();
}