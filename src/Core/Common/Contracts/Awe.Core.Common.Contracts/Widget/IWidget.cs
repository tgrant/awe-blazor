namespace Awe.Core.Common.Contracts.Widget;

public interface IWidget
{
    #region Parents

    //IWidget? ParentWidget { get; set; }
    T Parent<T>(IWidget? startWidget = null) where T : IWidget;
    T ParentPart<T>(IWidget? startWidget = null) where T : class, IWidgetPart;

    #endregion

    #region Childs

    IWidgetCollection WidgetChildren { get; }

    IEnumerable<T> ChildParts<T>(bool recurse = true, Func<IWidget, bool> selector = null) where T : IWidgetPart;

    #endregion

    #region Parts

    IEnumerable<IWidgetPart> Parts { get; }

    IController Controller { get; set; }
    IDataManager DataManager { get; set; }
    IView View { get; set; }
    int? Id { get; set; }

    #endregion

    IWidgetPart this[WidgetParts widgetPartType] { get; }
    IEnumerable<T> ChildWidgets<T>(bool recurse = true, Func<IWidget, bool> selector = null) where T : IWidget;
}

public interface IWidget<TC, TD, TV> : IWidget
    where TC : class, IController
    where TD : class, IDataManager
    where TV : class, IView
{
    new TC Controller { get; set; }
    new TD DataManager { get; set; }
    new TV View { get; set; }
}