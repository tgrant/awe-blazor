﻿using System.Collections;

namespace Awe.Core.Common.Contracts.Widget;


public interface IDataStore : IStore
{
    object GetOrDefault(string sParamName);
}

public interface IDataStore<out T> : IDataStore, IEnumerable
{
    new T GetOrDefault(string sParamName);
}

public interface IListDataStore : IDataStore
{
    void Init(IEnumerable<object?> items);
}

public interface IListDataStore<in T> : IListDataStore, IList
{
    void Init(IEnumerable<T> items);
}

public interface ISingleDataStore : IDataStore
{
    dynamic? Data { get; set; }
    void Init(object item);
}