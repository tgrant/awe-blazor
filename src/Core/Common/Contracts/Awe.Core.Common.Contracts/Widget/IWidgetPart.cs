﻿using Abp.Domain.Services;

namespace Awe.Core.Common.Contracts.Widget;

public interface IWidgetPart : IDomainService
{
}

public interface IWidgetPart<T> : IWidgetPart
{
}