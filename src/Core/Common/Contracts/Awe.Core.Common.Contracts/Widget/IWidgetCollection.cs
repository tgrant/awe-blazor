﻿namespace Awe.Core.Common.Contracts.Widget;

public interface IWidgetCollection : IDictionary<int, IWidget>
{
    void AddRange(IEnumerable<IWidget> widgetChildrenInstance);
}