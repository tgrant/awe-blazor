namespace Awe.Core.Common.Contracts.Widget;

public interface IDataManager : IWidgetPart<IDataManager>
{
    IDataStore DataStore { get; set; }
}