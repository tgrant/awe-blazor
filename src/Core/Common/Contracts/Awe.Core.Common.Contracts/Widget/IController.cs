namespace Awe.Core.Common.Contracts.Widget;

public interface IController : IWidgetPart<IController>
{
    void EventsInit();
}