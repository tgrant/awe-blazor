﻿using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;

namespace Awe.Core.Common.Contracts.ViewInfo
{
    public class LevelPanelViewInfo
    {
        public bool Enabled { get; set; }

        public int Level { get; set; }

        public ContentPanelViewInfo? ContentPanelViewInfo { get; set; }
        public LevelMenuViewInfo? LevelMenuViewInfo { get; set; }
        public FileStorageViewInfo? FileStorageViewInfo { get; set; }
    }

    public class FileStorageViewInfo
    {
        public string StubText { get; set; }
    }

    public class LevelMenuViewInfo
    {
        public string StubText { get; set; }
    }
}
