﻿using Awe.Core.Common.Contracts.ViewInfo;

namespace Awe.Core.Widget.Contracts.ViewInfo
{
    public class PageViewInfo
    {
        public string Caption { get; set; }

        public List<LevelPanelViewInfo> LevelPanelInfos { get; set; } = null!;
    }
}
