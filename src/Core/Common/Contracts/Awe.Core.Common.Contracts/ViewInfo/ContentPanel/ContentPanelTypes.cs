﻿namespace Awe.Core.Widget.Contracts.ViewInfo.ContentPanel;

public enum ContentPanelTypes
{
    None,
    Simple,
    Grid,
    Tree,
    FileStorage,
    LevelMenu
}