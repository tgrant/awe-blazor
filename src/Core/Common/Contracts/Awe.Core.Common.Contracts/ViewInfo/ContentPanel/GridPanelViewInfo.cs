﻿using Awe.Core.Common.Contracts.ViewEventsData.ContentPanel;

namespace Awe.Core.Common.Contracts.ViewInfo.ContentPanel;

public class GridPanelViewInfo
{
    public List<ColumnViewInfo>? ColumnViewInfos { get; set; }
    public LinkedPageInfo LinkedPageInfo { get; set; }
    public IListDataStore DataStore { get; set; }
    public bool Editable { get; set; }
}

public class ColumnViewInfo
{
    public string Caption { get; set; }
    public string FieldName { get; set; }
    public int Width { get; set; }
}