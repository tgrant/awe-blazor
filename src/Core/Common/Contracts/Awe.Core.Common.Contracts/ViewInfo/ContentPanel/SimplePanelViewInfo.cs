﻿namespace Awe.Core.Common.Contracts.ViewInfo.ContentPanel;

public class SimplePanelViewInfo
{
    public string? ViewPanelClass { get; set; }
    public ISingleDataStore DataStore { get; set; }
}