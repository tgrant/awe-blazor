﻿using Awe.Core.Widget.Contracts.ViewInfo.ContentPanel;

namespace Awe.Core.Common.Contracts.ViewInfo.ContentPanel;

public class ContentPanelViewInfo
{
    public int ViewerId { get; set; }

    public ContentPanelTypes ContentPanelType { get; set; }

    public SimplePanelViewInfo? SimplePanelViewInfo { get; set; }
    public GridPanelViewInfo? GridPanelViewInfo { get; set; }
    public string Caption { get; set; }
}