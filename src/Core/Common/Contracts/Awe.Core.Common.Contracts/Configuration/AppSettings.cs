﻿using Microsoft.Extensions.Options;

namespace Awe.Core.Common.Contracts.Configuration;

/// <summary>
/// Содержит общие параметры конфигурации для приложения.
/// </summary>
public class AppSettings : IOptions<AppSettings>
{
    /// <summary>
    /// Параметры конфигурации для приложения.
    /// </summary>
    public AppOptions App { get; set; }
    public string BaseAddress { get; set; }
    public string Environment { get; set; }

    AppSettings IOptions<AppSettings>.Value => this;
}

/// <summary>
/// Класс опций для приложения
/// </summary>
public class AppOptions : IOptions<AppOptions>
{
    /// <summary>
    /// Получает или задает URL приложения
    /// </summary>
    public string SelfUrl { get; set; }

    /// <summary>
    /// Получает или задает разрешенные URL-адреса для перенаправления
    /// </summary>
    public string RedirectAllowedUrls { get; set; }

    /// <summary>
    /// Получает или задает время ожидания выполнения команд
    /// </summary>
    public int CommandTimeout { get; set; }

    /// <summary>
    /// Получает или задает тип лицензии
    /// </summary>
    public string LicenseType { get; set; }

    /// <summary>
    /// Получает или задает серийный номер приложения
    /// </summary>
    public string SerialNumber { get; set; }

    /// <summary>
    /// Получает или задает URI-адрес сервис-провайдера клиентских настроек
    /// </summary>
    public string ClientSettingsProviderServiceUri { get; set; }

    /// <summary>
    /// Получает или задает время ожидания запроса
    /// </summary>
    public int RequestTimeout { get; set; }

    /// <summary>
    /// Получает или задает опции API
    /// </summary>
    public ApiOptions Api { get; set; }

    /// <summary>
    /// Contains configuration settings for the authentication server.
    /// </summary>
    public AuthServerSettings AuthServer { get; set; }
    
    /// <summary>
    /// Contains configuration settings for string encryption tasks.
    /// </summary>
    public StringEncryptionSettings StringEncryption { get; set; }

    AppOptions IOptions<AppOptions>.Value => this;
}

/// <summary>
/// Содержит параметры конфигурации для сервера аутентификации.
/// </summary>
public class AuthServerSettings : IOptions<AuthServerSettings>
{
    /// <summary>
    /// The URL authority that will handle authentication requests.
    /// </summary>
    public string Authority { get; set; }

    /// <summary>
    /// Specifies whether HTTPS is required for metadata address endpoints.
    /// </summary>
    public bool RequireHttpsMetadata { get; set; }

    AuthServerSettings IOptions<AuthServerSettings>.Value => this;
}

/// <summary>
/// Contains configuration settings for string encryption tasks.
/// </summary>
public class StringEncryptionSettings : IOptions<StringEncryptionSettings>
{
    /// <summary>
    /// The default passphrase for string encryption tasks.
    /// </summary>
    public string DefaultPassPhrase { get; set; }

    StringEncryptionSettings IOptions<StringEncryptionSettings>.Value => this;
}

/// <summary>
/// Класс опций для API
/// </summary>
public class ApiOptions : IOptions<ApiOptions>
{
    /// <summary>
    /// Получает или задает опции базового URL API
    /// </summary>
    public BaseApiOptions BaseApiUrl { get; set; }

    /// <summary>
    /// Получает или задает опции префикса API
    /// </summary>
    public BaseApiOptions ApiPrefix { get; set; }

    /// <summary>
    /// Получает или задает опции пользователя API
    /// </summary>
    public ApiUserOptions ApiUser { get; set; }

    ApiOptions IOptions<ApiOptions>.Value => this;
}

/// <summary>
/// Класс опций для базового URL
/// </summary>
public class BaseApiOptions : IOptions<BaseApiOptions>
{
    BaseApiOptions IOptions<BaseApiOptions>.Value => this;

    /// <summary>
    /// Свойство для значения по умолчанию
    /// </summary>
    public string Default { get; set; }

    /// <summary>
    /// Свойство для URL AweApi
    /// </summary>
    public string AweApi { get; set; }

    /// <summary>
    /// Свойство для URL AweApiSettings
    /// </summary>
    public string AweApiSettings { get; set; }

    /// <summary>
    /// Свойство для URL AweApiAuth
    /// </summary>
    public string AweApiAuth { get; set; }

    /// <summary>
    /// Свойство для URL AweApiAudit
    /// </summary>
    public string AweApiAudit { get; set; }

    /// <summary>
    /// Свойство для URL ApiBlackLabel
    /// </summary>
    public string ApiBlackLabel { get; set; }

    /// <summary>
    /// Свойство для URL ApiDistant
    /// </summary>
    public string ApiDistant { get; set; }

    /// <summary>
    /// Свойство для URL Ado
    /// </summary>
    public string Ado { get; set; }

    /// <summary>
    /// Свойство для URL LogDB
    /// </summary>
    public string LogDB { get; set; }

    /// <summary>
    /// Свойство для URL Study
    /// </summary>
    public string Study { get; set; }

    /// <summary>
    /// Свойство для URL Weapon
    /// </summary>
    public string Weapon { get; set; }
}

/// <summary>
/// Класс опций для пользовательской информации в API
/// </summary>
public class ApiUserOptions : IOptions<ApiUserOptions>
{
    /// <summary>
    /// The default user options.
    /// </summary>
    public UserOptions Default { get; set; }

    /// <summary>
    /// The user options for the "ApiDistant" API.
    /// </summary>
    public UserOptions ApiDistant { get; set; }

    /// <summary>
    /// The user options for the "ApiBlackLabel" API.
    /// </summary>
    public UserOptions ApiBlackLabel { get; set; }

    ApiUserOptions IOptions<ApiUserOptions>.Value => this;
}

public class UserOptions : IOptions<UserOptions>
{
    /// <summary>
    /// Получает или задает имя пользователя
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// Получает или задает пароль пользователя
    /// </summary>
    public string UserPass { get; set; }

    UserOptions IOptions<UserOptions>.Value => this;
}
