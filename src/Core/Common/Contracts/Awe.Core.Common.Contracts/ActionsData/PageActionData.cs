using Abp.Events.Bus;

namespace Awe.Core.Common.Contracts.ActionData;

public class PageActionData : WidgetActionData, IEventData
{
    public DateTime EventTime { get; set; }
    public object EventSource { get; set; }
}