using Abp.Events.Bus;

namespace Awe.Core.Common.Contracts.ActionData;

public class WidgetActionData : IEventData
{
    public DateTime EventTime { get; set; }
    public object EventSource { get; set; }

    public IWidget Widget { get; set; }
}