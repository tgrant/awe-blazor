﻿namespace Awe.Chop.Contracts.Kpi.IncidentDto
{
    //DTO для изменения коэффициентов событий 
    public class UpdateIncidentDto
    {
        public int Id { get; set; }

        //Коэффициент события  для рассчета зарплаты
        public string Name { get; set; }

        //Коэффициент события  для рассчета зарплаты
        public decimal Ratio { get; set; }

        //Примечание
        public string Description { get; set; }
    }
}
