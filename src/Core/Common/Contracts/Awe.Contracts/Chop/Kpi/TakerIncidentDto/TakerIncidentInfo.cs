﻿using System;

namespace Awe.Chop.Contracts.Kpi.TakerIncidentDto
{
    //DTO для отображения добавленных событий по всем тэйкерам (каталог)
    public class TakerIncidentInfo
    {
        public int Id { get; set; }

        //Id события
        public int IncidentId { get; set; }

        //Id охранника, по которому произошло событие
        public int TakerId { get; set; }

        //ФИО охранника, по которому произошло событие
        public string TakerFullName { get; set; }

        //Имя пользователя, который внес изменения
        public string ManagerName { get; set; }

        //Дата, когда произошло событие
        public DateTime Date { get; set; }

        //Тип события (профессионализм, лояльность, ...)
        public string CategoryName { get; set; }

        //Наименование события (замечание от руководителя, выход на замену, ...)
        public string Name { get; set; }

        //Коэффициент события для рассчета зарплаты
        public decimal Ratio { get; set; }

        //Примечание
        public string Description { get; set; }
    }
}
