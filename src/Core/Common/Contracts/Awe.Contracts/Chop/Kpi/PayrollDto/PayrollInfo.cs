﻿using System;

namespace Awe.Chop.Contracts.Kpi.PayrollDto
{
    //DTO отображения всех ведомостей или одной ведомости (каталог, карточка)
    public class PayrollInfo
    {
        public int Id { get; set; }

        //ФИО менеджера, который последним редактировал карточку
        public string ManagerName { get; set; }
        
        //Наименование периода, за который производится расчет(например, Апрель 2021)
        public DateTime Date { get; set; }
        
        //Наименование периода, за который производится расчет(например, Апрель 2021)
        public string PeriodName { get; set; }
        
        //Коэффициент KPI (суммарный по всем типам событий)
        public decimal KpiRatio { get; set; }
        
        //Примечание
        public string Description { get; set; }
        
        //Значение, указывающее подписана ли ведомость или нет
        public bool IsConfirmed { get; set; }
    }
}
