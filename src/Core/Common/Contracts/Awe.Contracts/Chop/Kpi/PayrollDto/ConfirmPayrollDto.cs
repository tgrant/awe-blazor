﻿namespace Awe.Chop.Contracts.Kpi.PayrollDto
{
    //DTO для подписи ведомости (карточка)
    public class ConfirmPayrollDto
    {
        public int Id { get; set; }

        //ФИО менеджера
        public string ManagerName { get; set; }
    }
}
