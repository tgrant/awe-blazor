﻿namespace Awe.Chop.Contracts.Kpi.PayrollDetail
{
    //DTO отображения деталей ведомости (спецификация)
    public class PayrollDetailInfo
    {
        public int Id { get; set; }

        //ID ведомости
        public int PayrollId { get; set; }

        //Id охранника, по которому рассчитывается ведомость
        public int TakerId { get; set; }

        //ФИО охранника, по которому рассчитывается ведомость
        public string TakerFullName { get; set; }

        //Зарплата, после применения коэффициента
        public string CardNumber { get; set; }

        //Зарплата до применения коэффициента
        public decimal BaseSalary { get; set; }

        //Зарплата после применения коэффициента
        public decimal FinalSalary { get; set; }

        //Коэффициент KPI (итоговый по всем типам событий)
        public decimal KpiRatio { get; set; }

        //Коэффициент типа профессионализм (по всем событиям данного типа)
        public decimal ProfessionalismRatio { get; set; }

        //Коэффициент типа лояльность (по всем событиям данного типа)
        public decimal LoyaltyRatio { get; set; }

        //Коэффициент типа обучение (по всем событиям данного типа)
        public decimal StudyRatio { get; set; }

        //Коэффициент типа психо-эмоциональное состояние (по всем событиям данного типа)
        public decimal EmotionalRatio { get; set; }
    }
}
