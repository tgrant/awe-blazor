﻿namespace Awe.Chop.Contracts.Kpi.PayrollDetail
{
    //DTO редактирование полных деталей ведомости, включая количество событий по сотруднику (карточка)
    public class UpdatePayrollDetailFullDto
    {
        public int Id { get; set; }

        #region Professionalism ratio and count
        //Замечание от руководителя: коэффициент и количество
        public decimal ProfReprimandsByChiefRatio { get; set; }
        public int ProfReprimandsByChiefCount { get; set; }
        //Замечание от заказчика: коэффициент и количество
        public decimal ProfReprimandsByCustomerRatio { get; set; }
        public int ProfReprimandsByCustomerCount { get; set; }
        //Благодарность от заказчика: коэффициент и количество
        public decimal ProfThanksRatio { get; set; }
        public int ProfThanksCount { get; set; }
        //Задержание нарушителей: коэффициент и количество
        public decimal ProfArrestsViolatorRatio { get; set; }
        public int ProfArrestsViolatorCount { get; set; }
        //Периодическая проверка(сдал): коэффициент и количество
        public decimal ProfPassesPeriodicalExamRatio { get; set; }
        public int ProfPassesPeriodicalExamCount { get; set; }
        #endregion

        #region Loyalty ratio and count
        //Выход на замену: коэффициент и количество
        public decimal LoyaltyAgreesReplacementRatio { get; set; }
        public int LoyaltyAgreesReplacementCount { get; set; }
        //Отказ от выхода на замену: коэффициент и количество
        public decimal LoyaltyRefusesReplacementRatio { get; set; }
        public int LoyaltyRefusesReplacementCount { get; set; }
        //Больничный: коэффициент и количество
        public decimal LoyaltyHasSickLeaveRatio { get; set; }
        public int LoyaltyHasSickLeaveCount { get; set; }
        //Прогулы: коэффициент и количество
        public decimal LoyaltyHasAbsenceRatio { get; set; }
        public int LoyaltyHasAbsenceCount { get; set; }
        //Нахождение на об. в сост. алк. опьянения: коэффициент и количество
        public decimal LoyaltyHasAlcoholInfluenceRatio { get; set; }
        public int LoyaltyHasAlcoholInfluenceCount { get; set; }
        //Просьбы о заменах: коэффициент и количество
        public decimal LoyaltyAsksForReplacementRatio { get; set; }
        public int LoyaltyAsksForReplacementCount { get; set; }
        //Отсутствие форм. одежды: коэффициент и количество
        public decimal LoyaltyHasNoUniformRatio { get; set; }
        public int LoyaltyHasNoUniformCount { get; set; }
        #endregion

        #region Study ratio and count
        //Треннинги: коэффициент и количество
        public decimal StudyHasCoachingRatio { get; set; }
        public int StudyHasCoachingCount { get; set; }
        //Тренировки: коэффициент и количество
        public decimal StudyHasTrainingRatio { get; set; }
        public int StudyHasTrainingCount { get; set; }
        //Ежекварт. зачеты по физ. подг: коэффициент и количество
        public decimal StudyPassesPhysicalTrainingGoalRatio { get; set; }
        public int StudyPassesPhysicalTrainingGoalCount { get; set; }
        //Ежекварт. зачеты по спец. подг: коэффициент и количество
        public decimal StudyPassesSpecialTrainingGoalRatio { get; set; }
        public int StudyPassesSpecialTrainingGoalCount { get; set; }
        #endregion

        #region Psyco-Emotional ratio and count
        //Входное тестирование: коэффициент и количество
        public decimal EmotionalHasInputTestRatio { get; set; }
        public int EmotionalHasInputTestCount { get; set; }
        //Ежекварт. автоматическое тестирование в ЕТЦ: коэффициент и количество
        public decimal EmotionalHasAutoTestRatio { get; set; }
        public int EmotionalHasAutoTestCount { get; set; }
        //Негативное поведение при внутр. проверках: коэффициент и количество
        public decimal EmotionalHasNegativeBehaviorRatio { get; set; }
        public int EmotionalHasNegativeBehaviorCount { get; set; }
        //Психо-эмоциональный фон при общении с коллегами: коэффициент и количество
        public decimal EmotionalInteractsWithColleaguesRatio { get; set; }
        public int EmotionalInteractsWithColleaguesCount { get; set; }
        #endregion
    }
}
