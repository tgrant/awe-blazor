﻿namespace Awe.Chop.Contracts.ExamDemandTakerDto
{
    //DTO для получения ExamDemandTaker
    public class ExamDemandTakerInfo : CreateExamDemandTakerDto
    {
        public int Id { get; set; }

        //Текстовый статус
        public string Status { get; set; }
    }
}