﻿namespace Awe.Chop.Contracts.ContragentDto
{
    public class ContragentInfo
    {
        public int Id { get; set; }

        //Наименование контрагента
        public string Name { get; set; }

        //Наименование города
        public string CityName { get; set; }
    }
}