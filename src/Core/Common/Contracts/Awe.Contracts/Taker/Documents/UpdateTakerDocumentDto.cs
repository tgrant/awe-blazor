﻿namespace Awe.Taker.Contracts.Documents
{
    /// <summary>
    /// Дто обновления 
    /// </summary>
    public class UpdateTakerDocumentDto : CreateTakerDocumentDto
    {
        /// <summary>
        /// Идентификатор документа
        /// </summary>
        public int Id { get; set; }
    }
}
