﻿namespace TGrant.Awe.SharedEnums
{
    /// <summary> Элемент справочника из БД </summary>
    public class CatalogItemInfo
    {
        /// <summary> Идентификатор элемента справочника </summary>
        public int Id { get; set; }

        /// <summary> Наименование элемента справочника </summary>
        public string Name { get; set; }
    }
}
