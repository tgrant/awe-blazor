﻿using System;

namespace Awe.Inspector.Contracts.ExamDemandTakerDto
{
    //DTO для получения ExamDemandTaker в модуле инспектор
    public class InspectorExamDemandTakerInfo
    {
        public int Id { get; set; }

        //ID Exam Demand
        public int ExamDemandId { get; set; }

        //Наименование ЧОП
        public string ChopName { get; set; }

        //ФИО тэйкера
        public string FullName { get; set; }

        //Дата рождения тэйкера
        public DateTime BirthDate { get; set; }

        //Должность
        public string PostName { get; set; }

        //Разряд
        public string Rank { get; set; }

        //Периодичность
        public string PeriodicType { get; set; }

        //Примечание
        public string Description { get; set; }

        //Cтатус подтверждения тэйкера
        public string Status { get; set; }
    }
}