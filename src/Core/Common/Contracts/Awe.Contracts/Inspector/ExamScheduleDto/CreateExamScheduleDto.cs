﻿using System;

namespace Awe.Inspector.Contracts.ExamScheduleDto
{
    public class CreateExamScheduleDto
    {
        //Наименование НОУ
        public string NouName { get; set; }

        //Дата экзамена
        public DateTime ExamDate { get; set; }

        //Примечание
        public string Description { get; set; }
    }
}