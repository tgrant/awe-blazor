﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Awe.Inspector.Contracts.ExamScheduleDto
{
    //Контракт для выгрузки деталей графиков
    public class ExamScheduleTakerInfo
    {
        //Идентификатор подвала заявки
        public int Id { get; set; }

        //Идентификатор заявки
        public int ExamDemandId { get; set; }

        //Разряд
        public int GuardcatId { get; set; }

        //Периодичность
        public int PeriodictypeId { get; set; }

        //Название ЧОПа
        public string ChopName { get; set; }

        //Фамилия
        public string FirstName { get; set; }

        //Имя
        public string LastName { get; set; }

        //Отчество
        public string MiddleName { get; set; }

        //Пол
        public string Gender { get; set; }

        // Адрес
        public string Address { get; set; }

        //Серия паспорта
        public string PasportSerial { get; set; }

        //Номер паспорта
        public string PasportNumber { get; set; }

        //Дата выдачи паспорта
        public DateTime? PasportIssuedDate { get; set; }

        //Кем выдан паспорт
        public string PasportIssuedBy { get; set; }

        //Код паспорта
        public string PassportAuthorityCode { get; set; }

        //Дата рождения
        public DateTime BirthDate { get; set; }

        //Место рождения
        public string BirthPlace { get; set; }

        //ИНН
        public string Inn { get; set; }

        //Снилс
        public string Snils { get; set; }

        //Серия удостоверения
        public string CertificationSerial { get; set; }

        //Номер удостоверения
        public string CertificationNumber { get; set; }

        //Дата выдачи удостоверения
        public DateTime? CertificationIssuedDate { get; set; }
    }
}
