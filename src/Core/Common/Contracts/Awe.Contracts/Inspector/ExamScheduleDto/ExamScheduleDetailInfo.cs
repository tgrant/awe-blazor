﻿using System;

namespace Awe.Inspector.Contracts.ExamScheduleDto
{
    public class ExamScheduleDetailInfo
    {
        //ID заявки
        public int DemandId { get; set; }

        //ID графика
        public int ScheduleId { get; set; }

        //Номер графика
        public string Number { get; set; }

        //Наименование ЧОП
        public string ChopName { get; set; }

        //Дата создания заявки
        public DateTime CreatedDate { get; set; }

        //Количество тэйкеров
        public int TakersCount { get; set; }

        //Примечание
        public string Description { get; set; }
    }
}