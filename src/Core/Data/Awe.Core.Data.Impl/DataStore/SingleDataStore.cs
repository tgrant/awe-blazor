﻿using Abp.Dependency;
using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Data.Impl.DataStore;

public class SingleDataStore : ISingleDataStore, ITransientDependency
{
    public object GetOrDefault(string sParamName)
    {
        throw new NotImplementedException();
    }

    public dynamic? Data { get; set; }

    public void Init(object item)
    {
        Data = item;
    }
}