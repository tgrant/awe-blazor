﻿using System.Diagnostics;
using Abp.Dependency;
using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Data.Impl.DataStore;

public class ListDataStore<T> : ListDataStore, IListDataStore<T>, ITransientDependency
{
    readonly List<T> innerList = new();

    public object GetOrDefault(string sParamName)
    {
        throw new NotImplementedException();
    }
    
    public void Init(IEnumerable<T> items)
    {
        innerList.AddRange(items);
    }

    public void Init(IEnumerable<object?> items)
    {
        foreach (var item in items)
        {
            innerList.Add((T)item);
        }
    }
}

public class ListDataStore : List<object>, IListDataStore, ITransientDependency
{
    public object GetOrDefault(string sParamName)
    {
        throw new NotImplementedException();
    }

    public void Init(IEnumerable<object?> items)
    {
        Debug.Assert(items != null, nameof(items) + " != null");

        AddRange(items);
    }
}