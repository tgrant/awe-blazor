﻿using System.Configuration;
using System.Diagnostics;
using Abp.Domain.Services;
using Abp.Extensions;
using Awe.Contracts.Settings.Enums.Action;
using Awe.Contracts.Settings.Enums.Viewer;
using Awe.Contracts.Settings.Enums.ViewerLinks;
using Awe.Contracts.Settings.Menu;
using Awe.Contracts.Settings.ProgramConstants;
using Awe.Contracts.Settings.ViewerBuild;
using Awe.Contracts.Settings.ViewerLinks;
using Awe.Core.Common.Contracts;
using Awe.Core.Common.Contracts.ViewEventsData.ContentPanel;
using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Data.Contracts.Settings;
using Awe.Core.Widget.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.ViewInfo.ContentPanel;
using Awe.DataClients.Contracts.Settings;

namespace Awe.Core.Data.Impl.Settings
{
    public class SettingsService : DomainService, ISettingsService
    {
        private ProgramConstInfo _appConstants;
        private readonly ISettingsClient _settingsClient;
        private readonly ISettingsFromConfigClient _settingsFromConfigClient;

        public SettingsService(ISettingsClient settingsClient,
            ISettingsFromConfigClient settingsFromConfigClient)
        {
            _settingsClient = settingsClient;
            _settingsFromConfigClient = settingsFromConfigClient;
        }

        public async Task<ProgramConstInfo> GetAppConstantsAsync()
        {
            return _appConstants 
                   ?? (_appConstants = await _settingsClient.GetProgramConstantsAsync());
        }

        public async Task<string> GetCustomProgramConstAsync(string constName)
        {
            return await _settingsClient.GetCustomProgramConst(constName);
        }

        public async Task<MenuItemInfo[]> LoadMainMenuAsync(int userId)
        {
            var result = await _settingsClient.LoadMainMenuAsync(userId);
            return result;
        }

        public async Task<MenuItemInfo[]> LoadSideMenuAsync(int userId)
        {
            var result = await _settingsClient.LoadSideMenuAsync(userId);
            return result;
        }

        public string GetKeyClmnNameAsync(string tbName)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetConStringAsync(string alias)
        {
            var connectionStrings = ConfigurationManager.ConnectionStrings;
            switch (alias)
            {
                case "":
                    return connectionStrings["Analitika"].ConnectionString;
                case "Analitika":
                case "Study":
                case "LogDB":
                case "Settings":
                    return connectionStrings[alias].ConnectionString;
                default:
                    var crud = await GetCrudAsync(alias);
                    var source = crud.Source;
                    return await GetConStringAsync(source);
            }
        }

        public async Task<ViewerFullInfo> GetViewerFullInfoAsync(int viewerId)
        {
            var result = await _settingsClient.GetViewerFullInfoAsync(viewerId);
            return result;
        }

        public ViewerInfo GetChildCardViewerInfoAsync(int viewerID, viType edit)
        {
            throw new System.NotImplementedException();
        }

        public async Task<ViewerInfo> GetViewerInfoAsync(int viewerId)
        {
            var result = await _settingsClient.GetViewerInfoAsync(viewerId);
            return result;
        }

        private async Task<ViewerType> GetViewerTypeAsync(int viewerId)
        {
            var viewerInfo = await GetViewerInfoAsync(viewerId);
            var viewerType = viewerInfo.ViewerType;
            return viewerType;
        }

        public async Task<CrudInfo> GetCrudAsync(string alias)
        {
            var result = await _settingsClient.GetCrudAsync(alias);
            return result;
        }

        public async Task<bool> IsFilterFormExistsAsync(int viewerId)
        {
            var result = await _settingsClient.IsFilterFormExistsAsync(viewerId);
            return result;
        }

        public async Task<bool> IsCardAsync(int viewerId)
        {
            var viewerType = await GetViewerTypeAsync(viewerId);

            return  viewerType.IsIn(
                ViewerType.Card,
                ViewerType.SimpleCard,
                ViewerType.DoubleCard,
                ViewerType.DoubleCardDetail);
        } 

        public async Task<bool> IsSingleRowAsync(int viewerId)
        {
            var viewerType = await GetViewerTypeAsync(viewerId);

            return viewerType.IsIn(
                ViewerType.SimpleCard,
                ViewerType.DoubleCard,
                ViewerType.OOCardReport);
        }

        public async Task<ViewerLinkInfo> GetViewerLinkAsync(int viewerId, ActionType actionType,
            string curDbColumnName = null)
        {
            var result = await _settingsClient.GetViewerLinkAsync(viewerId, actionType, curDbColumnName);
            return result;
        }

        public async Task<ViewerLinkInfo> GetFilterViewerLinkAsync(int viewerId)
        {
            var result = await _settingsClient.GetViewerLinkAsync(viewerId, ActionType.Filter);
            return result;
        }

        public async Task<IEnumerable<ViewerLinkInfo>> GetCtxMenuViewerLinksAsync(int viewerId)
        {
            var result = await _settingsClient.GetCtxMenuViewerLinksAsync(viewerId);
            return result;
        }

        public async Task<string> GetFullSpNameAsync(int viewerId)
        {
            var result = await _settingsClient.GetFullSpNameAsync(viewerId);
            return result;
        }

        public async Task<string> GetFullSpNameAsync(ViewerInfo viewerInfo)
        {
            return await GetFullSpNameAsync(viewerInfo.Id);
        }

        // -------------------------------------

        public async Task<PageViewInfo> GetPageViewInfo(PageType pageType, string viewerAlias)
        {
            var viewerInfo = await _settingsClient.GetViewerInfoAsync(viewerAlias, pageType);

            if (viewerInfo == null) throw new ArgumentNullException(nameof(viewerInfo));

            var viewerListInfos = await _settingsClient.GetViewerBuildSettingsAsync(viewerInfo.Id, pageType);

            var viewerBuildInfo = new ViewerBuildInfo
            {
                Viewers = viewerListInfos.ToArray()
            };

            return pageType == PageType.Book
                ? GetPageListViewInfo(viewerBuildInfo)
                : GetPageCardViewInfo(viewerBuildInfo);
        }

        private PageViewInfo GetPageListViewInfo(ViewerBuildInfo viewerBuildInfo)
        {
            var pageViewInfo = new PageViewInfo
            {
                Caption = viewerBuildInfo.Viewers.First().Caption,
                LevelPanelInfos = new List<LevelPanelViewInfo>()
            };

            foreach (var vfi in viewerBuildInfo.Viewers)
            {
                var columns = vfi
                    .CColumns?
                    .Select(columnInfo => new ColumnViewInfo
                    {
                        Caption = columnInfo.Caption,
                        FieldName = columnInfo.DBColumnName,
                        Width = columnInfo.Width
                    }).ToList();

                var levelPanelViewInfo = ObjectMapper.Map<LevelPanelViewInfo>(vfi);
                var gridPanelViewInfo = new GridPanelViewInfo
                {
                    ColumnViewInfos = columns?.Count > 0 ? columns : null,

                    // TODO получать из json
                    LinkedPageInfo = new LinkedPageInfo
                    {
                        PageType = PageType.Card,
                        Alias = "Viewer"
                    }
                };

                levelPanelViewInfo.Level = (int)LevelName.Master;
                levelPanelViewInfo.ContentPanelViewInfo = new ContentPanelViewInfo
                {
                    ViewerId = vfi.Id,
                    ContentPanelType = ContentPanelTypes.Grid,
                    GridPanelViewInfo = gridPanelViewInfo
                };

                pageViewInfo.LevelPanelInfos.Add(levelPanelViewInfo);
            }

            return pageViewInfo;
        }

        private PageViewInfo GetPageCardViewInfo(ViewerBuildInfo viewerBuildInfo)
        {
            var pageCardViewInfo = new PageViewInfo
            {
                Caption = viewerBuildInfo.Viewers.First().Caption,
                LevelPanelInfos = new List<LevelPanelViewInfo>
                {
                    GetCardMaster(viewerBuildInfo.Viewers[0]),
                    GetCardDetail(viewerBuildInfo.Viewers[1])
                }
            };

            return pageCardViewInfo;
        }

        private LevelPanelViewInfo GetCardMaster(ViewerFullInfo vfi)
        {
            var levelPanelViewInfo = ObjectMapper.Map<LevelPanelViewInfo>(vfi);

            levelPanelViewInfo.Level = (int)LevelName.Master;
            //levelPanelViewInfo.LevelMenuViewInfo = new LevelMenuViewInfo
            //{
            //    StubText = "***LevelMenuView***"
            //};
            levelPanelViewInfo.FileStorageViewInfo = new FileStorageViewInfo
            {
                StubText = "***FileStorageView***"
            };

            Debug.Assert(vfi
                .ViewerProps != null, "vfi\r\n                .ViewerProps != null");

            levelPanelViewInfo.ContentPanelViewInfo = new ContentPanelViewInfo
            {
                ViewerId = vfi.Id,
                ContentPanelType = ContentPanelTypes.Simple,
                SimplePanelViewInfo = new SimplePanelViewInfo
                {
                    ViewPanelClass = vfi
                        .ViewerProps
                        .FirstOrDefault(prop => prop.ViewerExtType == ViewerExtType.LevelView)?
                        .ExtClassName.Replace("~", "")
                }
            };

            return levelPanelViewInfo;
        }

        private LevelPanelViewInfo GetCardDetail(ViewerFullInfo vfi)
        {
            var levelPanelViewInfo = ObjectMapper.Map<LevelPanelViewInfo>(vfi);

            levelPanelViewInfo.Level = (int)LevelName.Detail;
            //levelPanelViewInfo.LevelMenuViewInfo = new LevelMenuViewInfo
            //{
            //    StubText = "***LevelMenuView***"
            //};
            levelPanelViewInfo.FileStorageViewInfo = new FileStorageViewInfo
            {
                StubText = "***FileStorageView***"
            };

            var columns = vfi
                .CColumns?
                .Select(columnInfo => new ColumnViewInfo
                {
                    Caption = columnInfo.Caption,
                    FieldName = columnInfo.DBColumnName
                }).ToList();

            levelPanelViewInfo.ContentPanelViewInfo = new ContentPanelViewInfo
            {
                ViewerId = vfi.Id,
                ContentPanelType = ContentPanelTypes.Grid,
                GridPanelViewInfo = new GridPanelViewInfo
                {
                    ColumnViewInfos = columns?.Count > 0
                        ? columns
                        : null,
                    Editable = true
                }
            };

            return levelPanelViewInfo;
        }
    }
}
