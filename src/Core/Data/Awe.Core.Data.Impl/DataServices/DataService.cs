﻿using Awe.Core.Data.Contracts.DataServices;
using Abp.Domain.Services;
using Awe.Core.Common.Contracts.Data;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Data.Contracts.Settings.Enums;
using Awe.DataClients.Api.Contracts.Api;
using Awe.Contracts.Settings.ViewerBuild;

namespace Awe.Core.Data.Impl.DataServices
{
    public class DataService : DomainService, IDataService
    {
        private readonly IAweApiClient _dataClient;

        public DataService(IAweApiClient dataClient)
        {
            _dataClient = dataClient;
        }

        public async Task LoadDataAsync(int? viewerId, IDataStore dataStore, FilterCollection filters)
        {
            var apiSource = ApiSource.AweSettingsFile;
            var queryParams = new ApiParams(filters);

            switch (dataStore)
            {
                case ISingleDataStore sds:
                    var item = await _dataClient.GetViewerDataSingleAsync<ViewerDCardInfo>(apiSource,
                        "Viewer",
                        "GetById",
                        queryParams);

                    sds.Init(item);
                    break;
                case IListDataStore lds:
                    switch (viewerId)
                    {
                        case 301:
                            await LoadDataAsync<ViewerListInfo>(apiSource, "Viewer", "GetAll", queryParams, lds);
                            break;
                        case 303:
                            await LoadDataAsync<CColumnInfo>(apiSource, "CColumn", "GetByViewer", queryParams, lds);
                            break;
                    }
                    break;
                default:
                    // TODO awe exception
                    throw new NotImplementedException(dataStore.ToString());
            }
        }

        private async Task LoadDataAsync<T>(ApiSource apiSource, 
            string viewerName, string verb, IApiParams queryParams, 
            IListDataStore lds)
        {
            var items = await _dataClient.GetViewerDataAsync<T>(apiSource, viewerName, verb, queryParams);
            if (items != null)
            {
                lds.Init(items.Select(i => (object)i));
            }
        }
    }
}
