﻿using Awe.Core.Common.Contracts.Data;
using Awe.Core.Data.Contracts.DataServices;

namespace Awe.Core.Data.Impl.DataServices
{
    public class ApiParams : Dictionary<string, object>, IApiParams
    {
        public ApiParams(FilterCollection filters)
        {
            foreach (var filter in filters)
            {
                Add(filter.Key, filter.Value);
            }

            if (filters.Count == 1)
            {
                KeyVal = filters.First().Value;
            }
        }

        public int? KeyVal { get; set; }
        public object Dto { get; set; }
    }
}
