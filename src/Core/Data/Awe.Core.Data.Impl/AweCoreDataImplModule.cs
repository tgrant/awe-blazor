﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Awe.Core.Data.Contracts;
using Awe.DataClients.Api.Impl;
using Awe.DataClients.Contracts;

namespace Awe.Core.Data.Impl
{
    [DependsOn(typeof(AweCoreDataContractsModule))]
    [DependsOn(typeof(AweDataClientsContractsModule))]
    [DependsOn(typeof(AweDataClientsApiImplModule))]
    public class AweCoreDataImplModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(GetType().GetAssembly());
        }
    }
}