﻿using Abp.Modules;
using Awe.Core.Common.Contracts;

namespace Awe.Core.Data.Contracts
{
    [DependsOn(typeof(AweCoreCommonContractsModule))]
    public class AweCoreDataContractsModule : AbpModule
    {
    }
}