﻿namespace Awe.Core.Data.Contracts.Settings.Enums
{
    public enum ApiSource
    {
        None,
        AweApi,
        AweApiSettings,
        AweApiAuth,
        AweApiAudit,
        
        ApiBlackLabel,
        ApiDistant,

        Ado,

        LogDB,
        MiniCRM,
        //Settings,
        Study,
        Weapon,

        AweSettingsFile
    }
}