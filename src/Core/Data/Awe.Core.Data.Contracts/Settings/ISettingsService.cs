﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Awe.Contracts.Settings.Enums.Action;
using Awe.Contracts.Settings.Enums.ViewerLinks;
using Awe.Contracts.Settings.Menu;
using Awe.Contracts.Settings.ProgramConstants;
using Awe.Contracts.Settings.ViewerBuild;
using Awe.Contracts.Settings.ViewerLinks;
using Awe.Core.Common.Contracts;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Data.Contracts.Settings
{
    public interface ISettingsService
    {
        Task<ViewerInfo> GetViewerInfoAsync(int viewerId);
        Task<ViewerFullInfo> GetViewerFullInfoAsync(int viewerId);
        ViewerInfo GetChildCardViewerInfoAsync(int viewerId, viType edit);

        Task<CrudInfo> GetCrudAsync(string alias);

        Task<bool> IsFilterFormExistsAsync(int viewerId);

        Task<ViewerLinkInfo> GetViewerLinkAsync(int viewerId, ActionType actionType, string curDbColumnName = null);
        Task<ViewerLinkInfo> GetFilterViewerLinkAsync(int viewerId);
        Task<IEnumerable<ViewerLinkInfo>> GetCtxMenuViewerLinksAsync(int viewerId);

        Task<string> GetFullSpNameAsync(int viewerId);

        Task<ProgramConstInfo> GetAppConstantsAsync();

        Task<string> GetCustomProgramConstAsync(string constName);
        Task<MenuItemInfo[]> LoadMainMenuAsync(int userId);
        Task<MenuItemInfo[]> LoadSideMenuAsync(int userId);

        string GetKeyClmnNameAsync(string tbName);
        Task<string> GetConStringAsync(string alias);
        Task<PageViewInfo> GetPageViewInfo(PageType pageType, string viewerAlias);
    }
}