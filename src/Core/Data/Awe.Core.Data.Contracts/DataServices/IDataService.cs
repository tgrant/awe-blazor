﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Awe.Core.Common.Contracts.Data;
using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Data.Contracts.DataServices
{
    public interface IDataService
    {
        Task LoadDataAsync(int? viewerId, IDataStore dataStore, FilterCollection eventDataFilters);
    }

    public interface IApiDataStore
    {
    }

    public interface IApiResult
    {
        IApiDataStore DataStore { get; set; }
        object Value { get; set; }
    }

    public interface IApiParams : IDictionary<string, object>
    {
        int? KeyVal { get; set; }
        object Dto { get; set; }
    }
}
