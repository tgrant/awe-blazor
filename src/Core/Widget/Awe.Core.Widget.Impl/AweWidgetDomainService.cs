﻿using Abp.Dependency;
using Abp.Domain.Services;

namespace Awe.Core.Widget.Domain
{
    public class AweWidgetDomainService : DomainService
    {
        protected static IIocManager IocManager => Abp.Dependency.IocManager.Instance;
    }
}
