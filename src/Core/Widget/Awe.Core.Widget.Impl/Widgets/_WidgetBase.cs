using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Widget.Domain.Widgets;

public class WidgetBase<TC, TD, TV> : AweWidgetDomainService, IWidget<TC, TD, TV>
    where TC : class, IController
    where TD : class, IDataManager
    where TV : class, IView
{
    IDataManager IWidget.DataManager
    {
        get => DataManager;
        set => DataManager = (TD)value;
    }

    IView IWidget.View
    {
        get => View;
        set => View = (TV)value;
    }

    public virtual int? Id { get; set; }

    IController IWidget.Controller
    {
        get => Controller;
        set => Controller = (TC)value;
    }

    public TC Controller { get; set; }
    public TD DataManager { get; set; }
    public TV View { get; set; }

    public WidgetBase(TC controller, TD dataManager)
    {
        Controller = controller;
        //controller.Widget = this;

        DataManager = dataManager;
    }

    public IWidget? ParentWidget { get; set; }
    public T Parent<T>(IWidget? startWidget = null) where T : IWidget
    {
        throw new NotImplementedException();
    }

    public T ParentPart<T>(IWidget? startWidget = null) where T : class, IWidgetPart
    {
        throw new NotImplementedException();
    }

    public IWidgetCollection WidgetChildren { get; } = new WidgetCollection(); 

    public IEnumerable<T> ChildParts<T>(bool recurse = true, Func<IWidget, bool> selector = null) where T : IWidgetPart
    {
        if (!WidgetChildren.Any()) yield break;

        var wpt = GetWidgetPartType<T>();

        selector ??= widget => true;

        foreach (var widget in WidgetChildren.Values
                                .Where(widget => widget[wpt] is T)
                                .Where(selector))
        {
            yield return (T)widget[wpt];
        }

        if (!recurse) yield break;

        foreach (var widgetPart in WidgetChildren.SelectMany(chwidget => chwidget.Value.ChildParts<T>(recurse, selector)))
        {
            yield return widgetPart;
        }
    }

    public IEnumerable<IWidgetPart> Parts { get; }

    public IWidgetPart this[WidgetParts widgetPartType] => throw new NotImplementedException();

    public IEnumerable<T> ChildWidgets<T>(bool recurse = true, Func<IWidget, bool> selector = null) where T : IWidget
    {
        if (!WidgetChildren.Any()) yield break;

        selector ??= widget => true;

        foreach (var widget in WidgetChildren.Values
                     .Where(widget => widget is T)
                     .Where(selector))
        {
            yield return (T)widget;
        }

        if (!recurse) yield break;

        foreach (var widget in WidgetChildren.Values.SelectMany(
                     wch => wch.ChildWidgets<T>(recurse, selector)
                 ))
        {
            yield return widget;
        }
    }

    private static WidgetParts GetWidgetPartType<T>() where T : IWidgetPart
    {
        var typeT = typeof(T);

        return typeof(IController).IsAssignableFrom(typeT)
            ? WidgetParts.Controller
            : typeof(IDataManager).IsAssignableFrom(typeT)
                ? WidgetParts.Model
                : typeof(IView).IsAssignableFrom(typeT)
                    ? WidgetParts.View
                    : WidgetParts.NotDefined;
    }
}