using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Controller.ContentPanel;
using Awe.Core.Widget.Contracts.Data.ContentPanel;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;

namespace Awe.Core.Widget.Domain.Widgets.ContentPanel;

public class WGridPanel : WContentPanel<IGridPanelController, IGridPanelDataManager, IGridPanelView>,
    IWGridPanel
{
    public WGridPanel(IGridPanelController controller, IGridPanelDataManager dataManager) 
        : base(controller, dataManager)
    {
    }

    public GridPanelViewInfo GridPanelViewInfo { get; set; }
}