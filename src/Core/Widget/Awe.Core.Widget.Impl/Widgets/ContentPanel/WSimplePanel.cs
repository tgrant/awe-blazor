using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Controller.ContentPanel;
using Awe.Core.Widget.Contracts.Data.ContentPanel;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;
using Awe.Core.Widget.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;

namespace Awe.Core.Widget.Domain.Widgets.ContentPanel;

public class WSimplePanel : WContentPanel<ISimplePanelController, ISimplePanelDataManager, ISimplePanelView>,
    IWSimplePanel
{
    public WSimplePanel(ISimplePanelController controller, ISimplePanelDataManager dataManager) : base(controller, dataManager)
    {
    }

    public SimplePanelViewInfo SimplePanelViewInfo { get; set; }
}