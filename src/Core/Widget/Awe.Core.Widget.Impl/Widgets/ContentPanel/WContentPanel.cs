﻿using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Controller.ContentPanel;
using Awe.Core.Widget.Contracts.Data.ContentPanel;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;

namespace Awe.Core.Widget.Domain.Widgets.ContentPanel;

public class WContentPanel<TC, TD, TV> : WidgetBase<IContentPanelController, IContentPanelDataManager, IContentPanelView>,
    IWContentPanel<TC, TD, TV>
    where TC : class, IContentPanelController
    where TD : class, IContentPanelDataManager
    where TV : class, IContentPanelView
{
    public WContentPanel(TC controller, TD dataManager) : base(controller, dataManager)
    {
    }

    public ContentPanelViewInfo ContentPanelViewInfo { get; set; }
}