﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.View.LevelPanels;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Domain.Widgets;

public class WFileStorage : WidgetBase<IFileStorageController, IFileStorageDataManager, IFileStorageView>, IWFileStorage
{
    public WFileStorage(IFileStorageController controller, IFileStorageDataManager dataManager) : base(controller, dataManager)
    {
    }

    public FileStorageViewInfo FileStorageViewInfo { get; set; }
}