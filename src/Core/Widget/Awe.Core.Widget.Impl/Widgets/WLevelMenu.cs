﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.Builder;
using Awe.Core.Widget.Contracts.View.LevelPanels;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Domain.Widgets;

public class WLevelMenu : WidgetBase<ILevelMenuController, ILevelMenuDataManager, ILevelPanelMenuView>, IWLevelMenu
{
    public WLevelMenu(ILevelMenuController controller, ILevelMenuDataManager dataManager) : base(controller, dataManager)
    {
    }

    public LevelMenuViewInfo LevelMenuViewInfo { get; set; }
}