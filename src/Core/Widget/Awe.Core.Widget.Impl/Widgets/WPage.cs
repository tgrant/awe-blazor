using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.View.Page;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Domain.Widgets;

public class WPage : WidgetBase<IPageController, IPageDataManager, IPageView>,
    IWPage
{
    public WPage(IPageController controller, IPageDataManager dataManager) : base(controller, dataManager)
    {
    }

    public PageViewInfo PageViewInfo { get; set; }
}