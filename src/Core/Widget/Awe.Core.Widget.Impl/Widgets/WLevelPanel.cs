﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.View.LevelPanels;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;

namespace Awe.Core.Widget.Domain.Widgets;

public class WLevelPanel : WidgetBase<ILevelPanelController, ILevelPanelDataManager, ILevelPanelView>, IWLevelPanel
{
    public IWLevelMenu? LevelMenuWidget { get; set; }
    public IWContentPanel ContentPanelWidget { get; set; }
    public IWFileStorage? FileStorageWidget { get; set; }

    public WLevelPanel(ILevelPanelController controller,
        ILevelPanelDataManager dataManager

        ) : base(controller, dataManager)
    {
    }

    public LevelPanelViewInfo LevelPanelViewInfo { get; set; }
}