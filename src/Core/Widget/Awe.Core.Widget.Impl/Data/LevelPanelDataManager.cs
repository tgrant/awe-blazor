﻿using Abp.Domain.Services;
using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Domain.Data;

public class LevelPanelDataManager : DomainService, ILevelPanelDataManager
{
    public LevelPanelViewInfo ViewInfo { get; set; }
    public IDataStore DataStore { get; set; }
}