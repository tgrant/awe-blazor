﻿using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Builder;

namespace Awe.Core.Widget.Domain.Data;

public class LevelMenuDataManager : DataManagerBase, ILevelMenuDataManager
{
    public override IDataStore DataStore { get; set; }
}