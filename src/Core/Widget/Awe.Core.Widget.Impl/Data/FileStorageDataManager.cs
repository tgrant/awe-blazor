﻿using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Widget.Domain.Data;

public class FileStorageDataManager : DataManagerBase, IFileStorageDataManager
{
    public override IDataStore DataStore { get; set; }
}