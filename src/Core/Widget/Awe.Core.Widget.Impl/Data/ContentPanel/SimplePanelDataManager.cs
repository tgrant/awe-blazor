﻿using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Data.ContentPanel;

namespace Awe.Core.Widget.Domain.Data.ContentPanel;

public class SimplePanelDataManager : AweWidgetDomainService, ISimplePanelDataManager
{
    public SimplePanelDataManager(ISingleDataStore dataStore)
    {
        DataStore = dataStore;
    }

    IDataStore IDataManager.DataStore
    {
        get => DataStore;
        set => DataStore = (ISingleDataStore)value;
    }

    public ISingleDataStore DataStore { get; set; }
}