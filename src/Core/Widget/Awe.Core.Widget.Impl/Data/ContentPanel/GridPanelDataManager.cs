﻿using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Data.ContentPanel;

namespace Awe.Core.Widget.Domain.Data.ContentPanel;

public class GridPanelDataManager : AweWidgetDomainService, IGridPanelDataManager
{
    public GridPanelDataManager(IListDataStore dataStore)
    {
        DataStore = dataStore;
    }

    public IListDataStore DataStore { get; set; }

    IDataStore IDataManager.DataStore
    {
        get => DataStore;
        set => DataStore = (IListDataStore)value;
    }
}