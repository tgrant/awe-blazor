﻿using Awe.Core.Widget.Contracts.Data.ContentPanel;

namespace Awe.Core.Widget.Domain.Data.ContentPanel;

public abstract class ContentPanelDataManager : DataManagerBase,
    IContentPanelDataManager
{
}