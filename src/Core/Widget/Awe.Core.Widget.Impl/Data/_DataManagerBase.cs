﻿using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Widget.Domain.Data;

public abstract class DataManagerBase: AweWidgetDomainService, IDataManager
{
    public abstract IDataStore DataStore { get; set; }
}