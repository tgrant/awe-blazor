using Abp.Domain.Services;
using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Widget.Domain.Data;

public class LevelPanelMenuDataManager : DomainService, ILevelPanelMenuDataManager
{
    public IDataStore DataStore { get; set; }
}