using Abp.Domain.Services;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Domain.Data;

public class PageDataManager : DomainService, IPageDataManager
{
    public IDataStore DataStore { get; set; }
    public PageViewInfo PageViewInfo { get; set; }
}