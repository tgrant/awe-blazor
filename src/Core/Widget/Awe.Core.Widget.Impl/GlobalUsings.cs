// Global using directives

global using Awe.Contracts.Settings.Enums.Menu;
global using Awe.Contracts.Settings.Menu;
global using Awe.Core.Widget.Contracts;
global using Awe.Core.Widget.Contracts.Controller;
global using Awe.Core.Widget.Contracts.Data;
global using Awe.Core.Widget.Contracts.View;
global using Awe.Core.Widget.Contracts.Widget;