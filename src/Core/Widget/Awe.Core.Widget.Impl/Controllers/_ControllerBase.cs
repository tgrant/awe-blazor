﻿using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Widget.Domain.Controllers;

public class ControllerBase : AweWidgetDomainService, IController
{
    public virtual void EventsInit()
    {
    }
}