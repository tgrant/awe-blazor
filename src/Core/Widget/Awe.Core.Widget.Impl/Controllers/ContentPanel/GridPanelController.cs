﻿using Awe.Core.Common.Contracts.ViewEvent;
using Awe.Core.Widget.Contracts.Controller.ContentPanel;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;

namespace Awe.Core.Widget.Domain.Controllers.ContentPanel;

public class GridPanelController : ContentPanelController, IGridPanelController
{
    private readonly IWidgetEventService _widgetEventService;

    public GridPanelController(IWidgetEventService widgetEventService)
    {
        _widgetEventService = widgetEventService;
    }

    public override async Task RowDoubleClickHandle(IGridPanelView gridPanelView, object o)
    {
        new NotImplementedException("RowDoubleClickHandle");
    }
}