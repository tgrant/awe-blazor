﻿using Awe.Core.Widget.Contracts.Controller.ContentPanel;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;

namespace Awe.Core.Widget.Domain.Controllers.ContentPanel;

public abstract class ContentPanelController : ControllerBase,
    IContentPanelController
{
    public abstract Task RowDoubleClickHandle(IGridPanelView gridPanelView, object o);
}