﻿using Abp.Domain.Services;
using Abp.Events.Bus;
using Awe.Core.Common.Contracts.ViewEvent;
using Newtonsoft.Json;

namespace Awe.Core.Widget.Domain;

public class WidgetEventService : DomainService, IWidgetEventService
{
    private readonly EventBus _eventBus;

    public WidgetEventService()
    {
        _eventBus = EventBus.Default;
    }

    public void TriggerEvent<T>(T eventData)
        where T: WidgetEventData
    {
        _eventBus.Trigger(eventData);

        Logger.Debug(JsonConvert.SerializeObject(eventData));
    }

    public async Task TriggerEventAsync<T>(T eventData) where T : WidgetEventData
    {
        await _eventBus.TriggerAsync(eventData);

        Logger.Debug(JsonConvert.SerializeObject(eventData));
    }
}