﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Awe.Contracts.Settings.ViewerBuild;
using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Domain;

[DependsOn(typeof(AweWidgetContractsModule))]
[DependsOn(typeof(AbpAutoMapperModule))]
public class AweCoreWidgetImplModule : AbpModule
{
    public override void PreInitialize()
    {
        base.PreInitialize();

        Configuration.Modules.AbpAutoMapper().Configurators.Add(config =>
        {
            config.CreateMap<ViewerFullInfo, LevelPanelViewInfo>();
        });
    }

    public override void Initialize()
    {
        IocManager.RegisterAssemblyByConvention(GetType().GetAssembly());
    }
}