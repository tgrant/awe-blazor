﻿using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Builder.ContentPanel;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;

namespace Awe.Core.Widget.Domain.Builders.ContentPanel;

public class GridPanelBuilder : AweWidgetDomainService, IGridPanelBuilder
{
    public IWGridPanel Build(IWLevelPanel? parentWidget, GridPanelViewInfo gridPanelViewInfo)
    {
        var widget = IocManager.Resolve<IWGridPanel>();
        widget.GridPanelViewInfo = gridPanelViewInfo;

        return widget;
    }
}