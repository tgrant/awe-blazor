﻿using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Builder.ContentPanel;
using Awe.Core.Widget.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;

namespace Awe.Core.Widget.Domain.Builders.ContentPanel;

public class ContentPanelBuilder : AweWidgetDomainService,
    IContentPanelBuilder
{
    private readonly ISimplePanelBuilder _simplePanelBuilder;
    private readonly IGridPanelBuilder _gridPanelBuilder;

    public ContentPanelBuilder(ISimplePanelBuilder simplePanelBuilder, IGridPanelBuilder gridPanelBuilder)
    {
        _gridPanelBuilder = gridPanelBuilder;
        _simplePanelBuilder = simplePanelBuilder;
    }

    public IWContentPanel Build(IWLevelPanel? parentWidget, ContentPanelViewInfo contentPanelViewInfo)
    {
        IWContentPanel widget;

        // TODO move to ioc fabric
        switch (contentPanelViewInfo.ContentPanelType)
        {
            case ContentPanelTypes.Simple:
                widget = _simplePanelBuilder.Build(parentWidget, contentPanelViewInfo.SimplePanelViewInfo ?? throw new ArgumentNullException());
                break;
            case ContentPanelTypes.Grid:
                widget = _gridPanelBuilder.Build(parentWidget, contentPanelViewInfo.GridPanelViewInfo ?? throw new ArgumentNullException());
                break;
            case ContentPanelTypes.None:
            case ContentPanelTypes.FileStorage:
            case ContentPanelTypes.LevelMenu:
            case ContentPanelTypes.Tree:
            default:
                throw new NotSupportedException();
        }

        widget.ContentPanelViewInfo = contentPanelViewInfo;

        return widget;
    }
}