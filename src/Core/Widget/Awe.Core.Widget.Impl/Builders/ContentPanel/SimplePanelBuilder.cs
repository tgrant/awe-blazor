﻿using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Builder.ContentPanel;
using Awe.Core.Widget.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;

namespace Awe.Core.Widget.Domain.Builders.ContentPanel;

public class SimplePanelBuilder : AweWidgetDomainService, ISimplePanelBuilder
{
    public IWSimplePanel Build(IWLevelPanel? parentWidget, SimplePanelViewInfo simplePanelViewInfo)
    {
        var widget = IocManager.Resolve<IWSimplePanel>();
        widget.SimplePanelViewInfo = simplePanelViewInfo;

        return widget;
    }
}