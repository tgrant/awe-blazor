using Awe.Core.Common.Contracts.ViewEvent;
using Awe.Core.Widget.Contracts.Builder;
using Awe.Core.Widget.Contracts.View.MainMenu;
using Awe.Core.Widget.Contracts.View.MainMenu.MainMenuItem;

namespace Awe.Core.Widget.Domain.Builders;

public class MainMenuBuilder : AweWidgetDomainService, IMainMenuBuilder
{
    private readonly IMainMenuView _mainMenuView;
    private readonly IWidgetEventService _widgetEventService;
    
    public MainMenuBuilder(IMainMenuView mainMenuView, 
        IWidgetEventService widgetEventService)
    {
        _mainMenuView = mainMenuView;
        _widgetEventService = widgetEventService;
    }

    public void Build(IEnumerable<MenuItemInfo> menuItemInfos)
    {
        _mainMenuView.BeginUpdate();
            
        var cMenuItems = CreateMenuItems(menuItemInfos);

        _mainMenuView.AddItems(cMenuItems);

        _mainMenuView.EndUpdate();
    }
    
    private IEnumerable<IMenuItemView> CreateMenuItems(IEnumerable<MenuItemInfo> viewerChildMenus)
        {
            foreach (var menuItemInfo in viewerChildMenus)
            {
                switch (menuItemInfo.MenuItemType)
                {
                    case MenuItemType.MenuSeparator :
                        break;
                    case MenuItemType.MenuGroup :
                        var menuItemGroup = IocManager.Resolve<IMainMenuItemGroupView>();
                        menuItemGroup.Caption = menuItemInfo.Caption;

                        var menuItems = CreateMenuItems(menuItemInfo.MenuSubItems);
                        menuItemGroup.AddSubItems(menuItems);

                        _mainMenuView.AddItemGroup(menuItemGroup);
                        yield return menuItemGroup;
                        break;
                    case MenuItemType.MenuItem:
                    case MenuItemType.MenuPrintItem:
                        var menuItemButton =  IocManager.Resolve<IMainMenuItemButtonView>();
                        menuItemButton.Caption = menuItemInfo.Caption;

                        throw new NotImplementedException();
                        menuItemButton.ItemClick += delegate
                            {

                                //_widgetEventService.TriggerEvent(new MenuClickEventData(
                                //    menuItemInfo.Id,
                                //    menuItemInfo.Viewer != null
                                //        ? menuItemInfo.Viewer.Id
                                //        : -1)
                                //);

                                //EventBus.Default.Trigger(new WidgetActionData());
                            };

                        yield return menuItemButton;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
}