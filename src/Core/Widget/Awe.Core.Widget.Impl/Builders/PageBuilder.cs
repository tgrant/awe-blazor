﻿using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Builder;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Domain.Builders
{
    public class PageBuilder : AweWidgetDomainService, IPageBuilder
    {
        private readonly ILevelPanelBuilder _levelPanelBuilder;

        public PageBuilder(ILevelPanelBuilder levelPanelBuilder) 
        {
            _levelPanelBuilder = levelPanelBuilder;
        }

        public IWPage Build(IPageView view, PageViewInfo pageViewInfo)
        {
            if (view == null)
            {
                throw new ArgumentNullException();
            }

            var widget = IocManager.Resolve<IWPage>();
            widget.PageViewInfo = pageViewInfo;
            widget.View = view;

            widget.WidgetChildren.AddRange(WidgetChildrenInstance(widget, pageViewInfo));

            return widget;
        }

        private IEnumerable<IWLevelPanel> WidgetChildrenInstance(IWidget widget, PageViewInfo pageViewInfo)
        {
            return pageViewInfo
                .LevelPanelInfos
                .Select(pi => _levelPanelBuilder.Build(widget, pi));
        }
    }
}