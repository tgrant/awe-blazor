﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.Builder;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Domain.Builders;

public class LevelMenuBuilder : AweWidgetDomainService,
    ILevelMenuBuilder
{
    public IWLevelMenu Build(IWLevelPanel? parentWidget, LevelMenuViewInfo levelMenuViewInfo)
    {
        var widget = IocManager.Resolve<IWLevelMenu>();
        widget.LevelMenuViewInfo = levelMenuViewInfo;

        return widget;
    }
}