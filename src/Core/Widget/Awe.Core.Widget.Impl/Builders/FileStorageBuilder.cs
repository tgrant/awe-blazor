﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.Builder;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Domain.Builders;

public class FileStorageBuilder : AweWidgetDomainService,
    IFileStorageBuilder
{
    public IWFileStorage Build(IWLevelPanel? parentWidget, FileStorageViewInfo fileStorageViewInfo)
    {
        var widget = IocManager.Resolve<IWFileStorage>();
        widget.FileStorageViewInfo = fileStorageViewInfo;

        return widget;
    }
}