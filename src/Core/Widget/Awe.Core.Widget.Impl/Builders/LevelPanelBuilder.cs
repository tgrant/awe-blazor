﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Builder;
using Awe.Core.Widget.Contracts.Builder.ContentPanel;

namespace Awe.Core.Widget.Domain.Builders;

public class LevelPanelBuilder : AweWidgetDomainService,
    ILevelPanelBuilder
{
    private readonly ILevelMenuBuilder _levelPanelMenuBuilder;
    private readonly IContentPanelBuilder _contentBuilder;
    private readonly IFileStorageBuilder _fileStorageBuilder;

    public LevelPanelBuilder(
        ILevelMenuBuilder levelPanelMenuBuilder,
        IContentPanelBuilder contentBuilder,
        IFileStorageBuilder fileStorageBuilder
        )
    {
        _levelPanelMenuBuilder = levelPanelMenuBuilder;
        _contentBuilder = contentBuilder;
        _fileStorageBuilder = fileStorageBuilder;
    }
    public IWLevelPanel Build(IWidget? parentWidget, LevelPanelViewInfo viewInfo)
    {
        var widget = IocManager.Resolve<IWLevelPanel>();
        widget.LevelPanelViewInfo = viewInfo;

        widget.WidgetChildren.AddRange(WidgetChildrenInstance(widget, viewInfo));

        return widget;
    }

    private IEnumerable<IWidget> WidgetChildrenInstance(IWLevelPanel widget, LevelPanelViewInfo viewInfo)
    {
        if (viewInfo.LevelMenuViewInfo != null)
        {
            var wLevelMenu = _levelPanelMenuBuilder.Build(widget, viewInfo.LevelMenuViewInfo) ?? throw new InvalidOperationException();
            widget.LevelMenuWidget = wLevelMenu;

            yield return wLevelMenu;
        }

        if (viewInfo.FileStorageViewInfo != null)
        {
            var wFileStorage = _fileStorageBuilder.Build(widget, viewInfo.FileStorageViewInfo) ?? throw new InvalidOperationException();
            widget.FileStorageWidget = wFileStorage;
            yield return wFileStorage;
        }

        if (viewInfo.ContentPanelViewInfo != null)
        {
            var wContentPanel = _contentBuilder.Build(widget, viewInfo.ContentPanelViewInfo) ?? throw new InvalidOperationException();
            widget.ContentPanelWidget = wContentPanel;
            yield return wContentPanel;
        }
        else
        {
            throw new NotImplementedException();
        }
    }
}