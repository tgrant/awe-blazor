﻿namespace Awe.Platform.Wasm.View;

public interface IStarter
{
    Task StartAsync(string[] args);
}