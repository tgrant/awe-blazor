using System.Collections.Concurrent;
using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Widget.Contracts.Widget;


// TODO wrap concurrent collection
public class WidgetCollection : Dictionary<int, IWidget>, IWidgetCollection
{
    public async Task AddRangeAsync(IAsyncEnumerable<IWidget> widgets)
    {
        //TODO
        var i = 0;
        await foreach (var widget in widgets)
        {
            this.Add(widget.Id ?? i, widget);
            i++;
        }
    }

    public void AddRange(IEnumerable<IWidget> widgets)
    {
        //TODO
        var i = 0;
        foreach (var widget in widgets)
        {
            this.Add(widget.Id ?? i, widget);
            i++;
        }
    }
}