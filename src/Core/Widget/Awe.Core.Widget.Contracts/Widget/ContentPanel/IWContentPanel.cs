﻿using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Controller.ContentPanel;
using Awe.Core.Widget.Contracts.Data.ContentPanel;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;

namespace Awe.Core.Widget.Contracts.Widget.ContentPanel;

public interface IWContentPanel : IWidget<IContentPanelController, IContentPanelDataManager, IContentPanelView>
{
    ContentPanelViewInfo ContentPanelViewInfo { get; set; }
}

public interface IWContentPanel<TC, TD, TV> 
    where TC : class, IContentPanelController
    where TD : class, IContentPanelDataManager
    where TV : class, IContentPanelView
{
}