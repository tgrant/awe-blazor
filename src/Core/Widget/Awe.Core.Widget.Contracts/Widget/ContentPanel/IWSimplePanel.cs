using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Controller.ContentPanel;
using Awe.Core.Widget.Contracts.Data.ContentPanel;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;

namespace Awe.Core.Widget.Contracts.Widget.ContentPanel;

public interface IWSimplePanel : IWContentPanel<ISimplePanelController, ISimplePanelDataManager, ISimplePanelView>,
    IWContentPanel
{
    SimplePanelViewInfo SimplePanelViewInfo { get; set; }
}