using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Controller.ContentPanel;
using Awe.Core.Widget.Contracts.Data.ContentPanel;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;
using Awe.Core.Widget.Contracts.ViewInfo.ContentPanel;

namespace Awe.Core.Widget.Contracts.Widget.ContentPanel;

public interface IWGridPanel : IWContentPanel<IGridPanelController, IGridPanelDataManager, IGridPanelView>,
    IWContentPanel
{
    GridPanelViewInfo GridPanelViewInfo { get; set; }
}