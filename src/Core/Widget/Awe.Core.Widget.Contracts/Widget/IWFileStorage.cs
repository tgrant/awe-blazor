﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Controller;
using Awe.Core.Widget.Contracts.Data;
using Awe.Core.Widget.Contracts.View.LevelPanels;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Contracts.Widget;

public interface IWFileStorage : IWidget<IFileStorageController, IFileStorageDataManager, IFileStorageView>
{
    FileStorageViewInfo FileStorageViewInfo { get; set; }
}