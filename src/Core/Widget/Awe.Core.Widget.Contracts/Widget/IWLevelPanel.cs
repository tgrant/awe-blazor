﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Controller;
using Awe.Core.Widget.Contracts.Data;
using Awe.Core.Widget.Contracts.View.LevelPanels;
using Awe.Core.Widget.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;

namespace Awe.Core.Widget.Contracts.Widget;

public interface IWLevelPanel : IWidget<ILevelPanelController, ILevelPanelDataManager, ILevelPanelView>
{
    IWLevelMenu? LevelMenuWidget { get; set; }
    IWContentPanel ContentPanelWidget { get; set; }
    IWFileStorage? FileStorageWidget { get; set; }
    LevelPanelViewInfo LevelPanelViewInfo { get; set; }
}