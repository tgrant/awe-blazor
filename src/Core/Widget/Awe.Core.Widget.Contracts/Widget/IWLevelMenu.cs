﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Builder;
using Awe.Core.Widget.Contracts.View.LevelPanels;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Contracts.Widget;

public interface IWLevelMenu : IWidget<ILevelMenuController, ILevelMenuDataManager, ILevelPanelMenuView>
{
    LevelMenuViewInfo LevelMenuViewInfo { get; set; }
}