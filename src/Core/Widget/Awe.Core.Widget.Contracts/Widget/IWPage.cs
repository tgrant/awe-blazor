using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Controller;
using Awe.Core.Widget.Contracts.Data;
using Awe.Core.Widget.Contracts.View.Page;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Contracts.Widget;

public interface IWPage : IWidget<IPageController, IPageDataManager, IPageView>
{
    PageViewInfo PageViewInfo { get; set; }
}