﻿using Abp.Modules;
using Awe.Core.Common.Contracts;
using Awe.Core.Data.Contracts;

namespace Awe.Core.Widget.Contracts;

[DependsOn(typeof(AweCoreCommonContractsModule))]
[DependsOn(typeof(AweCoreDataContractsModule))]
public class AweWidgetContractsModule : AbpModule
{
}