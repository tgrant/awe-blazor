using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Widget.Contracts.View.MainMenu.MainMenuItem;

public interface IMenuItemView : IView
{
    bool IsSeparator { get;}
    object InnerItem { get; }
}