﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.ViewInfo;

namespace Awe.Core.Widget.Contracts.Data;

public interface ILevelPanelDataManager : IDataManager
{
    LevelPanelViewInfo ViewInfo { get; set; }
}