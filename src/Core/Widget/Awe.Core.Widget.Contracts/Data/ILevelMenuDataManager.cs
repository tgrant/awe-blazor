﻿using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.Data;

namespace Awe.Core.Widget.Contracts.Builder;

public interface ILevelMenuDataManager : IDataManager
{
}