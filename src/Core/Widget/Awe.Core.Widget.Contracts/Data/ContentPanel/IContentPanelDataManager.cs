﻿using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Widget.Contracts.Data.ContentPanel;

public interface IContentPanelDataManager : IDataManager
{
}