using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Widget.Contracts.Data.ContentPanel;

public interface IGridPanelDataManager : IContentPanelDataManager
{
    new IListDataStore DataStore { get; set; }
}