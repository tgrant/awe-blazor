using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Widget.Contracts.Data.ContentPanel;

public interface ISimplePanelDataManager : IContentPanelDataManager
{
    new ISingleDataStore DataStore { get; set; }
}