﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.Widget;

namespace Awe.Core.Widget.Contracts.Builder;

public interface ILevelPanelBuilder : IWidgetBuilder
{
    IWLevelPanel Build(IWidget? parentWidget, LevelPanelViewInfo viewInfo);
}