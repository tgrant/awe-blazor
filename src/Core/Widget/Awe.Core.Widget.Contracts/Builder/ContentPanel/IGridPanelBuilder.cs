using Awe.Core.Common.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.ViewInfo.ContentPanel;
using Awe.Core.Widget.Contracts.Widget;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;

namespace Awe.Core.Widget.Contracts.Builder.ContentPanel;

public interface IGridPanelBuilder : IWidgetBuilder
{
    IWGridPanel Build(IWLevelPanel? parentWidget, GridPanelViewInfo gridPanelViewInfo);
}