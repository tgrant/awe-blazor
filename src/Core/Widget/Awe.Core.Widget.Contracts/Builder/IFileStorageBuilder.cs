﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.Widget;

namespace Awe.Core.Widget.Contracts.Builder;

public interface IFileStorageBuilder : IWidgetBuilder
{
    IWFileStorage Build(IWLevelPanel? parentWidget, FileStorageViewInfo viewInfoFileStorageViewInfo);
}