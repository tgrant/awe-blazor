using Awe.Contracts.Settings.Menu;

namespace Awe.Core.Widget.Contracts.Builder;

public interface IMainMenuBuilder : IWidgetBuilder
{
    void Build(IEnumerable<MenuItemInfo> menuItemInfos);
}
