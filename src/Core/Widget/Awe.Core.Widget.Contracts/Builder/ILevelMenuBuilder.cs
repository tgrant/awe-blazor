﻿using Awe.Core.Common.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.Widget;

namespace Awe.Core.Widget.Contracts.Builder;

public interface ILevelMenuBuilder : IWidgetBuilder
{
    IWLevelMenu Build(IWLevelPanel? parentWidget, LevelMenuViewInfo viewInfoLevelMenuViewInfo);
}