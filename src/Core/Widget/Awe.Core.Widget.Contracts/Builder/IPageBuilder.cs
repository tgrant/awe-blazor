﻿using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.View.Page;
using Awe.Core.Widget.Contracts.ViewInfo;
using Awe.Core.Widget.Contracts.Widget;

namespace Awe.Core.Widget.Contracts.Builder;

public interface IPageBuilder : IWidgetBuilder
{
    IWPage Build(IPageView view, PageViewInfo pageViewInfo);
}