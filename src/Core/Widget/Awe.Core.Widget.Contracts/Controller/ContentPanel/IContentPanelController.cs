﻿using Awe.Core.Common.Contracts.Widget;
using Awe.Core.Widget.Contracts.View.LevelPanels.ContentPanel;

namespace Awe.Core.Widget.Contracts.Controller.ContentPanel;

public interface IContentPanelController : IController
{
    Task RowDoubleClickHandle(IGridPanelView gridPanelView, object o);
}