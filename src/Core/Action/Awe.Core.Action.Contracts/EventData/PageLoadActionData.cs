using Awe.Core.Common.Contracts.Data;
using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Action.Contracts.EventData;

public class PageLoadActionData : PageActionData
{
    public PageLoadActionData(IWidget wPage, FilterCollection filters)
    {
        Widget = wPage;
        Filters = filters;
    }

    public string ViewerAlias { get; set; }
    public int? RowId { get; set; }
    public FilterCollection Filters { get; set; }
}