using Abp.Events.Bus;
using Awe.Core.Common.Contracts.ActionData;

namespace Awe.Core.Action.Contracts.EventData;

public class PageActionData : WidgetActionData, IEventData
{
    public DateTime EventTime { get; set; }
    public object EventSource { get; set; }
}