using Awe.Core.Common.Contracts;

namespace Awe.Core.Action.Contracts.EventData;

public class NavigateEventData : ActionViewEventData
{
    public PageType PageType { get; set; }
    public string Alias { get; set; }
    public int? RowId { get; set; }
}