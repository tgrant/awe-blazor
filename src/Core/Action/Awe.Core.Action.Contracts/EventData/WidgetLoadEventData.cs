using Abp.Events.Bus;
using Awe.Core.Common.Contracts.Data;
using Awe.Core.Common.Contracts.ViewEvent;
using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Action.Contracts.EventData;

public class WidgetLoadEventData : WidgetEventData, IEventData
{
    public int? ViewerId { get; set; }

    public IDataStore DataStore { get; set;}
    public FilterCollection Filters { get; set; }
}