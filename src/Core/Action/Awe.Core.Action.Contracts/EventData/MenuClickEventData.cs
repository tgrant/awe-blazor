﻿using Awe.Core.Common.Contracts.ViewEvent;

namespace Awe.Core.Action.Contracts.EventData
{
    public class MenuClickEventData : WidgetEventData
    {
        public int MenuId { get; }
        public int ViewerId { get; }

        public MenuClickEventData(int menuId, int viewerId)
        {
            MenuId = menuId;
            ViewerId = viewerId;
        }
    }
}
