﻿using Abp.Events.Bus;

namespace Awe.Core.Action.Contracts.EventData;

public class ActionViewEventData : IEventData
{
    public DateTime EventTime { get; set; }
    public object EventSource { get; set; }
}