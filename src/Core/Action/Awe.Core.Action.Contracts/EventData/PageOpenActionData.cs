using Awe.Core.Common.Contracts;
using Awe.Core.Common.Contracts.ViewEventsData;

namespace Awe.Core.Action.Contracts.EventData;

public class PageOpenEventData : ViewEventData
{
    public PageOpenEventData(DateTime eventTime, object eventSource, PageType pageType, string alias, int? rowId) 
        : base(eventTime, eventSource)
    {
        PageType = pageType;
        Alias = alias;
        RowId = rowId;
    }

    public PageType PageType { get; set; }
    public string Alias { get; set; }
    public int? RowId { get; set; }
}