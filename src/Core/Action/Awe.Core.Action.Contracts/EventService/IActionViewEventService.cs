﻿using Awe.Core.Action.Contracts.EventData;

namespace Awe.Core.Action.Contracts.EventService
{
    public interface IActionViewEventService
    {
        void TriggerEvent<T>(T eventData)
            where T : ActionViewEventData;

        Task TriggerEventAsync<T>(T eventData) where T : ActionViewEventData;
    }
}
