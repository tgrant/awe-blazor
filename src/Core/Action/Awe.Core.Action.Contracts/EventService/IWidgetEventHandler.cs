using Abp.Events.Bus.Handlers;
using Awe.Core.Common.Contracts.ViewEvent;

namespace Awe.Core.Action.Contracts.EventService;

public interface IWidgetEventHandler<in T> : IAsyncEventHandler<T>
    where T : WidgetEventData
{
}