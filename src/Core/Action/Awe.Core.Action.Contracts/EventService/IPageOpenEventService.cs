using Awe.Core.Action.Contracts.EventData;

namespace Awe.Core.Action.Contracts.EventService;

public interface IPageOpenEventService
{
    Task PageOpenAsync(PageOpenEventData pageOpenEventData);
}