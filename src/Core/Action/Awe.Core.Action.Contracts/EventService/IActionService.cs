using Abp.Events.Bus.Handlers;
using Awe.Core.Common.Contracts.ActionData;

namespace Awe.Core.Action.Contracts.EventService;

public interface IActionService<in T> : IAsyncEventHandler<T>
    where T : WidgetActionData
{
}