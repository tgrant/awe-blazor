using Abp.Events.Bus.Handlers;
using Awe.Core.Common.Contracts.ViewEventsData;

namespace Awe.Core.Action.Contracts.EventService;

public interface IViewEventHandler<in T> : IAsyncEventHandler<T>
    where T : ViewEventData
{
}