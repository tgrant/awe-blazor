using Awe.Core.Action.Contracts.EventData;

namespace Awe.Core.Action.Contracts.ActionServices;

public interface IPageLoadActionService
{
    Task PageLoadAsync(PageLoadActionData pageLoadActionData);
}