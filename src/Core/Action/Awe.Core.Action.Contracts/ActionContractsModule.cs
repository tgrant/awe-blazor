﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Awe.Core.Common.Contracts;
using Awe.Core.Data.Contracts;

namespace Awe.Core.Action.Contracts;

[DependsOn(typeof(AweCoreCommonContractsModule))]
[DependsOn(typeof(AweCoreDataContractsModule))]
public class ActionContractsModule : AbpModule
{
    public override void Initialize()
    {
        IocManager.RegisterAssemblyByConvention(GetType().GetAssembly());
    }
}