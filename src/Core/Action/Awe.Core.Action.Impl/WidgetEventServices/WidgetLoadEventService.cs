using Abp.Domain.Services;
using Awe.Core.Action.Contracts.EventData;
using Awe.Core.Action.Contracts.EventService;
using Awe.Core.Data.Contracts.DataServices;

namespace Awe.Core.Action.Impl.WidgetEventServices;

public class WidgetLoadEventService : DomainService, IWidgetEventHandler<WidgetLoadEventData>, IWidgetLoadEventService
{
    private readonly IDataService _dataService;

    public WidgetLoadEventService(IDataService dataService)
    {
        _dataService = dataService;
    }

    public async Task HandleEventAsync(WidgetLoadEventData eventData) => await WidgetLoadAsync(eventData);

    public async Task WidgetLoadAsync(WidgetLoadEventData eventData)
    {
        await _dataService.LoadDataAsync(eventData.ViewerId, eventData.DataStore, eventData.Filters);
    }
}