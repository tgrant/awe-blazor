﻿using Abp.Domain.Services;
using Awe.Core.Action.Contracts.EventService;
using Awe.Core.Common.Contracts.ViewEventsData.ContentPanel;
using Awe.Core.Data.Contracts.DataServices;

namespace Awe.Core.Action.Impl.WidgetEventServices.ContentPanel
{
    public class GridLoadEventService : DomainService, IViewEventHandler<GridLoadEventData>
    {
        private readonly IDataService _dataService;

        public GridLoadEventService(IDataService dataService)
        {
            _dataService = dataService;
        }

        public async Task HandleEventAsync(GridLoadEventData eventData)
        {
            await _dataService.LoadDataAsync(eventData.ViewerId, null, eventData.Filters);
        }
    }
}
