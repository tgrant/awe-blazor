﻿using Abp.Domain.Services;
using Awe.Core.Action.Contracts.EventData;
using Awe.Core.Action.Contracts.EventService;
using Awe.Core.Common.Contracts.ViewEventsData.ContentPanel;

namespace Awe.Core.Action.Impl.WidgetEventServices.ContentPanel
{
    public class GridPanelEventService : DomainService, IViewEventHandler<GridEventData>
    {
        private readonly IPageOpenEventService _pageOpenEventService;

        public GridPanelEventService(IPageOpenEventService pageOpenEventService)
        {
            _pageOpenEventService = pageOpenEventService;
        }

        public Task HandleEventAsync(GridEventData eventData)
        {
            var pageOpenEventData = new PageOpenEventData(
                eventData.EventTime, 
                eventData.EventSource,
                eventData.LinkedPageInfo.PageType, 
                eventData.LinkedPageInfo.Alias, 
                eventData.RowId);

            return _pageOpenEventService.PageOpenAsync(pageOpenEventData);
        }
    }
}
