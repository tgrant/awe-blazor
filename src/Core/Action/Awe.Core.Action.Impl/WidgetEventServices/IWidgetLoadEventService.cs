using Awe.Core.Action.Contracts.EventData;

namespace Awe.Core.Action.Impl.WidgetEventServices;

public interface IWidgetLoadEventService
{
    Task WidgetLoadAsync(WidgetLoadEventData eventData);
}