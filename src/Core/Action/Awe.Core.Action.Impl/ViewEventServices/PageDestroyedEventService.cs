﻿using Abp.Domain.Services;
using Awe.Core.Action.Contracts.ActionServices;
using Awe.Core.Action.Contracts.EventService;
using Awe.Core.Common.Contracts.ViewEventsData.Page;
using Awe.Core.Data.Contracts.Settings;
using Awe.Core.Widget.Contracts.Builder;

namespace Awe.Core.Action.Impl.ViewEventServices
{
    internal class PageDestroyedEventService : DomainService, IViewEventHandler<PageDestroyedViewEventData>
    {
        public PageDestroyedEventService(IPageBuilder pageBuilder, 
            ISettingsService settingsService, 
            IPageLoadActionService pageLoadService)
        {
        }

        public async Task HandleEventAsync(PageDestroyedViewEventData viewEventData)
        {

        }
    }
}
