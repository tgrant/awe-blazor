﻿using System.Diagnostics;
using Abp.Domain.Services;
using Awe.Core.Action.Contracts.ActionServices;
using Awe.Core.Action.Contracts.EventData;
using Awe.Core.Action.Contracts.EventService;
using Awe.Core.Common.Contracts.ViewEventsData.Page;
using Awe.Core.Data.Contracts.Settings;
using Awe.Core.Widget.Contracts.Builder;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;
using System.Linq;
using Awe.Core.Common.Contracts.Widget;

namespace Awe.Core.Action.Impl.ViewEventServices
{
    internal class PageBuildEventService : DomainService, IViewEventHandler<PageBuildViewEventData>
    {
        private readonly IPageBuilder _pageBuilder;
        private readonly ISettingsService _settingsService;
        private readonly IPageLoadActionService _pageLoadService;

        public PageBuildEventService(IPageBuilder pageBuilder, 
            ISettingsService settingsService, 
            IPageLoadActionService pageLoadService)
        {
            _pageBuilder = pageBuilder;
            _settingsService = settingsService;
            _pageLoadService = pageLoadService;
        }

        public async Task HandleEventAsync(PageBuildViewEventData viewEventData)
        {
            var pageView = viewEventData.PageView;

            var pageViewInfo = await _settingsService.GetPageViewInfo(viewEventData.PageType, viewEventData.ViewerAlias);

            var wPage = _pageBuilder.Build(pageView, pageViewInfo);

            await _pageLoadService.PageLoadAsync(new PageLoadActionData(wPage, viewEventData.Filters));

            foreach (var levelPanelViewInfo in pageViewInfo.LevelPanelInfos)
            {
                // TODO mapping dataManager <> viewInfo
                var dataStore = wPage
                    .ChildWidgets<IWContentPanel>()
                    .ElementAt(levelPanelViewInfo.Level)
                    .DataManager
                    .DataStore;

                Debug.Assert(levelPanelViewInfo.ContentPanelViewInfo != null, "levelPanelViewInfo.ContentPanelViewInfo != null");
                switch (dataStore)
                {
                    case ISingleDataStore singleDataStore:
                        Debug.Assert(levelPanelViewInfo.ContentPanelViewInfo.SimplePanelViewInfo != null, "levelPanelViewInfo.ContentPanelViewInfo.SimplePanelViewInfo != null");

                        levelPanelViewInfo.ContentPanelViewInfo.SimplePanelViewInfo.DataStore = singleDataStore;
                        break;
                    case IListDataStore listDataStore:
                        Debug.Assert(levelPanelViewInfo.ContentPanelViewInfo.GridPanelViewInfo != null, "levelPanelViewInfo.ContentPanelViewInfo.GridPanelViewInfo != null");

                        levelPanelViewInfo.ContentPanelViewInfo.GridPanelViewInfo.DataStore = listDataStore;

                        break;
                }
            }

            pageView.PageViewInfo = pageViewInfo;
        }
    }
}
