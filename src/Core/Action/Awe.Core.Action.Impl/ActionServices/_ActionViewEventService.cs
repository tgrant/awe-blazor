﻿using Abp.Domain.Services;
using Abp.Events.Bus;
using Awe.Core.Action.Contracts.EventData;
using Awe.Core.Action.Contracts.EventService;
using Newtonsoft.Json;

namespace Awe.Core.Action.Impl.EventService;

public class ActionViewEventService : DomainService, IActionViewEventService
{
    private readonly EventBus _eventBus;

    public ActionViewEventService()
    {
        _eventBus = EventBus.Default;
    }

    public void TriggerEvent<T>(T eventData)
        where T : ActionViewEventData
    {
        _eventBus.Trigger(eventData);

        Logger.Debug(JsonConvert.SerializeObject(eventData));
    }

    public async Task TriggerEventAsync<T>(T eventData) where T : ActionViewEventData
    {
        await _eventBus.TriggerAsync(eventData);

        Logger.Debug(JsonConvert.SerializeObject(eventData));
    }
}