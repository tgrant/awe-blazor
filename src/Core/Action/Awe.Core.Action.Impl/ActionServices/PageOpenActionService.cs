using Abp.Domain.Services;
using Awe.Core.Action.Contracts.EventData;
using Awe.Core.Action.Contracts.EventService;

namespace Awe.Core.Action.Impl.ActionServices;

public class PageOpenEventService : DomainService, IViewEventHandler<PageOpenEventData>, IPageOpenEventService
{
    private readonly IActionViewEventService _actionViewEventService;

    public PageOpenEventService(IActionViewEventService actionViewEventService)
    {
        _actionViewEventService = actionViewEventService;
    }

    public Task HandleEventAsync(PageOpenEventData eventDataData) => PageOpenAsync(eventDataData);

    public Task PageOpenAsync(PageOpenEventData pageOpenEventData)
    {
        return _actionViewEventService.TriggerEventAsync(new NavigateEventData
        {
            PageType = pageOpenEventData.PageType,
            Alias = pageOpenEventData.Alias,
            RowId = pageOpenEventData.RowId
        });
    }
}