using Abp.Domain.Services;
using Awe.Core.Action.Contracts.ActionServices;
using Awe.Core.Action.Contracts.EventData;
using Awe.Core.Action.Contracts.EventService;
using Awe.Core.Action.Impl.WidgetEventServices;
using Awe.Core.Widget.Contracts.Widget.ContentPanel;

namespace Awe.Core.Action.Impl.ActionServices;

public class PageLoadActionService : DomainService, IActionService<PageLoadActionData>, IPageLoadActionService
{
    private readonly IWidgetLoadEventService _widgetLoadService;

    public PageLoadActionService(IWidgetLoadEventService widgetLoadService)
    {
        _widgetLoadService = widgetLoadService;
    }

    public Task HandleEventAsync(PageLoadActionData actionDataData) => PageLoadAsync(actionDataData);

    public async Task PageLoadAsync(PageLoadActionData pageLoadActionData)
    {
        var wContentPanels = pageLoadActionData
            .Widget
            .ChildWidgets<IWContentPanel>()
            .ToArray();

        foreach (var wContentPanel in wContentPanels)
        {
            var widgetLoadEventData = new WidgetLoadEventData
            {
                ViewerId = wContentPanel.ContentPanelViewInfo.ViewerId,
                DataStore = wContentPanel.DataManager.DataStore,
                Filters = pageLoadActionData.Filters
            };

            await _widgetLoadService.WidgetLoadAsync(widgetLoadEventData);

            //wContentPanel.View.ViewModel = wContentPanel.DataManager.DataStore;
        }
    }
}