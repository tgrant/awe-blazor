﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Awe.Core.Action.Contracts;
using Awe.Core.Widget.Contracts;

namespace Awe.Core.Action.Impl;

[DependsOn(typeof(ActionContractsModule))]
[DependsOn(typeof(AweWidgetContractsModule))]
public class ActionDomainModule : AbpModule
{
    public override void Initialize()
    {
        IocManager.RegisterAssemblyByConvention(GetType().GetAssembly());
    }
}